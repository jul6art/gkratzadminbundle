<?php

namespace Gkratz\AdminBundle\Form;

use AppBundle\Entity\Map;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MapType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $ro = $options['readonly'];

        $builder
            ->add('displayMainAddress')
            ->add('mainAddressIconColor')
            ->add('mainAddressIconSize')
            ->add('title')
//            ->add('theme')
            ->add('infoText', CKEditorType::class)
            ->add('name', TextType::class, array(
                'disabled' => $ro,
                'translation_domain' => 'messages',
                'label' => 'Name',
                'attr' => array(
                    'placeholder' => 'Name',
                    'class' => 'sm-8',
                    'autofocus' => 'autofocus'
                )
            ))
            ->add('zoom', IntegerType::class, array(
                'disabled' => $ro,
                'translation_domain' => 'messages',
                'label' => 'Zoom(1 -> 20)',
                'attr' => array(
                    'placeholder' => 'Zoom(1 -> 20)',
                    'class' => 'sm-8 integerToRangeInput',
                    'min' => 1,
                    'max' => 20,
                    'scale' => 1
                )
            ))
            ->add('theme', ChoiceType::class, array(
                'disabled' => $ro,
                'translation_domain' => 'messages',
                'label' => 'Theme',
                'choices' => array(
                    1, 2, 3
                ),
                'multiple' => false,
                'expanded' => true,
                'attr' => array(
                    'placeholder' => 'Theme',
                    'class' => 'sm-8'
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('readonly');
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Map'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_map';
    }


}
