<?php

namespace Gkratz\AdminBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Gkratz\AdminBundle\Constants\Constants;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $ro = $options['readonly'];

        $builder
            ->add('username', null, array(
                'disabled' => $ro,
                'required' => true,
                'label' => 'form.username',
                'translation_domain' => 'FOSUserBundle',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'form.username',
                    'autofocus' => 'autofocus'
                )
            ))
            ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array(
                'disabled' => $ro,
                'required' => true,
                'label' => 'form.email',
                'translation_domain' => 'FOSUserBundle',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'form.email'
                )
            ))
            ->add('lastname', TextType::class, array(
                'disabled' => $ro,
                'required' => true,
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Lastname'
                )
            ))
            ->add('firstname', TextType::class, array(
                'disabled' => $ro,
                'required' => true,
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Firstname'
                )
            ))
            ->add('birthdate', 'Gkratz\AdminBundle\Form\Standard\DateTimePickerType', array(
                'disabled' => $ro,
                'required' => false,
                'translation_domain' => 'messages',
                'label' => 'Birthdate',
                'attr' => array(
                    'type' => 'datetime',
                    'class' => 'sm-8 datepicker',
                    'placeholder' => 'Birthdate'
                )
            ))
            ->add('address', TextType::class, array(
                'disabled' => $ro,
                'required' => false,
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Address'
                )
            ))
            ->add('phoneNumber', TextType::class, array(
                'disabled' => $ro,
                'required' => false,
                'translation_domain' => 'messages',
                'label' => 'Phone',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Phone'
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('readonly');
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
