<?php

namespace Gkratz\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuItemType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'translation_domain' => 'messages',
                'label' => 'Name',
                'attr' => array(
                    'placeholder' => 'Name',
                    'class' => 'sm-8',
                    'autofocus' => 'autofocus'
                )
            ))
            ->add('path', TextType::class, array(
                'translation_domain' => 'messages',
                'label' => 'Path',
                'attr' => array(
                    'placeholder' => 'Path',
                    'class' => 'sm-8'
                )
            ))->add('icon', TextType::class, array(
                'required' => false,
                'translation_domain' => 'messages',
                'label' => 'Icon',
                'attr' => array(
                    'placeholder' => 'Icon',
                    'class' => 'sm-8'
                )
            ))
            ->add('isSection', CheckboxType::class, array(
                'required' => false,
                'translation_domain' => 'messages',
                'label' => 'Section',
                'attr' => array(
                    'placeholder' => 'Section',
                    'class' => 'sm-8'
                )
            ))
            ;

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\MenuItem'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_menu_item';
    }
}
