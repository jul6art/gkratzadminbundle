<?php

namespace Gkratz\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $ro = $options['readonly'];

        $builder
            ->add('title', TextType::class, array(
                'disabled' => $ro,
                'translation_domain' => 'messages',
                'label' => 'Title',
                'attr' => array(
                    'placeholder' => 'Title',
                    'class' => 'sm-8',
                    'autofocus' => 'autofocus'
                )
            ))
            ->add('page', CheckboxType::class, array(
                'disabled' => $ro,
                'required' => false,
                'translation_domain' => 'messages',
                'label' => 'Page',
                'attr' => array(
                    'placeholder' => 'Page',
                    'class' => 'sm-8'
                )
            ))
            ->add('menu', CheckboxType::class, array(
                'disabled' => $ro,
                'required' => false,
                'translation_domain' => 'messages',
                'label' => 'Add to menu items',
                'attr' => array(
                    'placeholder' => 'Add to menu items',
                    'class' => 'sm-8'
                )
            ))
            ->add('tags', EntityType::class, array(
                'disabled' => $ro,
                'required' => false,
                'multiple' => true,
                'class' => 'AppBundle:Tag',
                'choice_label' => 'name',
                'choice_value' => 'id',
                'translation_domain' => 'messages',
                'label' => 'Tags',
                'attr' => array(
                    'placeholder' => 'Tags',
                    'class' => 'sm-12 gk-select2'
                ),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('t')->addOrderBy('t.name', 'asc');
                }
            ))
            ->add('content',CKEditorType::class, array(
                'disabled' => $ro,
                'required' => false,
                'translation_domain' => 'messages',
                'label' => 'Content',
                'attr' => array(
                    'placeholder' => 'Content',
                    'class' => 'sm-12'
                )
            ))
        ;

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('readonly');
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Post'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_post';
    }
}
