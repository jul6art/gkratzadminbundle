<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 13-02-17
 * Time: 17:21
 */

namespace Gkratz\AdminBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaintenanceDateType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', 'Gkratz\AdminBundle\Form\Standard\DateTimePickerType', array(
                'required' => true,
                'translation_domain' => 'messages',
                'label' => 'Date of return',
                'attr' => array(
                    'type' => 'datetime',
                    'class' => 'sm-8 datepicker',
                    'placeholder' => 'Date of return'
                )
            ))
            ->add('time', TextType::class, array(
                'required' => true,
                'translation_domain' => 'messages',
                'label' => 'Time of return',
                'attr' => array(
                    'type' => 'datetime',
                    'class' => 'sm-8 timepicker',
                    'placeholder' => 'Time of return'
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_maintenance_date';
    }


}
