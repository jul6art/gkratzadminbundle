<?php

namespace Gkratz\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaintenanceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required' => true,
                'label' => 'Name',
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Name'
                )
            ))
            ->add('phone', TextType::class, array(
                'required' => false,
                'label' => 'Phone',
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Phone'
                )
            ))
            ->add('email', EmailType::class, array(
                'required' => true,
                'label' => 'Email',
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Email'
                )
            ))
            ->add('facebook', TextType::class, array(
                'required' => false,
                'label' => 'Facebook url',
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Facebook url'
                )
            ))
            ->add('twitter', TextType::class, array(
                'required' => false,
                'label' => 'Twitter username',
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Twitter username'
                )
            ))
            ->add('googleMapApikey', TextType::class, array(
                'required' => false,
                'label' => 'Google Map apikey',
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Google Map apikey'
                )
            ))
            ->add('googleMapMainAddress', TextType::class, array(
                'required' => false,
                'label' => 'Google Map main address',
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Google Map main address'
                )
            ))
            ->add('logoSrc', FileType::class, array(
                'translation_domain' => 'messages',
                'mapped' => true,
                'required' => false,
                'label' => 'Logo',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Logo'
                )
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Parameters'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_maintenance';
    }


}
