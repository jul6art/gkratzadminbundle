<?php

namespace Gkratz\AdminBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Gkratz\AdminBundle\Constants\Constants;
use Gkratz\AdminBundle\Form\Standard\DateTimePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array(
                'label' => 'form.username',
                'translation_domain' => 'FOSUserBundle',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'form.username',
                    'autofocus' => 'autofocus'
                )
            ))
            ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array(
                'label' => 'form.email',
                'translation_domain' => 'FOSUserBundle',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'form.email'
                )
            ))
        ;

        $constraintsOptions = array(
            'message' => 'fos_user.current_password.invalid',
        );

        if (!empty($options['validation_groups'])) {
            $constraintsOptions['groups'] = array(reset($options['validation_groups']));
        }

        $builder->add('current_password', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'), array(
            'label' => 'form.current_password',
            'translation_domain' => 'FOSUserBundle',
            'mapped' => false,
            'constraints' => new UserPassword($constraintsOptions),
            'attr' => array(
                'class' => 'sm-8',
                'placeholder' => 'form.current_password'
            )
        ));

        $builder
            ->add('lastname', TextType::class, array(
                'required' => true,
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Lastname'
                )
            ))
            ->add('firstname', TextType::class, array(
                'required' => true,
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Firstname'
                )
            ))
            ->add('birthdate', 'Gkratz\AdminBundle\Form\Standard\DateTimePickerType', array(
                'required' => false,
                'translation_domain' => 'messages',
                'label' => 'Birthdate',
                'attr' => array(
                    'type' => 'datetime',
                    'class' => 'sm-8 datepicker',
                    'placeholder' => 'Birthdate'
                )
            ))
            ->add('address', TextType::class, array(
                'required' => false,
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Address'
                )
            ))
            ->add('phoneNumber', TextType::class, array(
                'required' => false,
                'translation_domain' => 'messages',
                'label' => 'Phone',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Phone'
                )
            ))
            ->add('avatarSrc', FileType::class, array(
                'required' => false,
                'translation_domain' => 'messages',
                'label' => 'Avatar',
                'attr' => array(
                    'class' => 'sm-8',
                    'placeholder' => 'Avatar'
                )
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
