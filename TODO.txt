###IMPORTANT TENIR A JOUR README.MD###
###IMPORTANT TENIR A JOUR README.MD###
###IMPORTANT TENIR A JOUR README.MD###


###VERSION 1### apikey:

A faire dans cet ordre:


//taches prioritaires
* revoir animations wow
* plusieurs ip pour maintenance et redirection si maintenance(pour référencement)
* corriger translation bundle
* enlever fa-spin sur l icone du bouton "vider le cache"
* ajouter gestion de routes et permissions(roles) (via config yml)
* ajouter gestion d'activation des modules via config yml (desactiver maintenance/analytics/post-menus (cms)/...
* revoir upload images (multiple comme dwb) et à l'edition afficher images et possibilité de les supprimer (truc tip - top) voir plugin enoprimes
* revoir eventlistener/postsunscriber + tester typage des params d'url dans admincontroller abstratit et surtout tester 404 propres + revoir gkratzcontroller en listener
* modifier entités, ajouter "Trait" php pour state et annotation @trashable
* modifier l'héritage multiple dans analyticBundle par des traits et use trait
* refactoriser code controllers (services(en arborescence) + chainer les setters + traits + tableaux [] au lien de array())
* translations yml par domaines et en respectant convention pour le nommage
	app.entite.champ: value
	admin.entite.champ: value
* j-editable + doc pour admins

* vérifier que le nombre de requetes est bien optimisé (index de FK et ?EAGER fetch?)
* modifier css du menu (+bouton toggle menu petites icones au clic) + sidebar(flex full height et expand ratio sur zone de tableaux))
* deux langues (FR-EN) en dropdown dans topbar
* essayer INTL-TEL-INPUT

* ajouter des annotations (uploadable, searchable, ...)


//refonte controleurs (sur une branche juste pour moi car trop compliqué à prendre en main)
* refaire le controlleur standard en utilisant un manager, un voter, des traits
* réadapter tous les contrôleurs


//taches secondaires
* doc pour utiliser la table superglobale paramètres (nom du site etc.. + y ajouter des paramètres comme avatar dossier url et un crud pour ajout/edition/deletion des NOUVEAUX champs , ...)
* formulaires add et edit du BO en ajax-form avec cote twig, if app.isxml extends modal ou extends layout
* soumission des formulaires d'AJOUT : select list pour choix de la redirection (rester sur la page ou revenir à liste ou ajout d'un autre)


//apilogs
* gestion des events pour chaque entité (create, update, delete) 
* PAS SUR FICHIERS mais en DB(APILOG) et dispatcher et listener (afficher une vue simple comme dans le theme metronic + crud complet avec fancydump pour les datas) et y ajouter 
	les logs deprecated de symfony
	les form errors
	les erreurs 403-404-500-400


//messagerie
* FOSMessageBundle à la place (intégration propre du bundle et ?EVENTS LISTENER? à la place de ma section messages
* box formulaire autofocus ou premier new (garder compte new)
* redirection au choix de l expediteur jquery pas dans ajax-admin
* selection + delete + new controller
* reprendre css et design et delete-selection repris dans profile
* messages privés marquer comme lus et lien avec #
* 50 derniers messages et bouton voir plus ancien/plus récent et form-filters et vue tableaux
* template mail unique et très design en display-table et multilangue


//maps
* bloquer pages /map si pas de google apikey
* formulaire map design
* objet map-marker en multiple=true avec choix couleur et taille icone et title et infoText
* translations
* crud table + filtre
* map config map id + entites (state 0) + champ address + icon-color icon-size + doc readme
* recuperer config dans page
* tester carte avec adresses des users en fixtures


//systeme de notifications avec listener, annottaions, traits, voter et yml config
* faire ca facon interact PRO
* table notifications // choix "entités.roles" notifiés en config.yml  (userid, adminid ou admins (tous) stocker en db userid, element class, elementid et text notification. en cas d'update d entite(eventlistener) remove notifs if entite state != 0


//ecrire une doc super etoffee EN PLUSIEURS CHAPITRES qui reprend tout: 
	raccourcis clavier, 
	options, 
	annotations, 
	admincontrolleur, 
	messagerie, 
	recherche avancéè, 
	views(bundle et app), 
	translations 
	new menu(path, name, ...), 
	entity, 
	form(avec ou sans readonly), 
	audit, 
	map(google map apikey) 
	etc...
	
###VERSION 1 FINIE: commande delete dossier _data et refaire le depot git pour vider les commits et test en prod depôt démo###



//passer à suite du planning général avant au moins d'avoir adapté le bundle à symfony4
//passer à suite du planning général avant au moins d'avoir adapté le bundle à symfony4
###VERSION 2###
+ themes (upload etc...)
+ traductions database sur LE MENU et LES PAGES puis sur une table exemple appelée foobar (id = -1: tous, id = 0:default, id = > 0 : langue choisie)
+ doc traductions et RECHERCHE APPROCHANTE sur tables traduites
###VERSION 2 dans une autre branche GIT###


###VERSION 3### AUTHKEYSDANSCONFIG
+ discus ?
+ facebook connection ?
+ doc social media
###VERSION 3 dans une autre branche GIT###


###APRES TEST EN PROD###
###APRES TEST EN PROD###
###APRES TEST EN PROD###
vérifier redirections javascript en cas de raccourcis clavier ou menu contextuel clics
vérifier droits et backup
- partial-javascript-keys.html.twig
- partial-javascript-context-elements.html.twig








