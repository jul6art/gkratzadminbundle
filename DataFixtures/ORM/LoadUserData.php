<?php

namespace Gkratz\AdminBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 29/01/2017
 * Time: 14:52
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var \AppBundle\Entity\User $user */
        $user = new User();
        $user->setUsername('user');
        $user->setEmail('gkratz@lusis.lu');
        $user->setEnabled(1);
        $user->setPlainPassword('user');
        $user->setSuperAdmin(0);
        $this->addReference('user_user', $user);

        $manager->persist($user);

        /** @var \AppBundle\Entity\User $user */
        $user = new User();
        $user->setUsername('test');
        $user->setEmail('test@test.test');
        $user->setEnabled(1);
        $user->setPlainPassword('test');
        $user->setSuperAdmin(0);
        $user->setState(1);
        $this->addReference('user_test', $user);

        $manager->persist($user);

        /** @var \AppBundle\Entity\User $user */
        $user = new User();
        $user->setUsername('admin');
        $user->setEmail('geoffreyk42@gmail.com');
        $user->setEnabled(1);
        $user->setPlainPassword('admin');
        $user->setSuperAdmin(1);
        $user->addRole('ROLE_ADMIN');
        $user->addRole('ROLE_SUPER_ADMIN');
        $user->setFirstname('Admini');
        $user->setLastname('Strateur');
        $user->setPhoneNumber('0497/18.99.98');
        $user->setAddress('rue de la révision 105, Liège, Belgique');
        $this->addReference('user_admin', $user);

        $manager->persist($user);

        /** @var \AppBundle\Entity\User $user */
        $user = new User();
        $user->setUsername('user avec un nom très long');
        $user->setEmail('user2@test.lu');
        $user->setEnabled(1);
        $user->setPlainPassword('user2');
        $user->setSuperAdmin(0);

        $manager->persist($user);

        for($i = 3; $i < 102; $i ++){
            /** @var \AppBundle\Entity\User $user */
            $user = new User();
            $user->setUsername('user' . $i);
            $user->setEmail('user' . $i . '@test.lu');
            $user->setEnabled(1);
            $user->setPlainPassword('user' . $i);
            $user->setSuperAdmin(0);

            $manager->persist($user);
        }

        $manager->flush();
    }


    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }
}