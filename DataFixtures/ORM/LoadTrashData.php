<?php

namespace Gkratz\AdminBundle\DataFixtures\ORM;

use AppBundle\Entity\Trash;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 29/01/2017
 * Time: 14:52
 */
class LoadTrashData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var \AppBundle\Entity\User $user */
        $user = $this->getReference('user_test');

        /** @var \AppBundle\Entity\Trash $trash */
        $trash = new Trash();
        $trash->setClass('AppBundle\Entity\User');
        $trash->setUser($user);
        $trash->setElementId($user->getId());
        $trash->setLegende($user->getUsername());

        $manager->persist($trash);
        $manager->flush();
    }


    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 3;
    }
}