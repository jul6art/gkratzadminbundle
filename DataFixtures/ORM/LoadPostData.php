<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 14-03-17
 * Time: 18:01
 */

namespace Gkratz\AdminBundle\DataFixtures\ORM;


use AppBundle\Entity\Post;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPostData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var \AppBundle\Entity\Post $post */
        $post = new Post();
        $post->setPage(1);
        $post->setMenu(1);
        $post->setTitle('My first post');
        $post->setAuthor($this->getReference('user_test'));
        $post->setContent('I don t care about the lorem ipsum, i faked the content by myself!');
        $post->addTag($this->getReference('first_tag'));
        $post->addTag($this->getReference('second_tag'));

        $manager->persist($post);
        /** @var \AppBundle\Entity\Post $post */
        $post = new Post();
        $post->setPage(1);
        $post->setMenu(1);
        $post->setTitle('My second post');
        $post->setAuthor($this->getReference('user_test'));
        $post->setContent('I don t care about the lorem ipsum, i faked the content by myself!');
        $post->addTag($this->getReference('second_tag'));

        $manager->persist($post);
        /** @var \AppBundle\Entity\Post $post */
        $post = new Post();
        $post->setPage(0);
        $post->setMenu(1);
        $post->setTitle('My third post');
        $post->setAuthor($this->getReference('user_test'));
        $post->setContent('I don t care about the lorem ipsum, i faked the content by myself!');
        $post->addTag($this->getReference('first_tag'));

        $manager->persist($post);
        /** @var \AppBundle\Entity\Post $post */
        $post = new Post();
        $post->setPage(0);
        $post->setMenu(0);
        $post->setTitle('My fourth post');
        $post->setAuthor($this->getReference('user_test'));
        $post->setContent('I don t care about the lorem ipsum, i faked the content by myself!');
        $post->addTag($this->getReference('first_tag'));
        $post->addTag($this->getReference('second_tag'));

        $manager->persist($post);

        $manager->flush();
    }


    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 5;
    }
}