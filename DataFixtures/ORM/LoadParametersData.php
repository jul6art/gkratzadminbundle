<?php

namespace Gkratz\AdminBundle\DataFixtures\ORM;

use AppBundle\Entity\Parameters;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 29/01/2017
 * Time: 14:52
 */
class LoadParametersData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var \AppBundle\Entity\Parameters $site */
        $site = new Parameters();
        $site->setIp('80.90.41.24');
        $site->setName('MyNew Site');
        $site->setEmail('admin@vsweb.be');
        $site->setPhone('+32 497 18 99 98');
        $site->setFacebook('vanillaskype.websolutions');
        $site->setTwitter('deepweb_be');
        $site->setMaintenance(0);
        $site->setCountTrash(1);
        $site->setDate(new \DateTime('2025-01-01'));
        $site->setGoogleMapApikey('YOUR_APIKEY');
        $site->setGoogleMapMainAddress('rue des guillemins 1, 400 Liège');

        $manager->persist($site);
        $manager->flush();
    }


    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}