<?php

namespace Gkratz\AdminBundle\DataFixtures\ORM;

use AppBundle\Entity\Tag;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 29/01/2017
 * Time: 14:52
 */
class LoadTagData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var \AppBundle\Entity\Tag $tag */
        $tag = new Tag();
        $tag->setName('the first with a long name tag');

        $manager->persist($tag);
        $this->addReference('first_tag', $tag);

        /** @var \AppBundle\Entity\Tag $tag */
        $tag = new Tag();
        $tag->setName('the second with a long name tag');

        $manager->persist($tag);
        $this->addReference('second_tag', $tag);

        /** @var \AppBundle\Entity\Tag $tag */
        $tag = new Tag();
        $tag->setName('tag1');

        $manager->persist($tag);

        /** @var \AppBundle\Entity\Tag $tag */
        $tag = new Tag();
        $tag->setName('tag2');

        $manager->persist($tag);

        $manager->flush();
    }


    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 4;
    }
}