<?php

namespace Gkratz\AdminBundle\DataFixtures\ORM;

use AppBundle\Entity\Menu;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 29/01/2017
 * Time: 14:52
 */
class LoadMenuData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var \AppBundle\Entity\Menu $menu */
        $menu = new Menu();
        $menu->setName('Main menu');

        $manager->persist($menu);
        $this->addReference('main_menu', $menu);

        /** @var \AppBundle\Entity\Menu $menu */
        $menu = new Menu();
        $menu->setName('Secondary menu');

        $manager->persist($menu);

        $manager->flush();
    }


    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 6;
    }
}