<?php

namespace Gkratz\AdminBundle\DataFixtures\ORM;

use AppBundle\Entity\Message;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 29/01/2017
 * Time: 14:52
 */
class LoadMessageData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var \AppBundle\Entity\User $userTest */
        $userTest = $this->getReference('user_test');
        /** @var \AppBundle\Entity\User $userUser */
        $userUser = $this->getReference('user_user');
        /** @var \AppBundle\Entity\User $userAdmin */
        $userAdmin = $this->getReference('user_admin');

        /** @var \DateTime $date */
        $date = new \DateTime('2016-10-10');

        /** @var \AppBundle\Entity\Message $message */
        $message = new Message();
        $message->setMessage('Hello user');
        $message->setSender($userTest);
        $message->setRecipient($userUser);
        $date->add(new \DateInterval('P1M'));
        $messageDate = clone $date;
        $message->setDate($messageDate);

        $manager->persist($message);

        /** @var \AppBundle\Entity\Message $message */
        $message = new Message();
        $message->setMessage('Hello test');
        $message->setSender($userUser);
        $message->setRecipient($userTest);
        $date->add(new \DateInterval('P1M'));
        $messageDate = clone $date;
        $message->setDate($messageDate);

        $manager->persist($message);

        /** @var \AppBundle\Entity\Message $message */
        $message = new Message();
        $message->setMessage('How are you?');
        $message->setSender($userTest);
        $message->setRecipient($userUser);
        $date->add(new \DateInterval('P1M'));
        $messageDate = clone $date;
        $message->setDate($messageDate);

        $manager->persist($message);

        /** @var \AppBundle\Entity\Message $message */
        $message = new Message();
        $message->setMessage('fine and you?');
        $message->setSender($userUser);
        $message->setRecipient($userTest);
        $date->add(new \DateInterval('P1M'));
        $messageDate = clone $date;
        $message->setDate($messageDate);

        $manager->persist($message);

        $manager->flush();
    }


    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 8;
    }
}