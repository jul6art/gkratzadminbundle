<?php

namespace Gkratz\AdminBundle\DataFixtures\ORM;

use AppBundle\Entity\MenuItem;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 29/01/2017
 * Time: 14:52
 */
class LoadMenuItemData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var \AppBundle\Entity\Menu $menu */
        $menu = $this->getReference('main_menu');

        /** @var \AppBundle\Entity\MenuItem $menuItem1 */
        $menuItem1 = new MenuItem();
        $menuItem1->setName('section 1');
        $menuItem1->setIsSection(1);
        $menuItem1->setIsRemovable(1);
        $menuItem1->setOrder(1);
        $menuItem1->setMenu($menu);

        $manager->persist($menuItem1);

        /** @var \AppBundle\Entity\MenuItem $menuItem2 */
        $menuItem2 = new MenuItem();
        $menuItem2->setName('section 2');
        $menuItem2->setIsSection(1);
        $menuItem2->setIsRemovable(1);
        $menuItem2->setOrder(4);
        $menuItem2->setMenu($menu);

        $manager->persist($menuItem2);

        /** @var \AppBundle\Entity\MenuItem $menuItem3 */
        $menuItem3 = new MenuItem();
        $menuItem3->setName('section 3');
        $menuItem3->setIsSection(1);
        $menuItem3->setIsRemovable(1);
        $menuItem3->setOrder(6);
        $menuItem3->setMenu($menu);

        $manager->persist($menuItem3);

//        /** @var \AppBundle\Entity\MenuItem $menuItem4 */
//        $menuItem4 = new MenuItem();
//        $menuItem4->setPath('page');
//        $menuItem4->setName('page');
//        $menuItem4->setIcon('fa-desktop');
//        $menuItem4->setIsSection(0);
//        $menuItem4->setIsRemovable(1);
//        $menuItem4->setParentMenu($menuItem3);
//        $menuItem4->setOrder(7);
//        $menuItem4->setMenu($menu);
//
//        $manager->persist($menuItem4);
//
//        /** @var \AppBundle\Entity\MenuItem $menuItem5 */
//        $menuItem5 = new MenuItem();
//        $menuItem5->setPath('parent_page');
//        $menuItem5->setName('parentPage');
//        $menuItem5->setIcon('fa-desktop');
//        $menuItem5->setIsSection(0);
//        $menuItem5->setIsRemovable(1);
//        $menuItem5->setParentMenu($menuItem2);
//        $menuItem5->setOrder(5);
//        $menuItem5->setMenu($menu);
//
//        $manager->persist($menuItem5);
//
//        /** @var \AppBundle\Entity\MenuItem $childMenuItem */
//        $childMenuItem = new MenuItem();
//        $childMenuItem->setPath('child_page');
//        $childMenuItem->setName('childPage');
//        $childMenuItem->setIcon('fa-desktop');
//        $childMenuItem->setIsSection(0);
//        $childMenuItem->setIsRemovable(1);
//        $childMenuItem->setParentMenu($menuItem1);
//        $childMenuItem->setOrder(2);
//        $childMenuItem->setMenu($menu);
//
//        $manager->persist($childMenuItem);
//
//        /** @var \AppBundle\Entity\MenuItem $grandChildMenuItem */
//        $grandChildMenuItem = new MenuItem();
//        $grandChildMenuItem->setPath('gd_child_page');
//        $grandChildMenuItem->setName('grandChildPage');
//        $grandChildMenuItem->setIcon('fa-desktop');
//        $grandChildMenuItem->setIsSection(0);
//        $grandChildMenuItem->setIsRemovable(1);
//        $grandChildMenuItem->setParentMenu($childMenuItem);
//        $grandChildMenuItem->setOrder(3);
//        $grandChildMenuItem->setMenu($menu);
//
//        $manager->persist($grandChildMenuItem);

        $manager->flush();
    }


    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 7;
    }
}