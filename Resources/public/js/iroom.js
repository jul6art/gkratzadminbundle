$(document).ready(function(){
	menuResponsive();
    setTimeout(animePage, 950);
    setTimeout(animePageSuite, 1000);
    animeLiens();
    mainfooter();
    scrollToTop();
	pluginFilters();
	selectTexte();
	menuEffect();
	password();
	toggleSlideFilters();
	mySelectSelects();

	//setInterval(texteShuffle, 10000 );
    // setTimeout(replace, 1000);
    // resizx();
});

function menuResponsive(){
	/* Quand je clique sur l'icône hamburger je rajoute une classe au body */
    $('#nav-icon2').click(function(e){
        e.preventDefault();
        $('body').toggleClass('with--volet');
		$(this).toggleClass('open');
    });
    
	/* Quand je clique sur l'icône hamburger je rajoute une classe au body */
    $('#nav-icon3').click(function(e){
        e.preventDefault();
		setTimeout(function(){$('#nav-icon3').toggleClass('open');}, 1000);
        $('body').toggleClass('with--active--volet');
    });
    
	/* Quand je clique sur l'icône hamburger je rajoute une classe au body */
    $('#nav-icon1').click(function(e){
        e.preventDefault();
        $('.main-aside').toggleClass('active-sidebar');
		$(this).toggleClass('open');
		$(this).toggleClass('active-sidebar');
    });

    /* Je veux pouvoir masquer le menu si on clique sur le cache */
    $('#site-cache').click(function(e){
        $('body').removeClass('with--sidebar');
        $('#nav-icon1').removeClass('open');
        $('#nav-icon2').removeClass('open');
    })
}

function animePage(){
    $('.animation-loading').fadeOut(50);
}

function mainfooter(){
    $('.main-footer').click(function(){
        $('.main-footer').toggleClass('active');
    });
}

function animePageSuite(){
    $('.animation-loading').hide();
    $('body').removeClass('animated');
}

function animeLiens(){
    $('.link-animated .header a').click(function(e){
        a = $(this);
        if (a.attr('target') != '_blank'){
            e.preventDefault();
    		$('.animation-loading').fadeIn(50);
    		$('body').addClass('animated');
			$('.animation-link').delay(300).fadeOut(300, function () {
				window.location.pathname = a.attr('href');
			});
        }
    });
}

function scrollToTop(){
    $(window).bind('scroll', function(){
		if($(this).scrollTop() > 500) {
		$("#scroll-to-top").removeClass('hidden');
		}
		if($(this).scrollTop() < 499){
			$("#scroll-to-top").addClass('hidden');
		}
	});

	$('#scroll-to-top').click(function(){
		$("html, body").animate({scrollTop:0}, 'slow');
	});
}

function pluginFilters(){
	$('.filters-button').click(function(e){
		e.preventDefault();
		var categorie = $(this).data('filtre');
		if (categorie == 'all'){
			$('.filters-display').slideDown(300);
		}else{
			$('.filters-display:not(.'+categorie+')').slideUp(300);
			$('.filters-display.'+categorie).slideDown(300);
		}
	});
}

function selectTexte(){
	var clip = function(el) {
		var range = document.createRange();
		range.selectNodeContents(el);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
	};
	$('.code-to-select').click(function(){ 
			clip(this);
	});
}

function menuEffect(){
	$('#marquerTousLus').click(function(e){
		e.preventDefault();
		$('article.new').removeClass('new');
	});
}

function password(){
	$('input[type="password"]').addClass('password').after('<span class="addon"><i class="fa fa-eye"></i></span>');
	$('span.addon').click(function(){
		bouton = $(this);
		icone = $(this).children('i');
		input = $(this).prev('input.password');
		if (input.attr('type') == 'password'){
			input.attr('type', 'text');
		}else{
			input.attr('type', 'password');
		}
		icone.toggleClass('fa-eye');
		icone.toggleClass('fa-eye-slash');
	});
}

function toggleSlideFilters(){
	$('.filters-toggle-button').click(function(e){
		e.preventDefault();
		$('.filter-form').toggleClass('slideUp');
		if($('.filter-form').hasClass('slideUp')){
			$('.filters-toggle-button').html('<i class="fa fa-chevron-down"></i>');
		} else {
			$('.filters-toggle-button').html('<i class="fa fa-chevron-up"></i>');
		}
	});
}

function mySelectSelects(){
	$('.gk-select2').select2();
	$('.integerToRangeInput').attr('type', 'range');
}
//function texteShuffle(){
//	var texte=$('.texte-cible').text();
//	$('.texte-cible').shuffleText(texte);
//}

// function replace(){
// 	$('input.datepicker').each(function(){
// 		var left = $(this).offset().left - 25;
// 		var top = $(this).offset().top - 33;
// 		$(this).next('div#date_root').css('left', left).css('top', top);
// 	});
// }
//
// function resizx(){
// 	$(window).resize(function(){
// 		replace();
// 	});
// }