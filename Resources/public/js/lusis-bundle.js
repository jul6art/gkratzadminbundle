/**
 * Created by gkratz on 28-11-16.
 */
$(document).ready(function(){
    manageLusisForm();
});

function manageLusisForm(){
    //init
    var form = $('form.lusis-trans-form');
    var locale = $('td.locale').text();
    locale = locale.slice(0, 2);

    //control submittion
    form.submit(function(e){
        //block submission
        e.preventDefault();

        //reset error message
        $("p.lusis-trans-error").remove();

        //define form
        var formSubmitted = $(this);

        //get parameters
        var url = formSubmitted.attr('action');
        var datastring = formSubmitted.serialize();

        //ajax call
        $.ajax({
            type: "POST",
            url: url,
            data: datastring,
            success: function(data) {
                if (data.httpcode >= 200){
                    var delayNotif;
                    if(data.httpcode == 200){
                        classCss = 'success';
                        delayNotif = 1000;
                    }else{
                        classCss = 'error';
                        delayNotif = 8000;
                    }
                    textNotif = '<p class="lusis-trans-' + classCss + '">STATUS ' + data.httpcode + ': ' + data.message + '</p>';
                    formSubmitted.before(textNotif);
                    textNotif = $('p.lusis-trans-' + classCss + '');
                    textNotif.fadeIn(600).delay(delayNotif).fadeOut(1500);
                    if(data.httpcode == 200){
                        setTimeout(reloadPage, 1000);
                    }
                }
            },
            error: function(){
                textError = '<p class="lusis-trans-error">STATUS 605: Error processing the ajax call.</p>';
                formSubmitted.before(textError);
                textError = $('p.lusis-trans-error');
                textError.fadeIn(600).delay(8000).fadeOut(1500);
            }
        });
    });
}

function reloadPage(){
    var targetUrl = $('#summary .status .container .break-long-words a').attr('href');
    window.location.href = targetUrl;
}
