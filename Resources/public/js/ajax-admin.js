
$(document).ready(function(){
    modalFormAjax();
    userAdminizeAjax();
    backupAjax();
    maintenanceStateSwitchAjax();
    maintenanceIpAjax();
    maintenanceCacheAjax();
    maintenanceSearchAjax();
    multiActions();
    resetFilter();
    strictSearchAjax();
});


function modalFormAjax(){
    $('div.modal-form form').submit(function (e) {
        e.preventDefault();
        removeAlerts($(this));
        url = $(this).attr('action');
        data = $(this).serialize();

        requestResponseAjax(url, data, $(this));
    });
}


function userAdminizeAjax() {
    $('.single-admin-button').click(function (e) {
        if(!$(this).hasClass('disabled')){
            e.preventDefault();
            url = $(this).attr('href');
            requestResponseAjax(url, null, $(this));
        }
    });
}


function strictSearchAjax() {
    $('.large-search-button').click(function (e) {
        e.preventDefault();
        url = $(this).attr('href');
        requestResponseAjax(url, null, $(this));
    });
}


function backupAjax() {
    $('.single-backup-button').click(function (e) {
        e.preventDefault();
        url = $(this).attr('href');
        requestResponseAjax(url, null, $(this));
    });
}


function maintenanceStateSwitchAjax(){
    $('.switch_maintenance_state').click(function(){
        if ($(this).is(':checked')){
            var url = $(this).parent().data('off');
        } else {
            var url = $(this).parent().data('on');
        }

        requestResponseAjax(url, null, $(this));
    });
}


function maintenanceSearchAjax(){
    $('.maintenance_search').click(function(e){
        e.preventDefault();
        var url = $(this).attr('href');

        requestResponseAjax(url, null, $(this));
    });
}


function maintenanceIpAjax(){
    $('.maintenance_ip').click(function(e){
        e.preventDefault();
        var url = $(this).attr('href');

        requestResponseAjax(url, null, $(this));
    });
}


function maintenanceCacheAjax(){
    $('.maintenance_cache').click(function(e){
        e.preventDefault();
        var url = $(this).attr('href');

        requestResponseAjax(url, null, $(this));
    });
}


function addWaiter(){
    var $waiter = '<div class="sk-cube-grid">' +
        '<div class="sk-cube sk-cube1"></div>' +
        '<div class="sk-cube sk-cube2"></div>' +
        '<div class="sk-cube sk-cube3"></div>' +
        '<div class="sk-cube sk-cube4"></div>' +
        '<div class="sk-cube sk-cube5"></div>' +
        '<div class="sk-cube sk-cube6"></div>' +
        '<div class="sk-cube sk-cube7"></div>' +
        '<div class="sk-cube sk-cube8"></div>' +
        '<div class="sk-cube sk-cube9"></div>' +
        '</div>';
    $('.site-container').after($waiter);
}


/**
 * @param url
 * @param data
 * @param obj
 */
function requestResponseAjax(url, data, obj){
    addWaiter();
    $.ajax({
        method: "POST",
        url: url,
        data: data
    }).done(function(response){
        $('.sk-cube-grid').remove();
        manageSuccessAjaxResponse(response, url);
    }).error(function(){
        $('.sk-cube-grid').remove();
        messageTimed(url, 3, 'ERROR AJAX CALL');
    });
}


/**
 * @param form
 */
function removeAlerts(form){
    $("form[action='" + form.attr('action') + "'] p.error").remove();
    $("form[action='" + form.attr('action') + "'] input.error").removeClass("error");
}


/**
 * @param response
 * @param url
 */
function manageSuccessAjaxResponse(response, url){
    switch (response.type) {
        case 'maintenance_switch_state':
            var label = $('#switch_state_label');
            if(response.error == 0){
                if(response.state == 1){
                    label.text(label.data('off'));
                } else {
                    label.text(label.data('on'));
                }
                messageTimedBox1(url, 2, response.message);
            } else {
                messageTimedBox1(url, 3, response.message);
            }
            break;
        case 'maintenance_cache':
            if(response.error == 0){
                $('#maintenance_cache_label').val(response.size);
                messageTimedBox1(url, 2, response.message);
            } else {
                messageTimedBox1(url, 3, response.message);
            }
            break;
        case 'maintenance_search':
            if(response.error == 0){
                messageTimedBox1(url, 2, response.message);
            } else {
                messageTimedBox1(url, 3, response.message);
            }
            break;
        case 'maintenance_ip':
            if(response.error == 0){
                $('#maintenance_ip_label').val(response.ip);
                messageTimedBox1(url, 2, response.message);
            } else {
                messageTimedBox1(url, 3, response.message);
            }
            break;
        case 'maintenance_date':
            if(response.error == 0){
                $('#maintenance_date_label').text(response.date);
                messageTimedBox1(url, 2, response.message);
            } else {
                messageTimedBox1(url, 3, response.message);
            }
            break;
        case 'json_form':
            manageFormReturn(response, url);
            break;
        case 'multiple_delete':
            if(response.error == 0){
                messageTimedBox1(url, 2, response.message);
                afficheCountTrash(response.countTrash);
                removeItems(response.ids);
                myReload(true);
            } else {
                messageTimedBox1(url, 3, response.message);
            }
            break;
        case 'multiple_manage':
            if(response.error == 0){
                messageTimedBox1(url, 2, response.message);
                afficheCountTrash(response.countTrash);
                removeItems(response.ids);
                myReload(true);
            } else {
                messageTimedBox1(url, 3, response.message);
            }
            break;
        case 'user_adminize':
            if(response.error == 0){
                messageTimedBox1(url, 2, response.message);
                targetListElement = $('.databaseItem[data-id="'+response.user+'"]');
                targetListElement.data('admin', response.state);
                targetListElement.data('adminurl', response.url);
                if(response.state == 1){
                    targetListElement.children().children('i').removeClass('fa-user-secret').addClass('fa-user');
                } else {
                    targetListElement.children().children('i').removeClass('fa-user').addClass('fa-user-secret');
                }
                myReload(true);
            } else {
                messageTimedBox1(url, 3, response.message);
            }
            break;
        case 'add_backup':
            if(response.error == 0){
                myReload(null);
            } else {
                messageTimedBox1(url, 3, response.message);
            }
            break;
        case 'large_search':
            if(response.error == 0){
                window.location = response.url;
            }
    }
}


/**
 * @param count
 */
function afficheCountTrash(count){
    if (count > 0){
        $('#trashCountMenuItem').html('<i class="fa fa-trash fa-2x"></i> <span class="badge">' + count + '</span>');
    } else {
        $('#trashCountMenuItem').html('<i class="fa fa-trash fa-2x"></i> ');
        $('.btn-bar').remove();
        $('.trash-content').html('<div class="text-center">'+
            '<p class="error">' + $('.trash-content').data('message') + '</p>'+
        '</div>');
    }
}


/**
 * @param ids
 */
function removeItems(ids){
    idsArray = ids.split(',');
    for(i = 0; i < idsArray.length; i++){
        $('.databaseItem[data-id="'+idsArray[i]+'"]').remove();
    }
}


/**
 * @param response
 * @param url
 */
function manageFormReturn(response, url){
    if(response.error == 1){
        classname = response.entity.split('Entity\\');
        var prefix = 'appbundle_' + classname[1].toLowerCase() + '_';
        $.each(response.errors, function(key, value){
            $('#' + prefix + key).addClass('error').next('span').after('<p class="error">' + value + '</p>');
        });
        messageTimed(url, 3, response.message);
    } else {
        messageTimed(url, 2, response.message);
    }
}


function uniqid(){
    var n = Math.floor(Math.random()*110000);
    return parseInt(n);
}


/**
 * @param url
 * @param boxClass
 * @param message
 */
function messageTimed(url, boxClass, message){
    m = uniqid();
    $('.tn-box').remove();
    $('.site-container').after('<div id="to-remove' + m + '" class="tn-box tn-box-color-' + boxClass + '">'+
        '<p>' + message + '</p>'+
        '<div class="tn-progress"></div>'+
        '</div>');
    $('#to-remove' + m).addClass('tn-box-active');
    setTimeout(function(m){
        $('#to-remove' + m).remove();
    }, 11000);
}


/**
 * @param url
 * @param boxClass
 * @param message
 */
function messageTimedBox1(url, boxClass, message){
    m = uniqid();
    $('.tn-box').remove();
    $('.site-container').after('<div id="to-remove' + m + '" class="tn-box tn-box-color-' + boxClass + '">'+
        '<p>' + message + '</p>'+
        '<div class="tn-progress"></div>'+
        '</div>');
    $('#to-remove' + m).addClass('tn-box-active');
    setTimeout(function(m){
        $('#to-remove' + m).remove();
    }, 11000);
}


/**
 * @param $elem
 */
function enable($elem){
    $elem.removeClass('disabled');
}


/**
 * @param $elem
 * @param $url
 */
function enablePlus($elem, $url){
    $elem.removeClass('disabled');
    $elem.attr('href', $url);
}


/**
 * @param $elem
 */
function disable($elem){
    $elem.addClass('disabled');
}


/**
 * @param $elem
 */
function disablePlus($elem){
    $elem.addClass('disabled');
    $elem.removeAttr('href');
}


/**
 * @param $bool
 */
function myReload($bool){
    if($bool == true){
        if($('.table-responsive #databaseListItems').length > 0){
            window.location = window.location.href;
        }
    } else {
        window.location = window.location.href;
    }
}


function resetFilter(){
    $('#reset[type="reset"]').click(function(e){
        $(this).closest('form')
            .find(':radio, :checkbox').removeAttr('checked').end()
            .find('textarea, :text, select').val('');
        $(".select2").val('').trigger('change');
        $('input[name="analytic_filter[date][left_date]_submit"], ' +
            'input[name="analytic_filter[date][right_date]_submit"], ' +
            'input[name="analytic_filter_date_left_date"], ' +
            'input[name="analytic_filter_date_right_date"]').val('');
        return false;
    });
}


/**
 *
 * @param $array
 * @param $input
 * @param $form
 * @param $button
 * @param $action
 * @param $bool
 */
function multipleSubmit($array, $input, $form, $button, $action, $bool){
    $button.click(function(e){
        e.preventDefault();
        $('.databaseItem').each(function(){
            if($bool == true){
                if($(this).hasClass('selected')){
                    $array.push($(this).data('id'));
                }
            } else {
                $array.push($(this).data('id'));
            }
        });
        $input.val($array);
        $form.attr('action', $action);
        $form.submit();
    });
}


/**
 *
 * @param $array
 * @param $input
 * @param $form
 * @param $action
 * @param $bool
 */
function multipleContextSubmit($array, $input, $form, $action, $bool){
    $('.databaseItem').each(function(){
        if($bool == true){
            if($(this).hasClass('selected')){
                $array.push($(this).data('id'));
            }
        } else {
            $array.push($(this).data('id'));
        }
    });
    $input.val($array);
    $form.attr('action', $action);
    $form.submit();
    disablePlus($('.single-edit-button'));
    disablePlus($('.single-admin-button'));
    disablePlus($('.single-view-button'));
    disablePlus($('.single-download-button'));
    disable($('.single-delete-button'));
}


function multiActions(){
    $('.single-delete-button').click(function (e) {
        e.preventDefault();
        if(!$(this).hasClass('disabled')){
            multipleContextSubmit([], $('#multiple-delete input[name="appbundle_multiple_actions[ids]"]'), $('#multiple-delete'), $('#multiple-delete').attr('action'), true);
        }
    });
    $('.single-permanent-delete-button').click(function (e) {
        e.preventDefault();
        if(!$(this).hasClass('disabled')){
            multipleContextSubmit([], $('#multiple-restore input[name="appbundle_multiple_actions[ids]"]'), $('#multiple-restore'), $actionBasePieces[0] + "/2", true);
        }
    });
    $('.single-restore-button').click(function (e) {
        e.preventDefault();
        if(!$(this).hasClass('disabled')){
            multipleContextSubmit([], $('#multiple-restore input[name="appbundle_multiple_actions[ids]"]'), $('#multiple-restore'), $actionBasePieces[0] + "/0", true);
        }
    });

    //view an element
    $('.databaseItem').dblclick(function(e){
        if($(this).is("[data-viewurl]")){
            e.preventDefault();
            $('.databaseItem').removeClass('selected');
            $(this).addClass('selected');
            window.location.pathname = $(this).data('viewurl');
        }
    });

    //select elements
    $('.databaseItem').click(function(e){
        e.preventDefault();

        if(e.shiftKey) {
            var i = $(this).parent().children('.selected:first').index();
            var i2 = $(this).index();
            ii = i;
            ii2 = i2;
            if (i > i2){
                ii = i2;
                ii2 = i;
            }
            while(ii <= ii2){
                $(this).parent().children(':eq(' + ii + ')').addClass('selected');
                ii += 1;
            }
        }else if (e.ctrlKey) {
            $(this).toggleClass('selected');
        } else {
            $('.databaseItem').removeClass('selected');
            $(this).addClass('selected');
        }

        $selectedElements = $('.databaseItem.selected');
        if ($selectedElements.length === 1){
            enablePlus($('.single-edit-button'), $('.databaseItem.selected').data('editurl'));
            enablePlus($('.single-admin-button'), $('.databaseItem.selected').data('adminurl'));
            enablePlus($('.single-view-button'), $('.databaseItem.selected').data('viewurl'));
            enablePlus($('.single-download-button'), $('.databaseItem.selected').data('downloadurl'));
        } else {
            disablePlus($('.single-edit-button'));
            disablePlus($('.single-admin-button'));
            disablePlus($('.single-view-button'));
            disablePlus($('.single-download-button'));
        }
        if ($selectedElements.length >= 1) {
            enable($('.single-delete-button'));
            enable($('.single-restore-button'));
            enable($('.single-permanent-delete-button'));
        } else {
            disable($('.single-delete-button'));
            disable($('.single-restore-button'));
            disable($('.single-permanent-delete-button'));
        }
    });

    $(window).click(function(e){
        if(($(e.target).closest('.databaseItem').length === 0) && ($(e.target).closest('.single-edit-button').length === 0)
        && ($(e.target).closest('.single-delete-button').length === 0) && ($(e.target).closest('.single-admin-button').length === 0)
        && ($(e.target).closest('.single-permanent-delete-button').length === 0) && ($(e.target).closest('.single-restore-button').length === 0)
        && ($(e.target).closest('.single-download-button').length === 0) && ($(e.target).closest('.single-view-button').length === 0)){
            $('.databaseItem').removeClass('selected');
            disablePlus($('.single-edit-button'));
            disablePlus($('.single-admin-button'));
            disablePlus($('.single-view-button'));
            disablePlus($('.single-download-button'));
            disable($('.single-delete-button'));
            disable($('.single-restore-button'));
            disable($('.single-permanent-delete-button'));
        }
    });

    //multiple delete
    multipleSubmit([], $('#multiple-delete input[name="appbundle_multiple_actions[ids]"]'), $('#multiple-delete'), $('#multiple-delete-button'), $('#multiple-delete').attr('action'), true);

    if($('#multiple-restore').length > 0){
        var $actionBasePieces = $('#multiple-restore').data('action').split("/0");

        //multiple restore
        multipleSubmit([], $('#multiple-restore input[name="appbundle_multiple_actions[ids]"]'), $('#multiple-restore'), $('#multiple-restore-button'), $actionBasePieces[0] + "/0", true);

        //multiple permanent delete
        multipleSubmit([], $('#multiple-restore input[name="appbundle_multiple_actions[ids]"]'), $('#multiple-restore'), $('#multiple-permanent-delete-button'), $actionBasePieces[0] + "/2", true);
        multipleSubmit([], $('#multiple-restore input[name="appbundle_multiple_actions[ids]"]'), $('#multiple-restore'), $('#multiple-permanent-delete-all-button'), $actionBasePieces[0] + "/2", false);
    }
}