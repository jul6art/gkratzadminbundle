<?php

namespace Gkratz\AdminBundle\Controller\Maintenance;

use AppBundle\Entity\Search;
use Gkratz\AdminBundle\Controller\StandardController;
use Gkratz\AdminBundle\Form\MaintenanceDateType;
use Gkratz\AdminBundle\Form\MaintenanceType;
use AppBundle\Entity\Parameters;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 28/01/2017
 * Time: 16:41
 */
/**
 * @Route("/maintenance")
 */
class MaintenanceController extends StandardController
{
    /**
     * @Route("/")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \AppBundle\Entity\Parameters $maintenance */
        $maintenance = $em->getRepository(Parameters::class)->find(1);
        $form = $this->createForm(MaintenanceType::class, $maintenance, array());
        $form2 = $this->createForm(MaintenanceDateType::class);
        $form->handleRequest($request);
        $form2->handleRequest($request);

        if('POST' === $request->getMethod()) {
            if($form->isSubmitted()){
                if($form->isValid()){
                    $maintenance->upload();
                    $this->getDoctrine()->getManager()->flush();
                    $request->getSession()
                        ->getFlashBag()
                        ->add('success', $this->get('translator')->trans('The main settings were successfully updated'))
                    ;
                    return $this->redirect($this->generateUrl('gkratz_admin_maintenance_maintenance_index'));
                } else {
                    $request->getSession()
                        ->getFlashBag()
                        ->add('error', $this->get('translator')->trans('The request parameters are not good'))
                    ;
                }
            }
            if($form2->isSubmitted()){
                if($form2->isValid()){
                    try {
                        $date = new \DateTime($form2->getData()['date']->format('Y-m-d') . 'T' . $form2->getData()['time']);
                        $maintenance->setDate($date);
                        $em->flush();
                        $request->getSession()
                            ->getFlashBag()
                            ->add('success', $this->get('translator')->trans('The main settings were successfully updated'))
                        ;
                        return $this->redirect($this->generateUrl('gkratz_admin_maintenance_maintenance_index'));
                    } catch (\Exception $e){
                        $request->getSession()
                            ->getFlashBag()
                            ->add('error', $this->get('translator')->trans('The request parameters are not good'))
                        ;
                    }
                } else {
                    $request->getSession()
                        ->getFlashBag()
                        ->add('error', $this->get('translator')->trans('The request parameters are not good'))
                    ;
                }
            }

         }

        $realCacheDir = $this->getParameter('kernel.cache_dir');

        $cacheSize = $this->get('gkratz_files_sizer')->sizeFolder($realCacheDir);

        return $this->render('@GkratzAdmin/admin/maintenance/index.html.twig', array(
            'cache_size' => $cacheSize,
            'maintenance' => $form->createView(),
            'dateForm' => $form2->createView()
        ));
    }

    /**
     * @Route("/state/edit/{state}")
     * @Method({"POST"})
     * @param $state
     * @param Request $request
     * @return JsonResponse
     */
    public function maintenanceSwitchAction($state, Request $request)
    {
        $jsonResponse = array(
            'error' => 1,
            'type' => 'maintenance_switch_state'
        );
        if(!$request->isXmlHttpRequest()){
            $jsonResponse['message'] = $this->get('translator')->trans('The request must be asyncronous');
        } else {
            /** @var  $em \Doctrine\ORM\EntityManager */
            $em = $this->getDoctrine()->getManager();
            /** @var  $maintenance \AppBundle\Entity\Parameters */
            $maintenance = $em->getRepository(Parameters::class)->find(1);
            if(null == $maintenance){
                $jsonResponse['message'] = $this->get('translator')->trans("The line ID 1 of the maintenance table doesn't exist");
            } else {
                $maintenance->setMaintenance($state);
                $em->flush();
                $jsonResponse['error'] = 0;
                $jsonResponse['state'] = (int) $maintenance->getMaintenance();
                $jsonResponse['message'] = $this->get('translator')->trans('The state of the maintenance was successfully updated');
            }
        }
        return new JsonResponse($jsonResponse);
    }


    /**
     * @Route("/cache")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function maintenanceCacheAction(Request $request)
    {
        $jsonResponse = array(
            'error' => 1,
            'type' => 'maintenance_cache'
        );
        if(!$request->isXmlHttpRequest()){
            $jsonResponse['message'] = $this->get('translator')->trans('The request must be asyncronous');
        } else {
            try{
                $filesystem   = $this->get('filesystem');
                $realCacheDir = $this->getParameter('kernel.cache_dir');
                $this->get('cache_clearer')->clear($realCacheDir);
                $filesystem->remove($realCacheDir);
                $jsonResponse['error'] = 0;
                $jsonResponse['size'] = "0 Ko";
                $jsonResponse['message'] = $this->get('translator')->trans('Cache successfully deleted');
            }catch(\Exception $e){
                $jsonResponse['message'] = $this->get('translator')->trans('Error deleting the cache');
            }
        }
        return new JsonResponse($jsonResponse);
    }


    /**
     * @Route("/search/reset")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function maintenanceSearchAction(Request $request)
    {
        $jsonResponse = array(
            'error' => 1,
            'type' => 'maintenance_search'
        );
        if(!$request->isXmlHttpRequest()){
            $jsonResponse['message'] = $this->get('translator')->trans('The request must be asyncronous');
        } else {
            try{
                /** @var \Doctrine\ORM\EntityManager $em */
                $em = $this->getDoctrine()->getManager();

                $results = $em->getRepository(Search::class)->findAll();
                foreach ($results as $result){
                    $em->remove($result);
                }

                $em->flush();

                $jsonResponse['error'] = 0;
                $jsonResponse['message'] = $this->get('translator')->trans('Entities successfully permanent deleted');
            }catch(\Exception $e){
                $jsonResponse['message'] = $this->get('translator')->trans('Error deleting the search results');
            }
        }
        return new JsonResponse($jsonResponse);
    }

    /**
     * @Route("/ip/edit")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function maintenanceIpAction(Request $request)
    {
        $jsonResponse = array(
            'error' => 1,
            'type' => 'maintenance_ip'
        );
        if(!$request->isXmlHttpRequest()){
            $jsonResponse['message'] = $this->get('translator')->trans('The request must be asyncronous');
        } else {
            /** @var  $em \Doctrine\ORM\EntityManager */
            $em = $this->getDoctrine()->getManager();
            /** @var  $maintenance \AppBundle\Entity\Parameters */
            $maintenance = $em->getRepository(Parameters::class)->find(1);
            if(null == $maintenance){
                $jsonResponse['message'] = $this->get('translator')->trans("The line ID 1 of the maintenance table doesn't exist");
            } else {
                $ip= $this->get('gkratz_analytic_geoloc_ip')->getIp();
                $maintenance->setIp($ip);
                $em->flush();
                $jsonResponse['error'] = 0;
                $jsonResponse['message'] = $this->get('translator')->trans('The exception IP was successfully updated');
                $jsonResponse['ip'] = $maintenance->getIp() . ' ' .
                    $this->get('gkratz_analytic_geoloc_ip')->lookupCity($maintenance->getIp(), $request->getLocale()) .
                    ', ' . $this->get('gkratz_analytic_geoloc_ip')->lookup($maintenance->getIp(), $request->getLocale())
                ;
            }
        }
        return new JsonResponse($jsonResponse);
    }
}