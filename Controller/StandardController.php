<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 28/01/2017
 * Time: 22:00
 */

namespace Gkratz\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\TranslatorInterface;

class StandardController extends Controller
{
    /**
     * @param FormInterface $form
     * @return array
     */
    protected function getErrorMessages(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors(true, false) as $error) {
            $errors[$error->getForm()->getName()] = $error->current()->getMessage();
        }
        return $errors;
    }
}