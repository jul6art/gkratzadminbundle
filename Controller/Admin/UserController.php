<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 09-02-17
 * Time: 14:48
 */

namespace Gkratz\AdminBundle\Controller\Admin;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/user")
 * Class UserController
 * @package Gkratz\AdminBundle\Controller\Admin
 */
class UserController extends AdminController
{
    protected function getClassFilterForm()
    {
        return \Gkratz\AdminBundle\FormFilter\UserFilterType::class;
    }

    protected function getClassForm()
    {
        return \Gkratz\AdminBundle\Form\UserType::class;
    }

    protected function getClass()
    {
        return 'AppBundle\Entity\User';
    }

    protected function getClassShortName()
    {
        return 'User';
    }

    protected function getClassName()
    {
        return \AppBundle\Entity\User::class;
    }

    protected function getName()
    {
        return 'user';
    }

    protected function getLabel()
    {
        return 'User';
    }

    /**
     * @Route("/add")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        $class = $this->getClassName();
        $entity = new $class();

        $form = $this->createForm($this->getClassForm(), $entity, array('readonly' => false));
        $form->handleRequest($request);
        if($form->isSubmitted()){
            if ($form->isValid()) {
                $entity->setPlainPassword(uniqid());
                $entity->setEnabled(1);
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'success', $this->get('translator')->trans('Entity successfully added')
                );
                return $this->redirectToRoute($this->getRoutePrefix().$this->getName().'_index');
            } else {
                $this->get('session')->getFlashBag()->add(
                    'error', $this->get('translator')->trans('The request parameters are not good')
                );
            }
        }

        return $this->render('@GkratzAdmin/admin/add.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'addRoute' => $this->getRoutePrefix().$this->getName().'_add',
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'indexRoute' => $this->getRoutePrefix().$this->getName().'_index',
        ));
    }


    /**
     * @Route("/admin/{user}/{state}")
     * @Method({"POST"})
     * @param Request $request
     * @param User $user
     * @param $state
     * @return JsonResponse
     */
    public function adminAction(Request $request, User $user, $state)
    {
        $jsonResponse = array(
            'error' => 1,
            'type' => 'user_adminize'
        );
        if(!$request->isXmlHttpRequest()){

        } else {
            $em = $this->getDoctrine()->getManager();
            if($state == 1){
                $user->addRole('ROLE_ADMIN');
            } else {
                $user->removeRole('ROLE_ADMIN');
            }
            $em->flush();
            $jsonResponse['message'] = $this->get('translator')->trans('The role user was successfully updated');
            $jsonResponse['error'] = 0;
            $jsonResponse['state'] = ($state == 1) ? 0 : 1 ;
            $jsonResponse['user'] = $user->getId();
            $jsonResponse['url'] = $this->generateUrl('gkratz_admin_admin_user_admin', array('user' => $user->getId(), 'state' => $jsonResponse['state']));
        }
        return new JsonResponse($jsonResponse);
    }
}