<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 09/04/2017
 * Time: 00:53
 */

namespace Gkratz\AdminBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/map")
 * Class MapController
 * @package Gkratz\AdminBundle\Controller\Admin
 */
class MapController extends AdminController
{
    protected function getClassFilterForm()
    {
        return false;
    }

    protected function getClassForm()
    {
        return \Gkratz\AdminBundle\Form\MapType::class;
    }

    protected function getClass()
    {
        return 'AppBundle\Entity\Map';
    }

    protected function getClassShortName()
    {
        return 'Map';
    }

    protected function getClassName()
    {
        return \AppBundle\Entity\Map::class;
    }

    protected function getName()
    {
        return 'map';
    }

    protected function getLabel()
    {
        return 'Map';
    }
}