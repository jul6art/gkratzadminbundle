<?php

namespace Gkratz\AdminBundle\Controller\Admin;

use AppBundle\Entity\Menu;
use AppBundle\Entity\MenuItem;
use Gkratz\AdminBundle\Form\MenuItemType;
use Gkratz\AdminBundle\Form\MenuType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/menu")
 * Class MenuController
 * @package Gkratz\AdminBundle\Controller\Menu
 */
class MenuController extends AdminController
{
    protected function getClassFilterForm()
    {
        return false;
    }

    protected function getClassForm()
    {
        return false;
    }

    protected function getClass()
    {
        return 'AppBundle\Entity\Menu';
    }

    protected function getClassShortName()
    {
        return 'Menu';
    }

    protected function getClassName()
    {
        return \AppBundle\Entity\Menu::class;
    }

    protected function getName()
    {
        return 'menu';
    }

    protected function getLabel()
    {
        return 'Menu';
    }

    /**
     * @Route("/")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $displayMode = $this->getDisplayMode();
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var \AppBundle\Repository\MenuItemRepository $repo */
        $repo = $em->getRepository('AppBundle:MenuItem');

        /** @var \AppBundle\Repository\MenuRepository $menuRepo */
        $menuRepo = $this->getDoctrine()->getRepository(Menu::class);

        $freeMenus = $repo->findAllMenuWithoutOrder();


        if ($request->query->has('menuName')){
            $menuParent = $menuRepo->find($request->query->get('menuName'));
            $myMenus = $repo->findByOrderWithSubMenus($menuParent);
        } else {
            $myMenus = null;
        }

        $menuNames = $menuRepo->findBy(array(), array('name' => 'asc'));

        return $this->render('@GkratzAdmin/admin/menu/index.html.twig', array(
            'freeMenus' => $freeMenus,
            'myMenus' => $myMenus,
            'displayMode' => $displayMode,
            'menuNames' => $menuNames
        ));
    }

    /**
     * @Route("/item/{id}")
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction($id, Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        if($id){
            $m = $em->getRepository('AppBundle:MenuItem')->find($id);
            if(!$m){
                $request->getSession()->getFlashBag()->add(
                    'error',
                    $this->get('translator')->trans(
                        'Unable to find entity.'
                    ));
                return $this->redirectToRoute('gkratz_admin_admin_menu_add');
            }
            if ($m->getIsSection()){
                $section = $m;
                /** @var \AppBundle\Entity\MenuItem $menu */
                $menu = new MenuItem();
            } else{
                $menu = $m;
                /** @var \AppBundle\Entity\MenuItem $section */
                $section = new MenuItem();
            }
        }
        else{
            /** @var \AppBundle\Entity\MenuItem $menu */
            $menu = new MenuItem();
            /** @var \AppBundle\Entity\MenuItem $section */
            $section = new MenuItem();
        }

        $formMenuItem = $this->get('form.factory')->createNamedBuilder('menuItem' ,MenuItemType::class, $menu)
            ->remove('isSection')
            ->getForm();

        $formSection = $this->get('form.factory')->createNamedBuilder('menuSection' ,MenuItemType::class, $section)
            ->remove('isSection')
            ->remove('path')
            ->getForm();

        if($request->request->has("menuItem") && $formMenuItem->handleRequest($request)->isValid()){
            $em->persist($menu);
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Entity successfully edited'
                ));
            return $this->redirectToRoute('gkratz_admin_admin_menu_index');
        }

        if ($request->request->has("menuSection") && $formSection->handleRequest($request)->isValid()){
            $section->setIsSection(true);
            $em->persist($section);
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Entity successfully edited'
                ));
            return $this->redirectToRoute('gkratz_admin_admin_menu_index');
        }

        $responseParams = array(
            'formMenuItem' => $formMenuItem->createView(),
            'formSection' => $formSection->createView()
        );

        if(isset($m)){
            $responseParams['entity'] = $m;
        }

        return $this->render('@GkratzAdmin/admin/menu/item.html.twig', $responseParams);

    }

    /**
     * @Route("/new/menu")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newMenuAction(Request $request)
    {
        $entity = new Menu();

        $form = $this->createForm(MenuType::class, $entity, array());
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'success', $this->get('translator')->trans('Entity successfully added')
                );
                return $this->redirectToRoute('gkratz_admin_admin_menu_index');
            } else {
                $this->get('session')->getFlashBag()->add(
                    'error', $this->get('translator')->trans('The request parameters are not good')
                );
            }
        }

        return $this->render('@GkratzAdmin/admin/menu/menu.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/save")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveAction(Request $request)
    {
        if($request->isXmlHttpRequest()){
            /** @var \Doctrine\ORM\EntityManager $em */
            $em = $this->getDoctrine()->getManager();

            $myMenuS = $request->request->get('myMenu');
            $pattern = '#([a-z0-9 ]{0,})\[(\d{0,})\]=(null|\d{0,})#i';
            preg_match_all($pattern, $myMenuS, $matches);
            $menu = [];

            foreach ($matches as $keyM => $value) {
                if ($keyM > 0) {
                    foreach ($value as $keyV => $info) {
                        $menu[$keyV + 1][$keyM] = $info;
                    }
                }
            }

            /** @var \AppBundle\Entity\Menu $parentMenu */
            $parentMenu = $this->getDoctrine()->getRepository(Menu::class)->find($request->request->get('parentMenu'));

            $em->createQuery('update AppBundle:MenuItem m set m.order = null, m.parentMenu = null where m.order IS NOT NULL and m.menu = '.$parentMenu->getId())->execute();
            /** @var \AppBundle\Repository\MenuRepository $repo */
            $rep = $em->getRepository('AppBundle:MenuItem');
            $entity = [];
            foreach ($menu as $key => $value) {
                if (!array_key_exists($value[2], $entity)) {
                    $entity[$value[2]] = $rep->find($value[2]);
                }
                $entity[$value[2]]->setOrder($key);
                if ($value[3] != null) {
                    if (!array_key_exists($value[3], $entity)) {
                        $entity[$value[3]] = $rep->find($value[3]);
                    }
                    $entity[$value[2]]->setParentMenu($entity[$value[3]]);
                }
                $entity[$value[2]]->setMenu($parentMenu);
                $em->persist($entity[$value[2]]);
            }
            $em->flush();

            return $this->json(['type' => 'success', 'message' => $this->get('translator')->trans('Entity successfully edited'), 'title' => 'Menu sauvegarder !', 'error' => 0]);
        }
        return $this->redirectToRoute('gkratz_admin_admin_menu_index');
    }

    /**
     * @Route("/delete/{id}")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Request $request, $id)
    {
        if(!$request->isXmlHttpRequest()) return $this->redirectToRoute('gkratz_admin_admin_menu_index');
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $menu = $em->getRepository('AppBundle:MenuItem')->find($id);
        if($menu){
            $em->remove($menu);
            $em->flush();
            return $this->json(['type' => 'success', 'message' => $this->get('translator')->trans('Entity successfully deleted'), 'title' => 'Menu supprimer !', 'error' => 0]);
        }
        else{
            return $this->json(['type' => 'error', 'message' => $this->get('translator')->trans('An error occured, sorry! If the problem persist, please contact us and give us the informations to fix the problem!'), 'title' => 'Erreur !', 'error' => 1]);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction()
    {
        /** @var \AppBundle\Entity\Menu $menu */
        $menu = $this->getDoctrine()->getRepository('AppBundle:MenuItem')->find(1);

        /** @var \AppBundle\Entity\MenuItem $menus */
        $menuItems = $this->getDoctrine()->getRepository('AppBundle:MenuItem')->findByOrderWithSubMenus($menu);

        return $this->render('default/partial-menu.html.twig', array(
            'menuItems' => $menuItems,
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR
        ));
    }
}
