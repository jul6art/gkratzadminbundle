<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/11/2016
 * Time: 13:19
 */

namespace Gkratz\AdminBundle\Controller\Admin;

use AppBundle\Entity\Preferences;
use Gkratz\AdminBundle\Constants\Constants;
use Gkratz\AdminBundle\Form\MultipleActionsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/analytics")
 * Class AnalyticController
 * @package Gkratz\AdminBundle\Controller\Admin
 */
class AnalyticController extends AdminController
{
    protected function getClassFilterForm()
    {
        return \Gkratz\AdminBundle\FormFilter\AnalyticFilterType::class;
    }

    protected function getClassForm()
    {
        return null;
    }

    protected function getClass()
    {
        return 'AppBundle\Entity\Analytic';
    }

    protected function getClassShortName()
    {
        return 'Analytic';
    }

    protected function getClassName()
    {
        return \AppBundle\Entity\Analytic::class;
    }

    protected function getName()
    {
        return 'analytic';
    }

    protected function getLabel()
    {
        return 'Analytic';
    }

    /**
     * @Route("/")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $displayMode = $this->getDisplayMode();

        $multipleForm = $this->createForm(MultipleActionsType::class);

        $repository = $this->getDoctrine()->getRepository($this->getClassName());

        $itemsPerPage = Constants::ITEM_PER_PAGE;
        if ($request->query->has('items')){
            if ($request->query->get('items') > 0){
                $itemsPerPage = $request->query->get('items');
                $session = $this->get('session');
                $session->set('items', $itemsPerPage);
            }
        } else {
            if ($request->getSession()->get('items') != NULL){
                $itemsPerPage = $request->getSession()->get('items');
            } else {
                $request->getSession()->remove('items');
            }
        }
        $page = $request->query->get('page', 1);
        $sort = $this->getFieldSort();
        if ($request->query->has('sort')){
            $sort = $request->query->get('sort');
        }
        $direction = ($request->query->has('direction'))?$request->query->get('direction'):$this->getSort();

        if($displayMode == Preferences::DISPLAY_MODE_THUMBNAIL || $displayMode == Preferences::DISPLAY_MODE_THUMBNAIL_FULL_SCREEN){
            /* @var $query \Doctrine\ORM\QueryBuilder */
            $query = $repository->createQueryBuilder($this->getName())
                ->where($this->getName().'.state = :state')
                ->setParameter('state', Constants::ENTITY_STATE_VALID);

            $entities = $query->getQuery()->getResult();
        } else {
            $query = $repository->createQueryBuilder($this->getName())
                ->where($this->getName().'.state = :state')
                ->setParameter('state', Constants::ENTITY_STATE_VALID);

            $form = $this->createForm($this->getClassFilterForm(), null, array('em' => $this->getDoctrine()->getManager()));
            $form->handleRequest($request);
            if ($form->isSubmitted()&&!$form->isValid()) {
                $entities = NULL;
            } else {
                if ($form->isSubmitted()) {
                    $session = $this->get('session');
                    $session->set($form->getName(), $request->request->get($form->getName()));
                    $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $query);
                    $page = 1;
                } else {
                    $form = $this->createForm($this->getClassFilterForm(), null, array('em' => $this->getDoctrine()->getManager()));
                    $session = $request->getSession();
                    if ($this->keepFilterForm($request,$form)){
                        $form->submit($session->get($form->getName()));
                        $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $query);
                    }
                }
                $paginator = $this->get('knp_paginator');
                $entities = $paginator->paginate($query, $page, $itemsPerPage, array('defaultSortFieldName' => $sort,
                        'defaultSortDirection' => $direction, 'wrap-queries' => true)
                );
            }
        }

        $paramsReturn = array(
            'entities' => $entities,
            'multipleForm' => $multipleForm->createView(),
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'classShortName' => $this->getClassShortName(),
            'displayMode' => $displayMode
        );

        if($displayMode != Preferences::DISPLAY_MODE_THUMBNAIL && $displayMode != Preferences::DISPLAY_MODE_THUMBNAIL_FULL_SCREEN){
            $paramsReturn['form'] = $form->createView();
            $paramsReturn['itemsPerPage'] = $itemsPerPage;
        }

        return $this->render('@GkratzAdmin/admin/'.$this->getName().'/index.html.twig', $paramsReturn);
    }

    /**
     * @Route("/drop")
     * @Method({"GET"})
     */
    public function dropAction()
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository($this->getClassName())->findAll();
        foreach($entities as $entity){
            $em->remove($entity);
        }
        $em->flush();
        $this->get('session')->getFlashBag()->add(
            'success', $this->get('translator')->trans('Entities successfully permanent deleted')
        );
        return $this->redirectToRoute('gkratz_admin_admin_route_analytic');
    }

    /**
     * @return bool|int
     */
    protected function getDisplayMode()
    {
        /** @var \AppBundle\Entity\Preferences $displayModeObject */
        $displayModeObject = $this->getDoctrine()->getRepository(Preferences::class)->findOneBy(array(
            'user' => $this->getUser(),
            'clef' => 'displayMode',
            'option' => 'class',
            'valeurOption' => $this->getClassShortName()
        ));
        $displayMode = ($displayModeObject != null) ? $displayModeObject->getValeur() : Preferences::DISPLAY_MODE_TABLE;
        return $displayMode;
    }
}