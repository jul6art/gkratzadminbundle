<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 14/03/2017
 * Time: 23:44
 */

namespace Gkratz\AdminBundle\Controller\Admin;


use AppBundle\Entity\Preferences;
use AppBundle\Entity\Search;
use Gkratz\AdminBundle\Constants\Constants;
use Gkratz\AdminBundle\Form\SearchType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SearchController
 * @package Gkratz\AdminBundle\Controller\Admin
 */
class SearchController extends AdminController
{
    protected function getClassFilterForm()
    {
        return null;
    }

    protected function getClassForm()
    {
        return null;
    }

    protected function getClass()
    {
        return 'AppBundle\Entity\Search';
    }

    protected function getClassShortName()
    {
        return 'Search';
    }

    protected function getClassName()
    {
        return \AppBundle\Entity\Search::class;
    }

    protected function getName()
    {
        return 'search';
    }

    protected function getLabel()
    {
        return 'Search';
    }


    /**
     * @Method({"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $searchForm = $this->createForm(SearchType::class);

        return $this->render('@GkratzAdmin/admin/search/form.html.twig', array(
            'searchForm' => $searchForm->createView()
        ));
    }


    /**
     * @Route("/search")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request)
    {
        $searchForm = $this->createForm(SearchType::class);
        $searchForm->handleRequest($request);

        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $search = $searchForm['search']->getData();

        $searchParameters = $this->get('gkratz_configurator')->convertSearchArray($this->get('gkratz_configurator')->getSearch());

        if($searchParameters['allow_approaching'] && $request->query->has('approaching')){
            $result = $this->get('gkratz_string_searcher')->getResultId(
                $searchParameters,
                $this->getDoctrine()->getRepository(Search::class)->findOneBySearchId($request->query->get('search'))->getSearchText(),
                $em
            );
            return new JsonResponse(array(
                'error' => 0,
                'type' => 'large_search',
                'url' => $this->generateUrl('gkratz_admin_admin_search_display', array(
                    'id' => $result['id'],
                    'allowApproaching' => $result['allowApproaching']))
            ));
        } elseif($searchForm->isSubmitted() && $searchForm->isValid()) {
            $searchParameters['allow_approaching'] = false;
            $result = $this->get('gkratz_string_searcher')->getResultId($searchParameters, $search, $em);
        }

        return $this->redirectToRoute('gkratz_admin_admin_search_display', array(
            'id' => $result['id'],
            'allowApproaching' => $result['allowApproaching']
        ));
    }


    /**
     * @Route("/results/{id}/{allowApproaching}")
     * @Method({"GET"})
     * @param Request $request
     * @param $id
     * @param $allowApproaching
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function displayAction(Request $request, $id, $allowApproaching)
    {
        $displayMode = $this->getDisplayMode();

        $query = $this->getDoctrine()->getRepository(Search::class)->createQueryBuilder('search')
            ->where('search.searchId = :id')
            ->andWhere('search.points > 0')
            ->setParameter('id', $id);

        $page = $request->query->get('page', 1);
        $sort = 'search.points';

        $itemsPerPage = Constants::ITEM_PER_PAGE;
        if ($request->query->has('items')){
            if ($request->query->get('items') > 0){
                $itemsPerPage = $request->query->get('items');
                $session = $this->get('session');
                $session->set('items', $itemsPerPage);
            }
        } else {
            if ($request->getSession()->get('items') != NULL){
                $itemsPerPage = $request->getSession()->get('items');
            } else {
                $request->getSession()->remove('items');
            }
        }

        if ($request->query->has('sort')){
            $sort = $request->query->get('sort');
        }

        if(count($query->getQuery()->getResult()) > 0){
            $request->getSession()
                ->getFlashBag()
                ->add('success', $this->get('translator')->transChoice(
                    'There is one result found|There are %count% results found',
                    count($query->getQuery()->getResult()),
                    array('%count%' => count($query->getQuery()->getResult()))
                ))
            ;
        }

        $direction = ($request->query->has('direction')) ? $request->query->get('direction') : 'desc';

        $entities = $this->get('knp_paginator')->paginate($query, $page, $itemsPerPage, array('defaultSortFieldName' => $sort,
                'defaultSortDirection' => $direction, 'wrap-queries' => true)
        );

        $searchParameters = $this->get('gkratz_configurator')->convertSearchArray($this->get('gkratz_configurator')->getSearch());

        $responseParams = array(
            'entities' => $entities,
            'displayMode' => $displayMode,
            'itemsPerPage' => $itemsPerPage,
            'allowApproaching' => $allowApproaching
        );

        if($searchParameters['allow_approaching'] && !$allowApproaching){
            $responseParams['approach'] = $searchParameters['allow_approaching'];
        }

        return $this->render('@GkratzAdmin/admin/search/index.html.twig', $responseParams);
    }


    /**
     * @Route("/mode/{classname}/{mode}")
     * @Method({"GET"})
     * @param Request $request
     * @param $classname
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function modeAction(Request $request, $classname, $mode)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var \AppBundle\Entity\Preferences $displayModeForYou */
        $displayModeForYou = $this->getDoctrine()->getRepository(Preferences::class)->findOneBy(array('user' => $this->getUser(),'clef' => 'displayMode', 'option' => 'class', 'valeurOption' => $classname));
        if($displayModeForYou == null){
            $displayModeForYou = new Preferences();
            $displayModeForYou->setUser($this->getUser());
            $displayModeForYou->setOption('class');
            $displayModeForYou->setClef('displayMode');
            $displayModeForYou->setValeurOption($classname);
        }
        $displayModeForYou->setValeur($mode);
        $em->persist($displayModeForYou);
        $em->flush();
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }
}