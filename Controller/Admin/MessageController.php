<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 21/03/2017
 * Time: 23:01
 */

namespace Gkratz\AdminBundle\Controller\Admin;



use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Gkratz\AdminBundle\Constants\Constants;
use Gkratz\AdminBundle\Form\MessageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/message")
 * Class MessageController
 * @package Gkratz\AdminBundle\Controller\Admin
 */
class MessageController extends AdminController
{
    protected function getClassFilterForm()
    {
        return \Gkratz\AdminBundle\FormFilter\PostFilterType::class;
    }

    protected function getClassForm()
    {
        return \Gkratz\AdminBundle\Form\PostType::class;
    }

    protected function getClass()
    {
        return 'AppBundle\Entity\Message';
    }

    protected function getClassShortName()
    {
        return 'Message';
    }

    protected function getClassName()
    {
        return \AppBundle\Entity\Post::class;
    }

    protected function getName()
    {
        return 'message';
    }

    protected function getLabel()
    {
        return 'Message';
    }

    /**
     * @Route("/")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(MessageType::class);
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        //if isset recipient and sender get messages and count messages and senderId for response
        //if profile message, only get with $this->getUser()
        $user = $em->getRepository(User::class)->find(1);
//        $user = l autre user passé en parametre;
        $messages = $em->getRepository(Message::class)->createQueryBuilder('m')
            ->where("m.state = :state")
            ->andWhere("m.sender = :user OR m.recipient = :user")
            ->addOrderBy("m.date", "asc")
            ->setParameters(array(
                "state" => Constants::ENTITY_STATE_VALID,
                "user" => $user,
            ))->getQuery()->getResult();
        $count = count($messages);

        /** @var \DateTime $date */
        $date = new \DateTime();

        /** @var \AppBundle\Entity\Message $message */
        foreach ($messages as $message){
            if($message->getAdminSeenAt() == null){
                $message->setAdminSeenAt($date);
            }
            $message->setAdminSeenTimes($message->getAdminSeenTimes() + 1);
        }

        $em->flush();

        //else
        //$count = 0;

        return $this->render("@GkratzAdmin/admin/message/index.html.twig", array(
            "messages" => $messages,
            "count" => $count,
            "user" => $user,
            "form" => $form->createView()
        ));
    }


    /**
     * @Route("/send/{user}")
     * @Method({"POST"})
     * @param Request $request
     * @param $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function sendAction(Request $request, $user)
    {
        $form = $this->createForm(MessageType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            /** @var \Doctrine\ORM\EntityManager $em */
            $em = $this->getDoctrine()->getManager();

            $user = $em->getRepository(User::class)->findOneBy(array(
                "id" => $user,
                "state" => Constants::ENTITY_STATE_VALID
            ));
            if(null != $user){
                /** @var \AppBundle\Entity\Message $entity */
                $entity = $form->getData();
                $entity->setMessage(nl2br($form['message']->getData()));
                $entity->setSender($this->getUser());
                $entity->setRecipient($user);
                $em->persist($entity);
                $em->flush();

                //mail auto
            }
        }
        return $this->redirectToRoute("gkratz_admin_admin_message_index");
    }
}