<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 09-02-17
 * Time: 13:48
 */

namespace Gkratz\AdminBundle\Controller\Admin;

use AppBundle\Entity\Parameters;
use AppBundle\Entity\Preferences;
use AppBundle\Entity\Trash;
use Doctrine\ORM\QueryBuilder;
use Gkratz\AdminBundle\Constants\Constants;
use Gkratz\AdminBundle\Controller\StandardController;
use Gkratz\AdminBundle\Form\MultipleActionsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class AdminController extends StandardController
{
    abstract protected function getClassFilterForm();           //  \AppBundle\FormFilter\UserFilterType::class
    abstract protected function getClassForm();                 //  \AppBundle\Form\UserType::class
    abstract protected function getClass();                     //  AppBundle\Entity\User
    abstract protected function getClassShortName();            //  User
    abstract protected function getClassName();                 //  \AppBundle\Entity\User::class
    abstract protected function getName();                      //  user
    abstract protected function getLabel();                     //  User


    protected function getRoutePrefix()
    {
        return "gkratz_admin_admin_";
    }

    protected function getFieldSort()
    {
        return $this->getName().'.id';
    }

    protected function getSort()
    {
        return 'desc';
    }


    /**
     * @Route("/")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $displayMode = $this->getDisplayMode();

        $multipleForm = $this->createForm(MultipleActionsType::class);

        $repository = $this->getDoctrine()->getRepository($this->getClassName());

        $itemsPerPage = Constants::ITEM_PER_PAGE;
        if ($request->query->has('items')){
            if ($request->query->get('items') > 0){
                $itemsPerPage = $request->query->get('items');
                $session = $this->get('session');
                $session->set('items', $itemsPerPage);
            }
        } else {
            if ($request->getSession()->get('items') != NULL){
                $itemsPerPage = $request->getSession()->get('items');
            } else {
                $request->getSession()->remove('items');
            }
        }
        $page = $request->query->get('page', 1);
        $sort = $this->getFieldSort();
        if ($request->query->has('sort')){
            $sort = $request->query->get('sort');
        }
        $direction = ($request->query->has('direction'))?$request->query->get('direction'):$this->getSort();

        /* @var $query \Doctrine\ORM\QueryBuilder */
        $query = $repository->createQueryBuilder($this->getName())
            ->where($this->getName().'.state = :state')
            ->setParameter('state', Constants::ENTITY_STATE_VALID);

        $entities = $query->getQuery()->getResult();

        if(($displayMode != Preferences::DISPLAY_MODE_THUMBNAIL && $displayMode != Preferences::DISPLAY_MODE_THUMBNAIL_FULL_SCREEN) || count($entities) > Constants::LIMIT_ENTITIES_DISPLAYED_MODE_THUMBNAIL){
            $query = $repository->createQueryBuilder($this->getName())
                ->where($this->getName().'.state = :state')
                ->setParameter('state', Constants::ENTITY_STATE_VALID);

            $this->joinSort($query, $sort, $request->getLocale());

            $form = $this->createForm($this->getClassFilterForm(), null, array('em' => $this->getDoctrine()->getManager()));
            $form->handleRequest($request);
            if ($form->isSubmitted()&&!$form->isValid()) {
                $entities = NULL;
            } else {
                if ($form->isSubmitted()) {
                    $session = $this->get('session');
                    $session->set($form->getName(), $request->request->get($form->getName()));
                    $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $query);
                    $page = 1;
                } else {
                    $form = $this->createForm($this->getClassFilterForm(), null, array('em' => $this->getDoctrine()->getManager()));
                    $session = $request->getSession();
                    if ($this->keepFilterForm($request,$form)){
                        $form->submit($session->get($form->getName()));
                        $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $query);
                    }
                }
                $entities = $this->get('knp_paginator')->paginate($query, $page, $itemsPerPage, array('defaultSortFieldName' => $sort,
                        'defaultSortDirection' => $direction, 'wrap-queries' => true)
                );
            }
            if ($displayMode == Preferences::DISPLAY_MODE_THUMBNAIL) {
                $displayMode = Preferences::DISPLAY_MODE_TABLE;
            } elseif ($displayMode == Preferences::DISPLAY_MODE_THUMBNAIL_FULL_SCREEN){
                $displayMode = Preferences::DISPLAY_MODE_TABLE_FULL_SCREEN;
            }
        }

        $paramsReturn = array(
            'entities' => $entities,
            'multipleForm' => $multipleForm->createView(),
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'classShortName' => $this->getClassShortName(),
            'displayMode' => $displayMode
        );

        if($displayMode != Preferences::DISPLAY_MODE_THUMBNAIL && $displayMode != Preferences::DISPLAY_MODE_THUMBNAIL_FULL_SCREEN){
            $paramsReturn['form'] = $form->createView();
            $paramsReturn['itemsPerPage'] = $itemsPerPage;
        }

        return $this->render('@GkratzAdmin/admin/'.$this->getName().'/index.html.twig', $paramsReturn);
    }


    /**
     * @Route("/add")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        $class = $this->getClassName();
        $entity = new $class();

        $form = $this->createForm($this->getClassForm(), $entity, array('readonly' => false));
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'success', $this->get('translator')->trans('Entity successfully added')
                );
                return $this->redirectToRoute($this->getRoutePrefix().$this->getName().'_index');
            } else {
                $this->get('session')->getFlashBag()->add(
                    'error', $this->get('translator')->trans('The request parameters are not good')
                );
            }
        }

        return $this->render('@GkratzAdmin/admin/add.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'addRoute' => $this->getRoutePrefix().$this->getName().'_add',
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'indexRoute' => $this->getRoutePrefix().$this->getName().'_index',
        ]);
    }

    /**
     * @Route("/edit/{id}")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {
        $entity = $this->getDoctrine()->getRepository($this->getClassName())->find($id);

        if (!$entity || $entity->getState() != Constants::ENTITY_STATE_VALID) {
            return $this->render('@GkratzAdmin/admin/404.html.twig');
        }

        $form = $this->createForm($this->getClassForm(),$entity, array('readonly' => false));
        $form->handleRequest($request);

        if($form->isSubmitted()){
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                $this->get('session')->getFlashBag()->add(
                    'success', $this->get('translator')->trans('Entity successfully edited')
                );
                return $this->redirectToRoute($this->getRoutePrefix() . $this->getName() . '_index');
            } else {
                $this->get('session')->getFlashBag()->add(
                    'error', $this->get('translator')->trans('The request parameters are not good')
                );
            }
        }

        return $this->render('@GkratzAdmin/admin/edit.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'editRoute' => $this->getRoutePrefix().$this->getName().'_edit',
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'indexRoute' => $this->getRoutePrefix().$this->getName().'_index',
            'nbRevisions' => count($this->get('simplethings_entityaudit.reader')->findRevisions('AppBundle\Entity\Post', $entity->getId()))
        ]);
    }


    /**
     * @Route("/view/{id}")
     * @Method({"GET"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        $entity = $this->getDoctrine()->getRepository($this->getClassName())->find($id);

        if (!$entity || $entity->getState() != Constants::ENTITY_STATE_VALID){
            return $this->render('@GkratzAdmin/admin/404.html.twig');
        }

        $form = $this->createForm($this->getClassForm(),$entity, ['readonly' => true]);

        return $this->render('@GkratzAdmin/admin/view.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'editRoute' => $this->getRoutePrefix().$this->getName().'_edit',
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'indexRoute' => $this->getRoutePrefix().$this->getName().'_index'
        ]);
    }


    /**
     * @Route("/delete/{classname}")
     * @Method({"POST"})
     * @param Request $request
     * @param $classname
     * @return JsonResponse
     */
    public function deleteAction(Request $request, $classname)
    {
        $jsonResponse = [
            'error' => 1,
            'type' => 'multiple_delete'
        ];

        if(!$request->isXmlHttpRequest()){
            $jsonResponse['message'] = $this->get('translator')->trans('The request must be asyncronous');
        } else {
            $form = $this->createForm(MultipleActionsType::class);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $jsonResponse['ids'] = $form->getData()['ids'];
                $ids = explode(",", $jsonResponse['ids']);
                /** @var \Doctrine\ORM\EntityManager $em */
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:'.$classname);
                $entities = $repository->createQueryBuilder($classname)
                    ->where($classname.'.state = :state')
                    ->andWhere($classname.'.id in (:ids)')
                    ->setParameters(array(
                        'state' => Constants::ENTITY_STATE_VALID,
                        'ids' => $ids
                    ))->getQuery()->getResult();

                if (count($entities) > 0){
                    $countTrash = 0;
                    foreach ($entities as $entity){
                        $class = get_class($entity);
                        $entity->setState(Constants::ENTITY_STATE_TRASH);

                        /** @var \AppBundle\Entity\Trash $trash */
                        $trash = new Trash();
                        $trash->setElementId($entity->getId());
                        $trash->setClass($class);
                        switch($class){
                            case 'AppBundle\Entity\User':
                                $trash->setLegende($entity->getUsername());
                                break;
                            case 'AppBundle\Entity\Post':
                                $trash->setLegende($entity->getTitle());
                                break;
                            case 'AppBundle\Entity\Backup':
                                $date = $entity->getDate();
                                $trash->setLegende($date->format('d M Y H:i:s'));
                                break;
                            default:
                                $trash->setLegende($trash->getElementId());
                        }
                        $trash->setUser($this->getUser());
                        $em->persist($trash);
                        $countTrash ++;
                    }
                    /** @var \AppBundle\Entity\Parameters $site */
                    $site = $em->getRepository(Parameters::class)->find(1);
                    $site->setCountTrash($site->getCountTrash() + $countTrash);
                    $em->flush();
                    $jsonResponse['message'] = $this->get('translator')->trans('Entities successfully deleted');
                    $jsonResponse['error'] = 0;
                    $jsonResponse['countTrash'] = $site->getCountTrash();
                } else {
                    $jsonResponse['message'] = $this->get('translator')->trans('No entities selectionned');
                }
            } else {
                $jsonResponse['message'] = $this->get('translator')->trans('No entities selectionned');
            }
        }
        return new JsonResponse($jsonResponse);
    }


    /**
     * @Route("/compare/{id}")
     * @Method({"GET"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \SimpleThings\EntityAudit\Exception\DeletedException
     * @throws \SimpleThings\EntityAudit\Exception\NoRevisionFoundException
     * @throws \SimpleThings\EntityAudit\Exception\NotAuditedException
     */
    public function compareAction(Request $request, $id)
    {
        $revisions = $this->get('simplethings_entityaudit.reader')->findRevisions($this->getClass(), $id);

        if($request->query->has('rev')){
            foreach($revisions as $revision){
                if ($revision->getRev() < $request->query->get('rev')){
                    unset($revisions[array_search($revision, $revisions)]);
                }
                $lastItems = array_merge(array(), array_slice($revisions, -2, 2, true));
            }
        } else {
            $lastItems = array_merge(array(), array_slice($revisions, 0, 2, true));
        }

        $comparaisonObjects = [
            [
                'entity' => $this->get('simplethings_entityaudit.reader')->find(
                    $this->getClass(),
                    $id,
                    $rev = $lastItems[1]->getRev()
                ),
                'revision' => $lastItems[1]
            ],
            [
                'entity' => $this->get('simplethings_entityaudit.reader')->find(
                    $this->getClass(),
                    $id,
                    $rev = $lastItems[0]->getRev()
                ),
                'revision' => $lastItems[0]
            ]
        ];
        return $this->render('@GkratzAdmin/admin/compare.html.twig', [
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'entities'=> $comparaisonObjects,
            'revisions' => $this->get('simplethings_entityaudit.reader')->findRevisions('AppBundle\Entity\Post', $id)
        ]);
    }

    /**
     * @Route("/reload/{id}/{rev}")
     * @Method({"GET"})
     * @param $id
     * @param $rev
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \SimpleThings\EntityAudit\Exception\DeletedException
     * @throws \SimpleThings\EntityAudit\Exception\NoRevisionFoundException
     * @throws \SimpleThings\EntityAudit\Exception\NotAuditedException
     */
    public function reloadAction($id, $rev)
    {
        $em = $this->getDoctrine()->getManager();

        $targetRevision = $this->get('simplethings_entityaudit.reader')->find(
            $this->getClass(),
            $id,
            $rev
        );

        $entity = $em->getRepository($this->getClassName())->find($id);

        $reflexion = new \ReflectionClass($this->getClass());

        foreach($reflexion->getProperties() as $property){
            if($property->getName() != 'id'){
                $getFunction = 'get' . ucfirst($property->getName());
                $setFunction = 'set' . ucfirst($property->getName());
                $entity->$setFunction($targetRevision->$getFunction());
            }
        }
        $em->flush();
        return $this->redirectToRoute('gkratz_admin_admin_'.$this->getName().'_index');
    }

    /**
     * @Route("/mode/{classname}/{mode}")
     * @Method({"GET"})
     * @param Request $request
     * @param $classname
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function modeAction(Request $request, $classname, $mode)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var \AppBundle\Entity\Preferences $displayModeForYou */
        $displayModeForYou = $this->getDoctrine()->getRepository(Preferences::class)->findOneBy(array('user' => $this->getUser(),'clef' => 'displayMode', 'option' => 'class', 'valeurOption' => $classname));
        if($displayModeForYou == null){
            $displayModeForYou = (new Preferences())->setUser($this->getUser());
            $displayModeForYou->setOption('class');
            $displayModeForYou->setClef('displayMode');
            $displayModeForYou->setValeurOption($classname);
        }
        $displayModeForYou->setValeur($mode);
        $em->persist($displayModeForYou);
        $em->flush();
        if($mode == Preferences::DISPLAY_MODE_THUMBNAIL || $mode == Preferences::DISPLAY_MODE_THUMBNAIL_FULL_SCREEN){
            return $this->redirectToRoute($this->getRoutePrefix().$this->getName().'_index');
        } else {
            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param $column
     * @param $locale
     */
    protected function joinSort(QueryBuilder $queryBuilder, $column, $locale)
    {
        switch ($column){
            case "t.name":
//                $queryBuilder->join($this->getName().'.translations', 't', 'WITH', 't.locale = :locale')->setParameter('locale', $locale);
//                $queryBuilder->addSelect('t');
                break;
            default:
                break;
        }
    }


    /**
     * @param Request $request
     * @param Form $form
     * @return bool
     */
    protected function keepFilterForm(Request $request, Form $form)
    {
        $keepForm = ($request->getSession()->get($form->getName()) != NULL);
        if (!$keepForm){
            $request->getSession()->remove($form->getName());
        }
        return $keepForm;
    }

    /**
     * @return bool|int
     */
    protected function getDisplayMode()
    {
        /** @var \AppBundle\Entity\Preferences $displayModeObject */
        $displayModeObject = $this->getDoctrine()->getRepository(Preferences::class)->findOneBy([
            'user' => $this->getUser(),
            'clef' => 'displayMode',
            'option' => 'class',
            'valeurOption' => $this->getClassShortName()
        ]);
        $displayMode = ($displayModeObject != null) ? $displayModeObject->getValeur() : Preferences::DISPLAY_MODE_THUMBNAIL;
        return $displayMode;
    }
}