<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 10-02-17
 * Time: 15:27
 */

namespace Gkratz\AdminBundle\Controller\Admin;

use AppBundle\Entity\Parameters;
use AppBundle\Entity\Trash;
use Doctrine\ORM\EntityManager;
use Gkratz\AdminBundle\Constants\Constants;
use Gkratz\AdminBundle\Form\MultipleActionsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/trash")
 * Class TrashController
 * @package Gkratz\AdminBundle\Controller\Admin
 */
class TrashController extends AdminController
{
    protected function getClassFilterForm()
    {
        return false;
    }

    protected function getClassForm()
    {
        return false;
    }

    protected function getClass()
    {
        return 'AppBundle\Entity\Trash';
    }

    protected function getClassShortName()
    {
        return 'Trash';
    }

    protected function getClassName()
    {
        return \AppBundle\Entity\Trash::class;
    }

    protected function getName()
    {
        return 'trash';
    }

    protected function getLabel()
    {
        return 'Trash';
    }


    /**
     * @Route("/")
     * @Method({"GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $displayMode = $this->getDisplayMode();

        $multipleForm = $this->createForm(MultipleActionsType::class);

        $repository = $this->getDoctrine()->getRepository($this->getClassName());
        /* @var $query \Doctrine\ORM\QueryBuilder */
        $query = $repository->createQueryBuilder($this->getName());

        if(!$this->getUser()->hasRole('ROLE_SUPER_ADMIN')){
            $query->where($this->getName().'.softDeleted = :softDelete')->setParameter('softDelete', 0);
        }

        $entities = $query->getQuery()->getResult();

        return $this->render('@GkratzAdmin/admin/'.$this->getName().'/index.html.twig', array(
            'entities' => $entities,
            'multipleForm' => $multipleForm->createView(),
            'displayMode' => $displayMode
        ));
    }


    /**
     * @Route("/manage/{state}")
     * @Method({"POST"})
     * @param Request $request
     * @param $state
     * @return JsonResponse
     */
    public function manageAction(Request $request, $state)
    {
        $jsonResponse = array(
            'error' => 1,
            'state' => $state,
            'type' => 'multiple_manage'
        );
        if(!$request->isXmlHttpRequest()){
            $jsonResponse['message'] = $this->get('translator')->trans('The request must be asyncronous');
        }else{
            $form = $this->createForm(MultipleActionsType::class);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {
                $jsonResponse['ids'] = $form->getData()['ids'];
                $ids = explode(",", $jsonResponse['ids']);
                /** @var \Doctrine\ORM\EntityManager $em */
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository(Trash::class);
                $entities = $repository->createQueryBuilder('trash')
                    ->andWhere('trash.id in (:ids)')
                    ->setParameter('ids', $ids)
                    ->orderBy('trash.class', 'asc')
                    ->getQuery()->getResult();

                if (count($entities) > 0) {
                    $countTrash = 0;
                    $className = $entities[0]->getClass();
                    $entitiesIds = array();
                    foreach ($entities as $entity) {
                        if ($entity->getClass() == $className) {
                            $entitiesIds[] = $entity->getElementId();
                        } else {
                            $this->manageTrashes($className, $entitiesIds, $em, $state);
                            $className = $entity->getClass();
                            $entitiesIds = array();
                            $entitiesIds[] = $entity->getElementId();
                        }
                        if ($state == Constants::ENTITY_STATE_DELETED) {
                            $entity->setSoftDeleted(1);
                        } else {
                            $em->remove($entity);
                        }
                        $countTrash++;
                    }
                    $this->manageTrashes($className, $entitiesIds, $em, $state);
                    /** @var \AppBundle\Entity\Parameters $site */
                    $site = $em->getRepository(Parameters::class)->find(1);
                    $finalCountTrash = (($site->getCountTrash() - $countTrash) > 0) ? $site->getCountTrash(
                        ) - $countTrash : 0;
                    $site->setCountTrash($finalCountTrash);
                    $em->flush();
                    $jsonResponse['message'] = $this->get('translator')->trans(
                        ($state == Constants::ENTITY_STATE_VALID) ? 'Entities successfully restored' : 'Entities successfully permanent deleted'
                    );
                    $jsonResponse['error'] = 0;
                    $jsonResponse['countTrash'] = $site->getCountTrash();
                } else {
                    $jsonResponse['message'] = $this->get('translator')->trans('No entities selectionned');
                }
            } else {
                $jsonResponse['message'] = $this->get('translator')->trans('No entities selectionned');
            }
        }
        return new JsonResponse($jsonResponse);
    }


    /**
     * @param $className
     * @param $entitiesIds
     * @param EntityManager $em
     * @param $state
     */
    private function manageTrashes($className, $entitiesIds, EntityManager $em, $state)
    {
        $entities = $em->getRepository($className)->createQueryBuilder('x')
            ->where('x.id in (:ids)')
            ->setParameter('ids', $entitiesIds)
            ->getQuery()->getResult();
        foreach ($entities as $entity){
            $entity->setState($state);
        }
    }
}