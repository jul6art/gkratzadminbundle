<?php

namespace Gkratz\AdminBundle\Controller\Admin;

use AppBundle\Entity\Analytic;
use Gkratz\AdminBundle\Controller\StandardController;
use Gkratz\AdminBundle\Form\SearchType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 28/01/2017
 * Time: 02:50
 */
/**
 * @Route("/")
 */
class DashboardController extends StandardController
{
    /**
     * @Route("/")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $datas = $this->get('gkratz_analytic_chart_homepage')->getHomePageUniqueGraph();

        return $this->render('@GkratzAdmin/admin/dashboard/index.html.twig', [
            'counts' => $datas['counts'],
            'days' => $datas['days']
        ]);
    }


    /**
     * @Route("/analytic")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function analyticAction()
    {
        //ALL THE GRAPHS
        $oneYearPerUser = $this->get('gkratz_analytic_charts')->oneYearPerUser();
        $oneYearPerCountry = $this->get('gkratz_analytic_charts')->oneYearPerCountry();
        $pieOneYearPerCountry = $this->get('gkratz_analytic_charts')->pieOneYearPerCountry();
        $oneYearPerCity = $this->get('gkratz_analytic_charts')->oneYearPerCity();
        $pieOneYearPerCity = $this->get('gkratz_analytic_charts')->pieOneYearPerCity();
        $oneYearPerLanguage = $this->get('gkratz_analytic_charts')->oneYearPerLanguage();
        $pieOneYearPerLanguage = $this->get('gkratz_analytic_charts')->pieOneYearPerLanguage();
        $oneYearPerBrowser = $this->get('gkratz_analytic_charts')->oneYearPerBrowser();
        $pieOneYearPerBrowser = $this->get('gkratz_analytic_charts')->pieOneYearPerBrowser();
        $oneYearPerPage = $this->get('gkratz_analytic_charts')->oneYearPerPage();
        $pieOneYearPerPage = $this->get('gkratz_analytic_charts')->pieOneYearPerPage();
        $oneYearPages = $this->get('gkratz_analytic_charts')->oneYearPages();
        $sevenDaysPerUser = $this->get('gkratz_analytic_charts')->sevenDaysPerUser();
        $sevenDaysPerCountry = $this->get('gkratz_analytic_charts')->sevenDaysPerCountry();
        $pieOneWeekPerCountry = $this->get('gkratz_analytic_charts')->pieOneWeekPerCountry();
        $pieOneWeekPerCity = $this->get('gkratz_analytic_charts')->pieOneWeekPerCity();
        $sevenDaysPerCity = $this->get('gkratz_analytic_charts')->sevenDaysPerCity();
        $sevenDaysPerLanguage = $this->get('gkratz_analytic_charts')->sevenDaysPerLanguage();
        $pieOneWeekPerLanguage = $this->get('gkratz_analytic_charts')->pieOneWeekPerLanguage();
        $pieOneWeekPerBrowser = $this->get('gkratz_analytic_charts')->pieOneWeekPerBrowser();
        $sevenDaysPerBrowser = $this->get('gkratz_analytic_charts')->sevenDaysPerBrowser();
        $sevenDaysPerPage = $this->get('gkratz_analytic_charts')->sevenDaysPerPage();
        $pieOneWeekPerPage = $this->get('gkratz_analytic_charts')->pieOneWeekPerPage();
        $sevenDaysPages = $this->get('gkratz_analytic_charts')->sevenDaysPages();

        return $this->render('@GkratzAdmin/admin/dashboard/analytic.html.twig', [
                'sevenDaysPages' => $sevenDaysPages, 'oneYearPerUser' => $oneYearPerUser, 'oneYearPerCountry' => $oneYearPerCountry,
                'pieOneYearPerCountry' => $pieOneYearPerCountry, 'oneYearPerCity' => $oneYearPerCity, 'pieOneYearPerCity' => $pieOneYearPerCity,
                'pieOneYearPerLanguage' => $pieOneYearPerLanguage, 'oneYearPerLanguage' => $oneYearPerLanguage, 'oneYearPerBrowser' => $oneYearPerBrowser,
                'pieOneYearPerBrowser' => $pieOneYearPerBrowser, 'oneYearPerPage' => $oneYearPerPage, 'pieOneYearPerPage' => $pieOneYearPerPage,
                'oneYearPages' => $oneYearPages, 'sevenDaysPerUser' => $sevenDaysPerUser, 'sevenDaysPerCountry' => $sevenDaysPerCountry,
                'pieOneWeekPerCountry' => $pieOneWeekPerCountry, 'pieOneWeekPerCity' => $pieOneWeekPerCity, 'sevenDaysPerCity' => $sevenDaysPerCity,
                'sevenDaysPerLanguage' => $sevenDaysPerLanguage, 'pieOneWeekPerLanguage' => $pieOneWeekPerLanguage, 'pieOneWeekPerBrowser' => $pieOneWeekPerBrowser,
                'sevenDaysPerBrowser' => $sevenDaysPerBrowser, 'sevenDaysPerPage' => $sevenDaysPerPage, 'pieOneWeekPerPage' => $pieOneWeekPerPage
            ]
        );
    }
}