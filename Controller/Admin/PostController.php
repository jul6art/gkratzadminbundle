<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 06-03-17
 * Time: 18:25
 */

namespace Gkratz\AdminBundle\Controller\Admin;


use AppBundle\Entity\MenuItem;
use AppBundle\Entity\Post;
use AppBundle\Entity\Tag;
use Doctrine\ORM\QueryBuilder;
use Gkratz\AdminBundle\Constants\Constants;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/post")
 * Class PostController
 * @package Gkratz\AdminBundle\Controller\Admin
 */
class PostController extends AdminController
{
    protected function getClassFilterForm()
    {
        return \Gkratz\AdminBundle\FormFilter\PostFilterType::class;
    }

    protected function getClassForm()
    {
        return \Gkratz\AdminBundle\Form\PostType::class;
    }

    protected function getClass()
    {
        return 'AppBundle\Entity\Post';
    }

    protected function getClassShortName()
    {
        return 'Post';
    }

    protected function getClassName()
    {
        return \AppBundle\Entity\Post::class;
    }

    protected function getName()
    {
        return 'post';
    }

    protected function getLabel()
    {
        return 'Post';
    }


    /**
     * @Route("/add")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        $class = $this->getClassName();
        $entity = new $class();

        $form = $this->createForm($this->getClassForm(), $entity, array('readonly' => false))->add('newTags', TextType::class, array(
                'mapped' => false,
                'required' => false,
                'label' => 'Add new tags (space delimiter)',
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-12',
                    'placeholder' => 'Add new tags (space delimiter)'
                )
            ))
            ->add('publicationDate','Gkratz\AdminBundle\Form\Standard\DateTimePickerType', array(
                'required' => false,
                'mapped' => false,
                'translation_domain' => 'messages',
                'label' => 'Publication date (only to postdate)',
                'attr' => array(
                    'placeholder' => 'Publication date',
                    'class' => 'sm-8 datepicker'
                )
            ))
            ->add('publicationTime',TextType::class, array(
                'required' => false,
                'mapped' => false,
                'translation_domain' => 'messages',
                'label' => 'Publication time (only to postdate)',
                'attr' => array(
                    'placeholder' => 'Publication time',
                    'class' => 'sm-8 timepicker'
                )
            ));
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $newTags = $form['newTags']->getData();
                if($newTags != null){
                    /** @var \AppBundle\Repository\TagRepository $tagRepo */
                    $tagRepo = $em->getRepository(Tag::class);

                    foreach (explode(' ', $newTags) as $tag){
                        $testTag = $tagRepo->findOneByName($tag);
                        if(!$testTag){
                            /** @var \AppBundle\Entity\Tag $newTag */
                            $newTag = new Tag();
                            $newTag->setName($tag);
                            $em->persist($newTag);
                            $entity->addTag($newTag);
                        }
                    }
                }

                if (null === $form['publicationDate']->getData() && null === $form['publicationTime']->getData()) {} else {
                    $entity = $this->managePublicationDate($entity, $form['publicationDate']->getData(), $form['publicationTime']->getData());
                }

                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'success', $this->get('translator')->trans('Entity successfully added')
                );
                return $this->redirectToRoute($this->getRoutePrefix().$this->getName().'_index');
            } else {
                $this->get('session')->getFlashBag()->add(
                    'error', $this->get('translator')->trans('The request parameters are not good')
                );
            }
        }

        return $this->render('@GkratzAdmin/admin/add.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'addRoute' => $this->getRoutePrefix().$this->getName().'_add',
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'indexRoute' => $this->getRoutePrefix().$this->getName().'_index',
        ));
    }

    /**
     * @Route("/edit/{id}")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {
        $entity = $this->getDoctrine()->getRepository($this->getClassName())->find($id);

        if (!$entity) {
            return $this->render('@GkratzAdmin/admin/404.html.twig');
        }

        if($entity->getState() != Constants::ENTITY_STATE_VALID){
            return $this->render('@GkratzAdmin/admin/404.html.twig');
        }

        $isMenu = $entity->isMenu();

        $form = $this->createForm($this->getClassForm(),$entity, array('readonly' => false))->add('newTags', TextType::class, array(
                'mapped' => false,
                'required' => false,
                'label' => 'Add new tags (space delimiter)',
                'translation_domain' => 'messages',
                'attr' => array(
                    'class' => 'sm-12',
                    'placeholder' => 'Add new tags (space delimiter)'
                )
            ))
            ->add('publicationDate','Gkratz\AdminBundle\Form\Standard\DateTimePickerType', array(
                'required' => false,
                'mapped' => false,
                'translation_domain' => 'messages',
                'label' => 'Publication date',
                'attr' => array(
                    'placeholder' => 'Publication date',
                    'class' => 'sm-8 datepicker'
                )
            ))
            ->add('publicationTime',TextType::class, array(
                'required' => false,
                'mapped' => false,
                'translation_domain' => 'messages',
                'label' => 'Publication time',
                'attr' => array(
                    'placeholder' => 'Publication time',
                    'class' => 'sm-8 timepicker'
                )
            ));
        $form->handleRequest($request);

        if($form->isSubmitted()){
            if ($form->isValid()) {
                /** @var \Doctrine\ORM\EntityManager $em */
                $em = $this->getDoctrine()->getManager();

                $newTags = $form['newTags']->getData();
                if($newTags != null){
                    /** @var \AppBundle\Repository\TagRepository $tagRepo */
                    $tagRepo = $em->getRepository(Tag::class);

                    foreach (explode(' ', $newTags) as $tag){
                        $testTag = $tagRepo->findOneByName($tag);
                        if(!$testTag){
                            /** @var \AppBundle\Entity\Tag $newTag */
                            $newTag = new Tag();
                            $newTag->setName($tag);
                            $em->persist($newTag);
                            $entity->addTag($newTag);
                        }
                    }
                }

                if (null === $form['publicationDate']->getData() && null === $form['publicationTime']->getData()) {} else {
                    $entity = $this->managePublicationDate($entity, $form['publicationDate']->getData(), $form['publicationTime']->getData());
                }

                $this->get('session')->getFlashBag()->add(
                    'success', $this->get('translator')->trans('Entity successfully edited')
                );

                if($isMenu && $form['menu']->getData() == 0){
                    $menuItem = $em->getRepository(MenuItem::class)->findOneByPath($entity->getSlug());
                    if (null != $menuItem){
                        if($menuItem->getMenuOrder() == null){
                            $em->remove($menuItem);
                        } else {
                            $entity->setMenu(1);
                            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Impossible to delete a parent item! Remove children, save the menu and try again!'));
                        }
                    }
                }

                if(!$isMenu && $form['menu']->getData() == 1) {
                    $menuItem = new MenuItem();
                    $menuItem->setName($entity->getTitle());
                    $menuItem->setIsRemovable(1);
                    $menuItem->setIsSection(0);
                    $menuItem->setPath($entity->getSlug());
                    $em->persist($menuItem);
                }

                $em->flush();
                return $this->redirectToRoute($this->getRoutePrefix().$this->getName().'_index');
            } else {
                $this->get('session')->getFlashBag()->add(
                    'error', $this->get('translator')->trans('The request parameters are not good')
                );
            }
        }

        return $this->render('@GkratzAdmin/admin/edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'editRoute' => $this->getRoutePrefix().$this->getName().'_edit',
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'indexRoute' => $this->getRoutePrefix().$this->getName().'_index',
            'nbRevisions' => count($this->get('simplethings_entityaudit.reader')->findRevisions('AppBundle\Entity\Post', $entity->getId()))
        ));
    }

    /**
     * @Route("/reload/{id}/{rev}")
     * @Method({"GET"})
     * @param $id
     * @param $rev
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \SimpleThings\EntityAudit\Exception\DeletedException
     * @throws \SimpleThings\EntityAudit\Exception\NoRevisionFoundException
     * @throws \SimpleThings\EntityAudit\Exception\NotAuditedException
     */
    public function reloadAction($id, $rev)
    {
        $em = $this->getDoctrine()->getManager();

        $targetRevision = $this->get('simplethings_entityaudit.reader')->find(
            $this->getClass(),
            $id,
            $rev
        );

        $entity = $em->getRepository($this->getClassName())->find($id);

        $entity->setTitle($targetRevision->getTitle());
        $entity->setContent($targetRevision->getContent());

        $em->flush();
        return $this->redirectToRoute('gkratz_admin_admin_'.$this->getName().'_index');
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param $column
     * @param $locale
     */
    protected function joinSort(QueryBuilder $queryBuilder, $column, $locale)
    {
        switch ($column){
            case "u.username":
                $queryBuilder->leftJoin('AppBundle:User', 'u', 'WITH', 'post.author = u.id');
                break;
            default:
                break;
        }
    }

    /**
     * @param Post $entity
     * @param $date
     * @param $time
     * @return Post
     */
    private function managePublicationDate(Post $entity, $date, $time)
    {
        if(null !== $date && null !== $time){
            $publicationDate = new \DateTime($date->format('Y-m-d') . ' ' . $time);
        } elseif(null !== $date && null === $time){
            $publicationDate = new \DateTime($date->format('Y-m-d') . ' 00:00:00');
        } elseif(null === $date && null !== $time){
            $date = new \DateTime();
            $publicationDate = new \DateTime($date->format('Y-m-d') . ' ' . $time);
        }
        $entity->setPublicationDate($publicationDate);
        return $entity;
    }
}