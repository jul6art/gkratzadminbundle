<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 06-03-17
 * Time: 18:25
 */

namespace Gkratz\AdminBundle\Controller\Admin;

use AppBundle\Entity\Backup;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/backup")
 * Class BackupController
 * @package Gkratz\AdminBundle\Controller\Admin
 */
class BackupController extends AdminController
{
    protected function getClassFilterForm()
    {
        return \Gkratz\AdminBundle\FormFilter\BackupFilterType::class;
    }

    protected function getClassForm()
    {
        return null;
    }

    protected function getClass()
    {
        return 'AppBundle\Entity\Backup';
    }

    protected function getClassShortName()
    {
        return 'Backup';
    }

    protected function getClassName()
    {
        return \AppBundle\Entity\Backup::class;
    }

    protected function getName()
    {
        return 'backup';
    }

    protected function getLabel()
    {
        return 'Backup';
    }

    /**
     * @Route("/add/files", name="backup_add_files")
     * @Method({"POST", "GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addFilesAction(Request $request){
        $jsonResponse = array(
            'error' => 1,
            'type' => 'add_backup'
        );
        if(!$request->isXmlHttpRequest()){
            $jsonResponse['message'] = $this->get('translator')->trans('The request must be asyncronous');
        } else {
            try{
                /** @var \AppBundle\Entity\Backup $backup */
                $backup = new Backup();
                $backup->setUser($this->getUser());
                $backup->setType(2);

                $path = $this->container->get('kernel')->getRootDir().'/../web/downloads';
                $root = $this->container->get('kernel')->getRootDir().'/../';

                $sources = array(
                    $root.'/app',
                    $root.'/bin',
                    $root.'/src',
                    $root.'/tests',
                    $root.'/web',
                );
                $filename = 'backupfrom'.$backup->getDate()->format('Y-m-d-H-i-s').'.zip';

                ini_set('max_execution_time', 1800);
                ini_set('memory_limit','1024M');



                $patterns = array("/backupfrom20/i");

                $singleFiles = array(
                    '.gitignore',
                    'composer.json',
                    'composer.lock',
                    'phpunit.xml.dist',
                    'README.md'
                );

                $zipper = $this->get('gkratz_files_zipper');

                $zipper->zip($sources, $path.'/'.$filename, $patterns, $singleFiles);

                $size = $this->get('gkratz_files_sizer')->sizeFile($path, $filename);

                $backup->setSize($size);

                $em = $this->getDoctrine()->getManager();
                $em->persist($backup);
                $em->flush();
                $jsonResponse['error'] = 0;
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', $this->get('translator')->trans('Entity successfully added'))
                ;
            } catch (\Exception $e){
                $jsonResponse['message'] = $this->get('translator')->trans('An error occured, sorry! If the problem persist, please contact us and give us the informations to fix the problem!');
            }
        }
        return new JsonResponse($jsonResponse);
    }

    /**
     * @Route("/add/database", name="backup_add_database")
     * @Method({"POST", "GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addDatabaseAction(Request $request){
        $jsonResponse = array(
            'error' => 1,
            'type' => 'add_backup'
        );
        if(!$request->isXmlHttpRequest()){
            $jsonResponse['message'] = $this->get('translator')->trans('The request must be asyncronous');
        } else {
            try{
                /** @var \AppBundle\Entity\Backup $backup */
                $backup = new Backup();
                $backup->setUser($this->getUser());
                $backup->setType(1);

                $dbUser = $this->getParameter('database_user');
                $dbPwd = $this->getParameter('database_password');
                $dbHost = $this->getParameter('database_host');
                $dbName = $this->getParameter('database_name');
                $path = $this->container->get('kernel')->getRootDir().'/../web/downloads';
                $filename = 'backupfrom'.$backup->getDate()->format('Y-m-d-H-i-s').'.sql';

                $dumper = $this->get('gkratz_mysql_dumper');
                $dumper->init('mysql:host='.$dbHost.';dbname='.$dbName, $dbUser, $dbPwd);

                $dumper->start($path . '/' . $filename);

                $size = $this->get('gkratz_files_sizer')->sizeFile($path, $filename);

                $backup->setSize($size);

                $em = $this->getDoctrine()->getManager();
                $em->persist($backup);
                $em->flush();

                $jsonResponse['error'] = 0;
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', $this->get('translator')->trans('Entity successfully added'))
                ;
            } catch (\Exception $e){
                $jsonResponse['message'] = $this->get('translator')->trans('An error occured, sorry! If the problem persist, please contact us and give us the informations to fix the problem!');
            }
        }
        return new JsonResponse($jsonResponse);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param $column
     * @param $locale
     */
    protected function joinSort(QueryBuilder $queryBuilder, $column, $locale){
        switch ($column){
            case "u.username":
                $queryBuilder->leftJoin('AppBundle:User', 'u', 'WITH', 'backup.user = u.id');
                break;
            default:
                break;
        }
    }
}