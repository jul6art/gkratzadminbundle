<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 12/03/2017
 * Time: 22:53
 */

namespace Gkratz\AdminBundle\Model;


/**
 * Interface MenuInterface
 * @package Gkratz\AdminBundle\Model
 */
interface MenuInterface
{
    /**
     * Set name
     *
     * @param string $name
     *
     * @return Menu
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();
}