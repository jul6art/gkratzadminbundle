<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 26/02/2017
 * Time: 02:28
 */

namespace Gkratz\AdminBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Preferences
 * @package Gkratz\AdminBundle\Model
 */
abstract class Preferences implements PreferencesInterface
{
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @var string
     * @ORM\Column(name="clef", type="string")
     */
    protected $clef;

    /**
     * @var string
     * @ORM\Column(name="valeur", type="string")
     */
    protected $valeur;

    /**
     * @var string
     * @ORM\Column(name="option", type="string")
     */
    protected $option;

    /**
     * @var string
     * @ORM\Column(name="valeur_option", type="string")
     */
    protected $valeurOption;


    public function __construct()
    {
        $this->date = new \Datetime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getClef()
    {
        return $this->clef;
    }

    /**
     * @param string $clef
     * @return $this
     */
    public function setClef($clef)
    {
        $this->clef = $clef;

        return $this;
    }

    /**
     * @return string
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * @param string $valeur
     * @return $this
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * @return string
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param string $option
     * @return $this
     */
    public function setOption($option)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * @return string
     */
    public function getValeurOption()
    {
        return $this->valeurOption;
    }

    /**
     * @param string $valeurOption
     * @return $this
     */
    public function setValeurOption($valeurOption)
    {
        $this->valeurOption = $valeurOption;

        return $this;
    }


}