<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 01/11/2016
 * Time: 23:26
 */

namespace Gkratz\AdminBundle\Model;


/**
 * Interface ParametersInterface
 * @package Gkratz\AdminBundle\Model
 */
interface ParametersInterface
{
    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Parameters
     */
    public function setDate($date);

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate();

    /**
     * Set name
     *
     * @param string $name
     * @return Parameters
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set ip
     *
     * @param string $ip
     * @return Parameters
     */
    public function setIp($ip);

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp();

    /**
     * Set maintenance
     *
     * @param boolean $maintenance
     * @return Parameters
     */
    public function setMaintenance($maintenance);

    /**
     * Get maintenance
     *
     * @return boolean
     */
    public function getMaintenance();

    /**
     * Set phone
     *
     * @param string $phone
     * @return Parameters
     */
    public function setPhone($phone);

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone();

    /**
     * Set email
     *
     * @param string $email
     * @return Parameters
     */
    public function setEmail($email);

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail();

    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Parameters
     */
    public function setFacebook($facebook);

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook();

    /**
     * Set twitter
     *
     * @param string $twitter
     * @return Parameters
     */
    public function setTwitter($twitter);

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter();

    /**
     * Set countTrash
     *
     * @param integer $countTrash
     * @return Parameters
     */
    public function setCountTrash($countTrash);

    /**
     * Get countTrash
     *
     * @return integer
     */
    public function getCountTrash();

    /**
     * Get googleMapApikey
     *
     * @return string
     */
    public function getGoogleMapApikey();

    /**
     * Set googleMapApikey
     *
     * @param string $googleMapApikey
     * @return Parameters
     */
    public function setGoogleMapApikey($googleMapApikey);

    /**
     * Get googleMapMainAddress
     *
     * @return string
     */
    public function getGoogleMapMainAddress();

    /**
     * Set googleMapMainAddress
     *
     * @param string $googleMapMainAddress
     * @return Parameters
     */
    public function setGoogleMapMainAddress($googleMapMainAddress);
}