<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 11/03/2017
 * Time: 22:27
 */

namespace Gkratz\AdminBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class MenuItem
 * @package Gkratz\AdminBundle\Model
 */
abstract class MenuItem implements MenuItemInterface
{
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    protected $path;

    /**
     * @Assert\Length(max="30")
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=30, nullable=true)
     */
    protected $icon;

    /**
     * @var int
     *
     * @ORM\Column(name="menu_order", type="integer", nullable=True)
     */
    protected $order;

    /**
     * @var bool
     *
     * @ORM\Column(name="isSection", type="boolean")
     */
    protected $isSection = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isRemovable", type="boolean")
     */
    protected $isRemovable = true;


    protected $entity = [];

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
 * Set name
 *
 * @param string $name
 *
 * @return MenuItem
 */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return MenuItem
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return MenuItem
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set menuOrder
     *
     * @param integer $menuOrder
     *
     * @return MenuItem
     */
    public function setMenuOrder($menuOrder)
    {
        $this->order = $menuOrder;

        return $this;
    }

    /**
     * Get menuOrder
     *
     * @return int
     */
    public function getMenuOrder()
    {
        return $this->order;
    }

    /**
     * Set isSection
     *
     * @param boolean $isSection
     *
     * @return MenuItem
     */
    public function setIsSection($isSection)
    {
        $this->isSection = $isSection;

        return $this;
    }

    /**
     * Get isSection
     *
     * @return bool
     */
    public function getIsSection()
    {
        return $this->isSection;
    }

    /**
     * Set isRemovable
     *
     * @param boolean $isRemovable
     *
     * @return MenuItem
     */
    public function setIsRemovable($isRemovable)
    {
        $this->isRemovable = $isRemovable;

        return $this;
    }

    /**
     * Get isRemovable
     *
     * @return bool
     */
    public function getIsRemovable()
    {
        return $this->isRemovable;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subMenus = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return MenuItem
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }
}