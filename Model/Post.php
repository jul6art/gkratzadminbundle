<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 11/03/2017
 * Time: 22:27
 */

namespace Gkratz\AdminBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Gkratz\AdminBundle\Constants\Constants;

/**
 * Class Post
 * @package Gkratz\AdminBundle\Model
 */
abstract class Post implements PostInterface
{
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    protected $content;

    /**
     * @var boolean
     *
     * @ORM\Column(name="page", type="boolean")
     */
    protected $page;

    /**
     * @var boolean
     *
     * @ORM\Column(name="menu", type="boolean")
     */
    protected $menu;

    /**
     * @var Integer
     * @ORM\Column(name="state", type="integer", nullable=true)
     */
    protected $state;

    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @var \DateTime
     * @ORM\Column(name="last_modification_date", type="datetime")
     */
    protected $lastModificationDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="publication_date", type="datetime", nullable=true)
     */
    protected $publicationDate;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->page = false;
        $this->state = Constants::ENTITY_STATE_VALID;
        $this->date = new \DateTime();
        $this->lastModificationDate = new \DateTime();
        $this->publicationDate = new \DateTime();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPage()
    {
        return $this->page;
    }

    /**
     * @param bool $page
     * @return $this
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Post
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return bool
     */
    public function isMenu()
    {
        return $this->menu;
    }

    /**
     * @param bool $menu
     * @return $this
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastModificationDate()
    {
        return $this->lastModificationDate;
    }

    /**
     * @param \DateTime $lastModificationDate
     * @return $this
     */
    public function setLastModificationDate($lastModificationDate)
    {
        $this->lastModificationDate = $lastModificationDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublicationDate()
    {
        return $this->publicationDate;
    }

    /**
     * @param \DateTime $publicationDate
     * @return $this
     */
    public function setPublicationDate($publicationDate)
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }
}