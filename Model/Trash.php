<?php

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 01/11/2016
 * Time: 22:30
 */

namespace Gkratz\AdminBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Trash
 * @package Gkratz\AdminBundle\Model
 */
abstract class Trash implements TrashInterface
{
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @var string
     * @ORM\Column(name="class", type="string", length=100)
     */
    protected $class;

    /**
     * @var string
     * @ORM\Column(name="legende", type="string", length=100)
     */
    protected $legende;

    /**
     * @var integer
     * @ORM\Column(name="element_id", type="integer")
     */
    protected $elementId;

    /**
     * @var boolean
     * @ORM\Column(name="soft_deleted", type="boolean")
     */
    protected $softDeleted;

    public function getTrashName(){
        $classNamePieces = explode('\\', $this->class);
        return $classNamePieces[2];
    }


    public function __construct()
    {
        $this->date = new \Datetime();
        $this->softDeleted = false;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Trash
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set class
     *
     * @param string $class
     * @return Trash
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return string
     */
    public function getLegende()
    {
        return $this->legende;
    }

    /**
     * @param string $legende
     * @return $this
     */
    public function setLegende($legende)
    {
        $this->legende = $legende;

        return $this;
    }

    /**
     * Set elementId
     *
     * @param integer $elementId
     * @return Trash
     */
    public function setElementId($elementId)
    {
        $this->elementId = $elementId;

        return $this;
    }

    /**
     * Get elementId
     *
     * @return integer
     */
    public function getElementId()
    {
        return $this->elementId;
    }

    /**
     * Set softDeleted
     *
     * @param boolean $softDeleted
     * @return Trash
     */
    public function setSoftDeleted($softDeleted)
    {
        $this->softDeleted = $softDeleted;

        return $this;
    }

    /**
     * Get softDeleted
     *
     * @return boolean
     */
    public function getSoftDeleted()
    {
        return $this->softDeleted;
    }
}
