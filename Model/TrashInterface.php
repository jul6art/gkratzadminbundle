<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 10-02-17
 * Time: 11:48
 */

namespace Gkratz\AdminBundle\Model;


/**
 * Interface TrashInterface
 * @package Gkratz\AdminBundle\Model
 */
interface TrashInterface
{
    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Trash
     */
    public function setDate($date);

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate();

    /**
     * Set class
     *
     * @param string $class
     * @return Trash
     */
    public function setClass($class);

    /**
     * Get class
     *
     * @return string
     */
    public function getClass();



    /**
     * @return string
     */
    public function getLegende();

    /**
     * @param string $legende
     */
    public function setLegende($legende);

    /**
     * Set elementId
     *
     * @param integer $elementId
     * @return Trash
     */
    public function setElementId($elementId);

    /**
     * Get elementId
     *
     * @return integer
     */
    public function getElementId();

    /**
     * Set softDeleted
     *
     * @param boolean $softDeleted
     * @return Trash
     */
    public function setSoftDeleted($softDeleted);

    /**
     * Get softDeleted
     *
     * @return boolean
     */
    public function getSoftDeleted();
}