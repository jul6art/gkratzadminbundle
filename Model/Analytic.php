<?php

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 01/11/2016
 * Time: 22:30
 */

namespace Gkratz\AdminBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Gkratz\AdminBundle\Constants\Constants;

/**
 * Class Analytic
 * @package Gkratz\AdminBundle\Model
 */
abstract class Analytic implements AnalyticInterface
{
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @var string
     * @ORM\Column(name="page", type="string")
     */
    protected $page;

    /**
     * @var string
     * @ORM\Column(name="ip", type="string")
     */
    protected $ip;

    /**
     * @var string
     * @ORM\Column(name="country", type="string")
     */
    protected $country;

    /**
     * @var string
     * @ORM\Column(name="city", type="string")
     */
    protected $city;

    /**
     * @var string
     * @ORM\Column(name="host", type="string")
     */
    protected $host;

    /**
     * @var string
     * @ORM\Column(name="browser", type="string")
     */
    protected $browser;

    /**
     * @var string
     * @ORM\Column(name="language", type="string")
     */
    protected $language;

    /**
     * @var string
     * @ORM\Column(name="referer", type="string")
     */
    protected $referer;

    /**
     *
     * @var boolean
     * @ORM\Column(name="new_session", type="boolean")
     */
    protected $newSession;

    /**
     * @var Integer
     * @ORM\Column(name="state", type="integer", nullable=true)
     */
    protected $state;



    public function __construct()
    {
        $this->date = new \Datetime();
        $this->state = Constants::ENTITY_STATE_VALID;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Analytic
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set page
     *
     * @param string $page
     * @return Analytic
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Analytic
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Analytic
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Analytic
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set host
     *
     * @param string $host
     * @return Analytic
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set browser
     *
     * @param string $browser
     * @return Analytic
     */
    public function setBrowser($browser)
    {
        $this->browser = $browser;

        return $this;
    }

    /**
     * Get browser
     *
     * @return string
     */
    public function getBrowser()
    {
        return $this->browser;
    }

    /**
     * Set language
     *
     * @param string $language
     * @return Analytic
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set referer
     *
     * @param string $referer
     * @return Analytic
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * Get referer
     *
     * @return string
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * Set newSession
     *
     * @param boolean $newSession
     * @return Analytic
     */
    public function setNewSession($newSession)
    {
        $this->newSession = $newSession;

        return $this;
    }

    /**
     * Get newSession
     *
     * @return boolean
     */
    public function getNewSession()
    {
        return $this->newSession;
    }

    /**
     * Get state
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set state
     *
     * @param int $state
     * @return Analytic
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }
}
