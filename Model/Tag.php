<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 11/03/2017
 * Time: 22:27
 */

namespace Gkratz\AdminBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tag
 * @package Gkratz\AdminBundle\Model
 */
abstract class Tag implements TagInterface
{
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}