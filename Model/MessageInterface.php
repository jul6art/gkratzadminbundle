<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 22-03-17
 * Time: 14:15
 */

namespace Gkratz\AdminBundle\Model;


/**
 * Interface MessageInterface
 * @package Gkratz\AdminBundle\Model
 */
interface MessageInterface
{
    /**
     * @return string
     */
    public function getMessage();

    /**
     * @param string $message
     */
    public function setMessage($message);

    /**
     * @return \DateTime
     */
    public function getDate();

    /**
     * @param \DateTime $date
     */
    public function setDate($date);

    /**
     * @return \DateTime
     */
    public function getSeenAt();

    /**
     * @param \DateTime $seenAt
     */
    public function setSeenAt($seenAt);

    /**
     * @return \DateTime
     */
    public function getAdminSeenAt();

    /**
     * @param \DateTime $adminSeenAt
     */
    public function setAdminSeenAt($adminSeenAt);

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Message
     */
    public function setState($state);

    /**
     * Get state
     *
     * @return integer
     */
    public function getState();

    /**
     * @return int
     */
    public function getSeenTimes();

    /**
     * @param int $seenTimes
     */
    public function setSeenTimes($seenTimes);

    /**
     * @return int
     */
    public function getAdminSeenTimes();

    /**
     * @param int $adminSeenTimes
     */
    public function setAdminSeenTimes($adminSeenTimes);
}