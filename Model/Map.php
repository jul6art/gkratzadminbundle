<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 11/03/2017
 * Time: 22:27
 */

namespace Gkratz\AdminBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Map
 * @package Gkratz\AdminBundle\Model
 */
abstract class Map implements MenuInterface
{
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var Integer
     * @ORM\Column(name="state", type="integer", nullable=true)
     */
    protected $state;

    /**
     * @var Integer
     * @ORM\Column(name="zoom", type="integer")
     */
    protected $zoom;

    /**
     * @var Integer
     * @ORM\Column(name="theme", type="integer")
     */
    protected $theme;

    /**
     * @var boolean
     * @ORM\Column(name="display_main_address", type="boolean")
     */
    protected $displayMainAddress;

    /**
     * @var Integer
     * @ORM\Column(name="main_address_icon_size", type="integer")
     */
    protected $mainAddressIconSize;

    /**
     * @var Integer
     * @ORM\Column(name="main_address_icon_color", type="integer")
     */
    protected $mainAddressIconColor;

    /**
     * @var String
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var String
     * @ORM\Column(name="info_text", type="text", nullable=true)
     */
    protected $infoText;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Map
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Map
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return int
     */
    public function getZoom()
    {
        return $this->zoom;
    }

    /**
     * @param $zoom
     * @return $this
     */
    public function setZoom($zoom)
    {
        $this->zoom = $zoom;

        return $this;
    }

    /**
     * @return int
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param $theme
     * @return $this
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * @return String
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return String
     */
    public function getInfoText()
    {
        return $this->infoText;
    }

    /**
     * @param $infoText
     * @return $this
     */
    public function setInfoText($infoText)
    {
        $this->infoText = $infoText;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isDisplayMainAddress()
    {
        return $this->displayMainAddress;
    }

    /**
     * @param $displayMainAddress
     * @return $this
     */
    public function setDisplayMainAddress($displayMainAddress)
    {
        $this->displayMainAddress = $displayMainAddress;

        return $this;
    }

    /**
     * @return int
     */
    public function getMainAddressIconSize()
    {
        return $this->mainAddressIconSize;
    }

    /**
     * @param $mainAddressIconSize
     * @return $this
     */
    public function setMainAddressIconSize($mainAddressIconSize)
    {
        $this->mainAddressIconSize = $mainAddressIconSize;

        return $this;
    }

    /**
     * @return int
     */
    public function getMainAddressIconColor()
    {
        return $this->mainAddressIconColor;
    }

    /**
     * @param $mainAddressIconColor
     * @return $this
     */
    public function setMainAddressIconColor($mainAddressIconColor)
    {
        $this->mainAddressIconColor = $mainAddressIconColor;

        return $this;
    }

    /**
     * Map constructor.
     */
    public function __construct()
    {
        $this->state = 0;
    }
}