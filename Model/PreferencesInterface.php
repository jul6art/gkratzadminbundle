<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 16-02-17
 * Time: 16:03
 */

namespace Gkratz\AdminBundle\Model;


/**
 * Interface PreferencesInterface
 * @package Gkratz\AdminBundle\Model
 */
interface PreferencesInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return \DateTime
     */
    public function getDate();

    /**
     * @param \DateTime $date
     */
    public function setDate($date);

    /**
     * @return string
     */
    public function getClef();

    /**
     * @param string $clef
     */
    public function setClef($clef);

    /**
     * @return string
     */
    public function getValeur();

    /**
     * @param string $valeur
     */
    public function setValeur($valeur);

    /**
     * @return string
     */
    public function getOption();

    /**
     * @param string $option
     */
    public function setOption($option);

    /**
     * @return string
     */
    public function getValeurOption();

    /**
     * @param string $valeurOption
     */
    public function setValeurOption($valeurOption);
}