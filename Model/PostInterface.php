<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 13-03-17
 * Time: 14:32
 */

namespace Gkratz\AdminBundle\Model;


/**
 * Interface PostInterface
 * @package Gkratz\AdminBundle\Model
 */
interface PostInterface
{
    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getContent();

    /**
     * @param string $content
     */
    public function setContent($content);

    /**
     * @return bool
     */
    public function isPage();

    /**
     * @param bool $page
     */
    public function setPage($page);

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Post
     */
    public function setState($state);

    /**
     * Get state
     *
     * @return integer
     */
    public function getState();

    /**
     * @return bool
     */
    public function isMenu();

    /**
     * @param bool $menu
     */
    public function setMenu($menu);

    /**
     * @return \DateTime
     */
    public function getDate();

    /**
     * @param \DateTime $date
     */
    public function setDate($date);

    /**
     * @return \DateTime
     */
    public function getLastModificationDate();

    /**
     * @param \DateTime $lastModificationDate
     */
    public function setLastModificationDate($lastModificationDate);

    /**
     * @return \DateTime
     */
    public function getPublicationDate();

    /**
     * @param \DateTime $publicationDate
     */
    public function setPublicationDate($publicationDate);
}