<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 15-03-17
 * Time: 14:33
 */

namespace Gkratz\AdminBundle\Model;


/**
 * Interface SearchInterface
 * @package Gkratz\AdminBundle\Model
 */
interface SearchInterface
{
    /**
     * @return int
     */
    public function getSearchId();

    /**
     * @param int $searchId
     */
    public function setSearchId($searchId);

    /**
     * @return \DateTime
     */
    public function getDate();

    /**
     * @param \DateTime $date
     */
    public function setDate($date);

    /**
     * @return string
     */
    public function getClass();

    /**
     * @param string $class
     */
    public function setClass($class);

    /**
     * @return int
     */
    public function getElementId();

    /**
     * @param int $elementId
     */
    public function setElementId($elementId);

    /**
     * @return string
     */
    public function getSearchText();

    /**
     * @param string $searchText
     */
    public function setSearchText($searchText);

    /**
     * @return string
     */
    public function getResultText();

    /**
     * @param string $resultText
     */
    public function setResultText($resultText);

    /**
     * @return string
     */
    public function getPoints();

    /**
     * @param string $points
     */
    public function setPoints($points);
}