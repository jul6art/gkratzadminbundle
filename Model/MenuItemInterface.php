<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 11/03/2017
 * Time: 22:27
 */

namespace Gkratz\AdminBundle\Model;


/**
 * Interface MenuItemInterface
 * @package Gkratz\AdminBundle\Model
 */
interface MenuItemInterface
{
    /**
     * Set name
     *
     * @param string $name
     *
     * @return MenuItem
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set path
     *
     * @param string $path
     *
     * @return MenuItem
     */
    public function setPath($path);

    /**
     * Get path
     *
     * @return string
     */
    public function getPath();

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return MenuItem
     */
    public function setIcon($icon);

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon();

    /**
     * Set menuOrder
     *
     * @param integer $menuOrder
     *
     * @return MenuItem
     */
    public function setMenuOrder($menuOrder);

    /**
     * Get menuOrder
     *
     * @return int
     */
    public function getMenuOrder();

    /**
     * Set isSection
     *
     * @param boolean $isSection
     *
     * @return MenuItem
     */
    public function setIsSection($isSection);

    /**
     * Get isSection
     *
     * @return bool
     */
    public function getIsSection();

    /**
     * Set isRemovable
     *
     * @param boolean $isRemovable
     *
     * @return MenuItem
     */
    public function setIsRemovable($isRemovable);

    /**
     * Get isRemovable
     *
     * @return bool
     */
    public function getIsRemovable();

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return MenuItem
     */
    public function setOrder($order);

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder();
}