<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 06-03-17
 * Time: 18:12
 */

namespace Gkratz\AdminBundle\Model;


/**
 * Interface BackupInterface
 * @package Gkratz\AdminBundle\Model
 */
interface BackupInterface
{
    /**
     * @return \DateTime
     */
    public function getDate();

    /**
     * @param \DateTime $date
     */
    public function setDate($date);

    /**
     * @return string
     */
    public function getType();

    /**
     * @param string $type
     */
    public function setType($type);

    /**
     * @return integer
     */
    public function getSize();

    /**
     * @param integer $size
     */
    public function setSize($size);

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Backup
     */
    public function setState($state);

    /**
     * Get state
     *
     * @return integer
     */
    public function getState();
}