<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 22-03-17
 * Time: 14:08
 */

namespace Gkratz\AdminBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Gkratz\AdminBundle\Constants\Constants;

/**
 * Class Message
 * @package Gkratz\AdminBundle\Model
 */
abstract class Message implements MessageInterface
{
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    protected $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="seen_at", type="datetime", nullable=true)
     */
    protected $seenAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="admin_seen_at", type="datetime", nullable=true)
     */
    protected $adminSeenAt;

    /**
     * @var Integer
     * @ORM\Column(name="state", type="integer", nullable=true)
     */
    protected $state;

    /**
     * @var Integer
     * @ORM\Column(name="seen_times", type="integer", nullable=true)
     */
    protected $seenTimes;

    /**
     * @var Integer
     * @ORM\Column(name="admin_seen_times", type="integer", nullable=true)
     */
    protected $adminSeenTimes;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Message constructor.
     */
    public function __construct()
    {
        $this->date = new \DateTime();
        $this->state = Constants::ENTITY_STATE_VALID;
        $this->seenTimes = 0;
        $this->adminSeenTimes = 0;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSeenAt()
    {
        return $this->seenAt;
    }

    /**
     * @param \DateTime $seenAt
     * @return $this
     */
    public function setSeenAt($seenAt)
    {
        $this->seenAt = $seenAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAdminSeenAt()
    {
        return $this->adminSeenAt;
    }

    /**
     * @param \DateTime $adminSeenAt
     * @return $this
     */
    public function setAdminSeenAt($adminSeenAt)
    {
        $this->adminSeenAt = $adminSeenAt;

        return $this;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Message
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return int
     */
    public function getSeenTimes()
    {
        return $this->seenTimes;
    }

    /**
     * @param int $seenTimes
     * @return $this
     */
    public function setSeenTimes($seenTimes)
    {
        $this->seenTimes = $seenTimes;

        return $this;
    }

    /**
     * @return int
     */
    public function getAdminSeenTimes()
    {
        return $this->adminSeenTimes;
    }

    /**
     * @param int $adminSeenTimes
     * @return $this
     */
    public function setAdminSeenTimes($adminSeenTimes)
    {
        $this->adminSeenTimes = $adminSeenTimes;

        return $this;
    }
}