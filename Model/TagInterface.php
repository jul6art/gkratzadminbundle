<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 13-03-17
 * Time: 17:38
 */

namespace Gkratz\AdminBundle\Model;


/**
 * Interface TagInterface
 * @package Gkratz\AdminBundle\Model
 */
interface TagInterface
{
    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tag
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();
}