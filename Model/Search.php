<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 15-03-17
 * Time: 14:27
 */

namespace Gkratz\AdminBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Search
 * @package Gkratz\AdminBundle\Model
 */
abstract class Search implements SearchInterface
{
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="search_id", type="string", length=255)
     */
    protected $searchId;

    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @var string
     * @ORM\Column(name="class", type="string", length=255)
     */
    protected $class;

    /**
     * @var integer
     * @ORM\Column(name="element_id", type="integer")
     */
    protected $elementId;

    /**
     * @var string
     * @ORM\Column(name="search_text", type="text")
     */
    protected $searchText;

    /**
     * @var string
     * @ORM\Column(name="result_text", type="text")
     */
    protected $resultText;

    /**
     * @var integer
     * @ORM\Column(name="points", type="integer")
     */
    protected $points;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return $this
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @return int
     */
    public function getElementId()
    {
        return $this->elementId;
    }

    /**
     * @param int $elementId
     * @return $this
     */
    public function setElementId($elementId)
    {
        $this->elementId = $elementId;

        return $this;
    }

    /**
     * @return string
     */
    public function getSearchText()
    {
        return $this->searchText;
    }

    /**
     * @param string $searchText
     * @return $this
     */
    public function setSearchText($searchText)
    {
        $this->searchText = $searchText;

        return $this;
    }

    /**
     * @return string
     */
    public function getResultText()
    {
        return $this->resultText;
    }

    /**
     * @param string $resultText
     * @return $this
     */
    public function setResultText($resultText)
    {
        $this->resultText = $resultText;

        return $this;
    }

    /**
     * @return string
     */
    public function getSearchId()
    {
        return $this->searchId;
    }

    /**
     * @param int $searchId
     * @return $this
     */
    public function setSearchId($searchId)
    {
        $this->searchId = $searchId;

        return $this;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param string $points
     * @return $this
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }
}