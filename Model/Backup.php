<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 26/02/2017
 * Time: 02:28
 */

namespace Gkratz\AdminBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Gkratz\AdminBundle\Constants\Constants;

/**
 * Class Backup
 * @package Gkratz\AdminBundle\Model
 */
abstract class Backup implements BackupInterface
{
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @var string
     * @ORM\Column(name="type", type="string")
     */
    protected $type;

    /**
     * @var Integer
     * @ORM\Column(name="size", type="integer")
     */
    protected $size;

    /**
     * @var Integer
     * @ORM\Column(name="state", type="integer", nullable=true)
     */
    protected $state;

    public function __construct()
    {
        $this->date = new \Datetime();
        $this->state = Constants::ENTITY_STATE_VALID;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Backup
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }
}