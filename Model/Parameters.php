<?php

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 01/11/2016
 * Time: 22:30
 */

namespace Gkratz\AdminBundle\Model;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bundle\SecurityBundle\Tests\DependencyInjection\MainConfigurationTest;

/**
 * Class Parameters
 * @package Gkratz\AdminBundle\Model
 */
abstract class Parameters implements ParametersInterface
{
    public function __construct()
    {
        $this->maintenance = false;
        $this->countTrash = 0;
    }

    protected $id;

    /**
     * @var string
     * @Encrypted()
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\Length(min=3, max=100)
     */
    protected $name;

    /**
     * @var string
     * @Encrypted()
     * @ORM\Column(name="google_map_apikey", type="string", length=255, nullable=true)
     * @Assert\Length(min=10, max=100)
     */
    protected $googleMapApikey;

    /**
     * @var string
     * @Encrypted()
     * @ORM\Column(name="google_map_main_address", type="string", length=255, nullable=true)
     * @Assert\Length(min=10, max=190)
     */
    protected $googleMapMainAddress;

    /**
     * @var boolean
     * @ORM\Column(name="maintenance", type="boolean")
     */
    protected $maintenance;

    /**
     * @var \Datetime
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @var string
     * @ORM\Column(name="ip", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $ip;

    /**
     * @var string
     * @Encrypted()
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     * @Encrypted()
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email()
     */
    protected $email;

    /**
     * @var string
     * @Encrypted()
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    protected $facebook;

    /**
     * @var string
     * @Encrypted()
     * @ORM\Column(name="twitter", type="string", length=255, nullable=true)
     */
    protected $twitter;

    /**
     * @var integer
     * @ORM\Column(name="count_trash", type="integer")
     */
    protected $countTrash;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Parameters
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Parameters
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Parameters
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Parameters
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Parameters
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     * @return Parameters
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Parameters
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set maintenance
     *
     * @param boolean $maintenance
     * @return Parameters
     */
    public function setMaintenance($maintenance)
    {
        $this->maintenance = $maintenance;

        return $this;
    }

    /**
     * Get maintenance
     *
     * @return boolean
     */
    public function getMaintenance()
    {
        return $this->maintenance;
    }

    /**
     * Set countTrash
     *
     * @param integer $countTrash
     * @return Parameters
     */
    public function setCountTrash($countTrash)
    {
        $this->countTrash = $countTrash;

        return $this;
    }

    /**
     * Get countTrash
     *
     * @return integer
     */
    public function getCountTrash()
    {
        return $this->countTrash;
    }

    /**
     * Get googleMapApikey
     *
     * @return string
     */
    public function getGoogleMapApikey()
    {
        return $this->googleMapApikey;
    }

    /**
     * Set googleMapApikey
     *
     * @param string $googleMapApikey
     * @return Parameters
     */
    public function setGoogleMapApikey($googleMapApikey)
    {
        $this->googleMapApikey = $googleMapApikey;

        return $this;
    }

    /**
     * Get googleMapMainAddress
     *
     * @return string
     */
    public function getGoogleMapMainAddress()
    {
        return $this->googleMapMainAddress;
    }

    /**
     * Set googleMapMainAddress
     *
     * @param string $googleMapMainAddress
     * @return Parameters
     */
    public function setGoogleMapMainAddress($googleMapMainAddress)
    {
        $this->googleMapMainAddress = $googleMapMainAddress;

        return $this;
    }
}
