<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 12/03/2017
 * Time: 22:53
 */

namespace Gkratz\AdminBundle\Model;


/**
 * Interface MapInterface
 * @package Gkratz\AdminBundle\Model
 */
interface MapInterface
{
    /**
     * Set name
     *
     * @param string $name
     *
     * @return Map
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Map
     */
    public function setState($state);

    /**
     * Get state
     *
     * @return integer
     */
    public function getState();

    /**
     * @return int
     */
    public function getZoom();

    /**
     * @param int $zoom
     */
    public function setZoom($zoom);

    /**
     * @return int
     */
    public function getTheme();

    /**
     * @param int $theme
     */
    public function setTheme($theme);

    /**
     * @return String
     */
    public function getTitle();

    /**
     * @param String $title
     */
    public function setTitle($title);

    /**
     * @return String
     */
    public function getInfoText();

    /**
     * @param String $infoText
     */
    public function setInfoText($infoText);

    /**
     * @return boolean
     */
    public function isDisplayMainAddress();

    /**
     * @param boolean $displayMainAddress
     */
    public function setDisplayMainAddress($displayMainAddress);

    /**
     * @return int
     */
    public function getMainAddressIconSize();

    /**
     * @param int $mainAddressIconSize
     */
    public function setMainAddressIconSize($mainAddressIconSize);

    /**
     * @return int
     */
    public function getMainAddressIconColor();

    /**
     * @param int $mainAddressIconColor
     */
    public function setMainAddressIconColor($mainAddressIconColor);
}