<?php

namespace Gkratz\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class GenerateAllCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('gkratz:generate:all')
            ->setDescription('STARTING CREATING EVERYTHING')
            ->addArgument('reset', InputArgument::OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $root = $this->getContainer()->get('kernel')->getRootDir();

        if($input->getArgument('reset') == 'reset'){
            try{
                $output->writeln('<fg=black;bg=cyan>Dropping existing database');

                $database_name = $this->getContainer()->getParameter('database_name');
                $database_user = $this->getContainer()->getParameter('database_user');
                $database_password = $this->getContainer()->getParameter('database_password');

                shell_exec('mysql -u ' . $database_user . ' -p' . $database_password . ' ' . $database_name . ' < '.$root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/sql/reset.sql');
            } catch(\Exception $e){}
        }

        if(!$input->hasOption('env', 'e') || $input->getOption('env') != 'prod'){
            //////////////////////////////////////////////////////////////////////////////////////
            //////////////////////COPYING FILES///////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////
            $output->writeln('<fg=black;bg=cyan>CREATING FILES AND FOLDERS</>');
            try{

                $output->writeln('<fg=green>Creating folder App/Resources/translations</>');
                mkdir($root.'/Resources/translations');

                $output->writeln('<fg=green>Creating folder web/downloads</>');
                mkdir($root.'/../web/downloads');

                $output->writeln('<fg=green>Creating folder web/uploads/img/filemanager</>');
                mkdir($root.'/../web/uploads');
                mkdir($root.'/../web/uploads/img');
                mkdir($root.'/../web/uploads/img/filemanager');

                $output->writeln('<fg=green>Creating folder App/Resources/FOSUserBundle</>');
                rename($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/FOSUserBundle',
                    $root.'/Resources/FOSUserBundle');

                $output->writeln('<fg=green>Creating folder App/Resources/TwigBundle</>');
                rename($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/TwigBundle',
                    $root.'/Resources/TwigBundle');

                $output->writeln('<fg=green>Creating folder web/assets</>');
                rename($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/assets',
                    $root.'/../web/assets');

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/partial-menu.txt');
                $file = fopen($root.'/Resources/views/default/partial-menu.html.twig', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/index.txt');
                $file = fopen($root.'/Resources/views/default/index.html.twig', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/AppBundle.txt');
                $file = fopen($root.'/../src/AppBundle/AppBundle.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                //////////////////////////////////////////////////////////////////////////////////////
                //////////////////////COPYING ENTITIES////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////


                $output->writeln('<fg=black;bg=cyan>Creating entities</>');
                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/Analytic.txt');
                $file = fopen($root.'/../src/AppBundle/Entity/Analytic.php', 'a+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/Parameters.txt');
                $file = fopen($root.'/../src/AppBundle/Entity/Parameters.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/Trash.txt');
                $file = fopen($root.'/../src/AppBundle/Entity/Trash.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/MenuItem.txt');
                $file = fopen($root.'/../src/AppBundle/Entity/MenuItem.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/Menu.txt');
                $file = fopen($root.'/../src/AppBundle/Entity/Menu.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/Map.txt');
                $file = fopen($root.'/../src/AppBundle/Entity/Map.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/Tag.txt');
                $file = fopen($root.'/../src/AppBundle/Entity/Tag.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/Post.txt');
                $file = fopen($root.'/../src/AppBundle/Entity/Post.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/Preferences.txt');
                $file = fopen($root.'/../src/AppBundle/Entity/Preferences.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/Backup.txt');
                $file = fopen($root.'/../src/AppBundle/Entity/Backup.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/Search.txt');
                $file = fopen($root.'/../src/AppBundle/Entity/Search.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/Message.txt');
                $file = fopen($root.'/../src/AppBundle/Entity/Message.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                //////////////////////////////////////////////////////////////////////////////////////
                //////////////////////COPYING REPOSITORIES////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////

                $output->writeln('<fg=black;bg=cyan>Creating repositories</>');
                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/AnalyticRepository.txt');
                $file = fopen($root.'/../src/AppBundle/Repository/AnalyticRepository.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/ParametersRepository.txt');
                $file = fopen($root.'/../src/AppBundle/Repository/ParametersRepository.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/TrashRepository.txt');
                $file = fopen($root.'/../src/AppBundle/Repository/TrashRepository.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/MenuItemRepository.txt');
                $file = fopen($root.'/../src/AppBundle/Repository/MenuItemRepository.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/MenuRepository.txt');
                $file = fopen($root.'/../src/AppBundle/Repository/MenuRepository.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/MapRepository.txt');
                $file = fopen($root.'/../src/AppBundle/Repository/MapRepository.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/TagRepository.txt');
                $file = fopen($root.'/../src/AppBundle/Repository/TagRepository.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/PostRepository.txt');
                $file = fopen($root.'/../src/AppBundle/Repository/PostRepository.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/PreferencesRepository.txt');
                $file = fopen($root.'/../src/AppBundle/Repository/PreferencesRepository.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/BackupRepository.txt');
                $file = fopen($root.'/../src/AppBundle/Repository/BackupRepository.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/SearchRepository.txt');
                $file = fopen($root.'/../src/AppBundle/Repository/SearchRepository.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $entity = file_get_contents($root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data/MessageRepository.txt');
                $file = fopen($root.'/../src/AppBundle/Repository/MessageRepository.php', 'w+');
                fputs($file, $entity);
                fclose($file);

                $output->writeln('<fg=black;bg=cyan>Files and folders successfully created</>');

                $dirname = $root.'/../vendor/gkratz/adminbundle/Gkratz/AdminBundle/_data';
//                array_map('unlink', glob("$dirname/*.*"));
//                $this->deltree($dirname);
            }catch(\Exception $e){
                $output->writeln('<fg=black;bg=red>Impossible to create files and folders. Verify the chmod or chown of your folders</>');
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////GENERATING FULL DATABASE////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////


        $output->writeln('<fg=black;bg=cyan>GENERATING FULL DATABASE</>');

        $command = $this->getApplication()->find('doctrine:generate:entities');
        $arguments = array(
            'command' => 'doctrine:generate:entities',
            'name'    => 'AppBundle/Entity'
        );
        $cmdInput = new ArrayInput($arguments);
        $returnCode = $command->run($cmdInput, $output);

        $command = $this->getApplication()->find('doctrine:migrations:generate');
        $arguments = array(
            'command' => 'doctrine:migrations:generate'
        );
        $cmdInput = new ArrayInput($arguments);
        $returnCode = $command->run($cmdInput, $output);

        $command = $this->getApplication()->find('doctrine:schema:update');
        $arguments = array(
            'command' => 'doctrine:schema:update',
            '--force' => true
        );
        $cmdInput = new ArrayInput($arguments);
        $returnCode = $command->run($cmdInput, $output);


//        if($input->getArgument('option') == 'dev'){
//            $this->deleteCacheInCommand($output);
//            $command = $this->getApplication()->find('doctrine:fixtures:load');
//            $arguments = array(
//                'command' => 'doctrine:fixtures:load',
//                '--append' => true
//            );
//            $cmdInput = new ArrayInput($arguments);
//            $returnCode = $command->run($cmdInput, $output);
//        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////INSTALLING ASSETS///////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////


        $output->writeln('<fg=black;bg=cyan>INSTALLING ASSETS</>');

        $command = $this->getApplication()->find('assets:install');
        $arguments = array(
            'command' => 'assets:install'
        );
        $cmdInput = new ArrayInput($arguments);
        $returnCode = $command->run($cmdInput, $output);

        $command = $this->getApplication()->find('assetic:dump');
        $arguments = array(
            'command' => 'assetic:dump'
        );
        $cmdInput = new ArrayInput($arguments);
        $returnCode = $command->run($cmdInput, $output);

        $this->deleteCacheInCommand($output);
    }

    public function delTree($dir)
    {
        $files = array_diff(scandir($dir), array('.','..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    /**
     * @param OutputInterface $output
     */
    public function deleteCacheInCommand(OutputInterface $output)
    {
        try{
            $filesystem   = $this->getContainer()->get('filesystem');
            $realCacheDir = $this->getContainer()->getParameter('kernel.cache_dir');
            $this->getContainer()->get('cache_clearer')->clear($realCacheDir);
            $filesystem->remove($realCacheDir);
            $output->writeln('<fg=black;bg=cyan>' . $this->getContainer()->get('translator')->trans('Cache successfully deleted') . '</>');
        }catch(\Exception $e){
            $output->writeln('<fg=black;bg=cyan>' . $this->getContainer()->get('translator')->trans('Error deleting the cache') . '</>');
        }
    }
}