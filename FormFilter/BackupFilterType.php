<?php

namespace Gkratz\AdminBundle\FormFilter;

use Gkratz\AdminBundle\Form\Standard\DatePickerRangeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/12/2016
 * Time: 23:41
 */
class BackupFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var  $em \Doctrine\ORM\EntityManager */
        $em = $options['em'];

        //get distinct pages
        $users = $em->getRepository(\AppBundle\Entity\Backup::class)->createQueryBuilder('a')
            ->join('AppBundle:User', 'u', 'WITH', 'a.user = u.id')
            ->groupBy('a.user')
            ->addOrderBy('u.username')
        ;
        $uOb = $users->getQuery()->getResult();
        $u = [];
        foreach($uOb as $ob){
            $u[ucfirst($ob->getUser()->getUsername())] = $ob->getUser()->getId();
        }

        $builder->add('user', ChoiceType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'placeholder' => 'User',
            'choices' => $u,
            'attr' => array(
                'class' => 'form-control gk-select2'
            )
        ));
        $builder->add('type', ChoiceType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'placeholder' => 'Type',
            'choices' => array(
                'database' => '1',
                'files' => '2'
            ),
            'attr' => array(
                'class' => 'form-control gk-select2'
            )
        ));
        $builder->add('date',
            DatePickerRangeType::class,
            array(
                'left_date_options' => array(
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'type' => 'datetime',
                        'placeholder' => 'Start date'
                    )
                ),
                'right_date_options' => array(
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'type' => 'datetime',
                        'placeholder' => 'End date'
                    )
                ),
                'required' => false
            ));
    }

    public function getBlockPrefix()
    {
        return 'backup_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(array('em'));
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}