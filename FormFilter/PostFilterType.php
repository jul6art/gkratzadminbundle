<?php

namespace Gkratz\AdminBundle\FormFilter;

use Gkratz\AdminBundle\Form\Standard\DatePickerRangeType;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/12/2016
 * Time: 23:41
 */
class PostFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var  $em \Doctrine\ORM\EntityManager */
        $em = $options['em'];

        //get distinct pages
        $users = $em->getRepository(\AppBundle\Entity\Post::class)->createQueryBuilder('a')
            ->join('AppBundle:User', 'u', 'WITH', 'a.author = u.id')
            ->groupBy('a.author')
            ->addOrderBy('u.username')
        ;
        $uOb = $users->getQuery()->getResult();
        $u = [];
        foreach($uOb as $ob){
            $u[ucfirst($ob->getAuthor()->getUsername())] = $ob->getAuthor()->getId();
        }

        $builder->add('content', TextType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'attr' => array(
                'class' => 'form-control',
                'placeholder' => 'Content',
            )
        ));

        $builder->add('page', ChoiceType::class, array(
            'required' => false,
            'label' => 'Page',
            'translation_domain' => 'messages',
            'placeholder' => 'Page',
            'choices' => array(
                'yes' => '1',
                'no' => '0'
            ),
            'attr' => array(
                'class' => 'form-control gk-select2'
            )
        ));

        $builder->add('publicationDate', ChoiceType::class, array(
            'required' => false,
            'label' => 'Published',
            'translation_domain' => 'messages',
            'placeholder' => 'Published',
            'choices' => array(
                'yes' => '2',
                'no' => '1'
            ),
            'attr' => array(
                'class' => 'form-control gk-select2'
            ),
            'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return null;
                }
                $field = 'post.publicationDate';
                if ($values['value'] == 1){
                    $expression = $filterQuery->getExpr()->gte($field, ':publicationDate');
                } elseif ($values['value'] == 2) {
                    $expression = $filterQuery->getExpr()->lte($field, ':publicationDate');
                }
                $parameters = array('publicationDate' => new \DateTime());
                return $filterQuery->createCondition($expression, $parameters);
            },
        ));

        $builder->add('menu', ChoiceType::class, array(
            'required' => false,
            'label' => 'Add to menu items',
            'translation_domain' => 'messages',
            'placeholder' => 'Add to menu items',
            'choices' => array(
                'yes' => '1',
                'no' => '0'
            ),
            'attr' => array(
                'class' => 'form-control gk-select2'
            )
        ));

        $builder->add('slug', TextType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'attr' => array(
                'class' => 'form-control',
                'placeholder' => 'Slug',
            )
        ));

        $builder->add('title', TextType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'attr' => array(
                'class' => 'form-control',
                'placeholder' => 'Title',
            )
        ));

        $builder->add('author', ChoiceType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'placeholder' => 'User',
            'choices' => $u,
            'attr' => array(
                'class' => 'form-control gk-select2'
            )
        ));
        $builder->add('lastModificationDate',
            DatePickerRangeType::class,
            array(
                'left_date_options' => array(
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'type' => 'datetime',
                        'placeholder' => 'Start date'
                    )
                ),
                'right_date_options' => array(
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'type' => 'datetime',
                        'placeholder' => 'End date'
                    )
                ),
                'required' => false
            ));
    }

    public function getBlockPrefix()
    {
        return 'post_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(array('em'));
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}