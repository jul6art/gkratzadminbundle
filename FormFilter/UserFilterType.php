<?php

namespace Gkratz\AdminBundle\FormFilter;

use AppBundle\Entity\User;
use Gkratz\AdminBundle\Constants\Constants;
use Gkratz\AdminBundle\Form\Standard\DatePickerRangeType;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/12/2016
 * Time: 23:41
 */
class UserFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var  $em \Doctrine\ORM\EntityManager */
        $em = $options['em'];

        $builder->add('username', TextType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'label' => 'Username' ,
            'attr' => array(
                'placeholder' => 'Username',
                'class' => 'sm-12'
            )
        ));

        $builder->add('email', TextType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'label' => 'Email' ,
            'attr' => array(
                'placeholder' => 'Email',
                'class' => 'sm-12'
            )
        ));

        $builder->add('firstname', TextType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'label' => 'Firstname' ,
            'attr' => array(
                'placeholder' => 'Firstname',
                'class' => 'sm-12'
            )
        ));

        $builder->add('lastname', TextType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'label' => 'Lastname' ,
            'attr' => array(
                'placeholder' => 'Lastname',
                'class' => 'sm-12'
            )
        ));

        $builder->add('address', TextType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'label' => 'Address',
            'attr' => array(
                'class' => 'sm-12',
                'placeholder' => 'Address',
            )
        ));

        $builder->add('phoneNumber', TextType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'attr' => array(
                'class' => 'sm-12',
                'placeholder' => 'Phone'
            )
        ));

        $builder->add('roles', ChoiceType::class, array(
            'required' => false,
            'translation_domain' => 'messages',
            'choices' => User::ROLES_LIST,
            'placeholder' => 'Roles',
            'label' => 'Roles',
            'attr' => array(
                'class' => 'sm-12 gk-select2',
                'placeholder' => 'Roles',
            ),
            'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return null;
                }
                $field = 'user.roles';
                $paramName = 'roles';
                $expression = $filterQuery->getExpr()->like($field, ':'.$paramName);
                $parameters = array($paramName => "%" . $values['value'] . "%");
                return $filterQuery->createCondition($expression, $parameters);
            },
        ));

        $builder->add('lastLogin',
            DatePickerRangeType::class,
            array(
                'left_date_options' => array(
                    'label' => 'Start date',
                    'attr' => array(
                        'class' => 'datepicker sm-12',
                        'type' => 'datetime',
                        'placeholder' => 'Start date'
                    )
                ),
                'right_date_options' => array(
                    'label' => 'End date',
                    'attr' => array(
                        'class' => 'datepicker sm-12',
                        'type' => 'datetime',
                        'placeholder' => 'End date'
                    )
                ),
                'required' => false
            ));
    }

    public function getBlockPrefix()
    {
        return 'user_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('em');
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}