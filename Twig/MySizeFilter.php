<?php

namespace Gkratz\AdminBundle\Twig;

use Gkratz\AdminBundle\Utils\SizeAndUnity;


/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 29/01/2017
 * Time: 18:33
 */
class MySizeFilter extends \Twig_Extension
{
    private $sizeAndUnity;

    /**
     * MySizeFilter constructor.
     * @param SizeAndUnity $sizeAndUnity
     */
    public function __construct(SizeAndUnity $sizeAndUnity)
    {
        $this->sizeAndUnity = $sizeAndUnity;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('size', array($this, 'returnFileSize'))
        );
    }

    public function getName()
    {
        return 'my_size_filter';
    }

    /**
     * @param $entity
     */
    public function returnFileSize($entity)
    {
        if($entity == null){
            echo '';
        } else {
            $size = $this->sizeAndUnity->getSizeWithUnity($entity);
            echo (int) $size['value'] . ' ' . $size['unity'];
        }
    }
}