<?php

namespace Gkratz\AdminBundle\Twig;

use Gkratz\AdminBundle\Utils\GeolocalisationIp;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 29/01/2017
 * Time: 18:33
 */
class IpLocationFilter extends \Twig_Extension
{
    private $geolocService;
    private $request;

    /**
     * IpLocationFilter constructor.
     * @param GeolocalisationIp $geolocalisationIp
     * @param RequestStack $request
     */
    public function __construct(GeolocalisationIp $geolocalisationIp, RequestStack $request)
    {
        $this->geolocService = $geolocalisationIp;
        $this->request = $request;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('geolocalize', array($this, 'returnIpGeolocalise'))
        );
    }

    public function getName()
    {
        return 'ip_location_filter';
    }

    /**
     * @param $entity
     */
    public function returnIpGeolocalise($entity)
    {
        if($entity == null){
            echo '';
        } else {
            $country = $this->geolocService->lookup($entity, $this->request->getCurrentRequest()->getLocale());
            $city = $this->geolocService->lookupCity($entity, $this->request->getCurrentRequest()->getLocale());
            echo $entity . ' ' . $city . ', ' . $country;
        }
    }
}