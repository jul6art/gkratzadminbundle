<?php

namespace Gkratz\AdminBundle\Twig;


/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 29/01/2017
 * Time: 18:33
 */
class EmojiFilter extends \Twig_Extension
{
    private $emojisCode = [
        ":-)" => 1,
        ":)" => 1,
        "=)" => 1,
        "(:" => 1,
        "X)" => 2,
        "x)" => 2,
        "(x" => 2,
        ":-(" => 3,
        ":(" => 3,
        "=(" => 3,
        "):" => 3,
        ":-|" => 4,
        ":|" => 4,
        "=|" => 4,
        "|:" => 4,
        ":-D" => 5,
        ":D" => 5,
        "=D" => 5,
        ";-)" => 6,
        ";)" => 6,
        "(;" => 6,
        ":-P" => 7,
        ":-p" => 7,
        ":P" => 7,
        ":p" => 7,
        "=P" => 7,
        "=p" => 7,
        ":-O" => 8,
        ":-o" => 8,
        ":O" => 8,
        ":o" => 8,
        "=O" => 8,
        "=o" => 8,
        "=-O" => 8,
        "o:" => 8,
        "8-)" => 9,
        "8)" => 9,
        "8-O" => 9,
        "8O" => 9,
        "X-D" => 10,
        "XD" => 10,
        "x-D" => 10,
        "xD" => 10,
        "xd" => 10,
        ":-$" => 11,
        ":$" => 11,
        "=$" => 11,
        "$:" => 11,
        ":-/" => 12,
        ":/" => 12,
        "=/" => 12,
        "/:" => 12,
        "<3" => 13,
        ":-X" => 14,
        ":-x" => 14,
        ":X" => 14,
        ":x" => 14,
        ":-#" => 14,
        ":#" => 14,
        "=X" => 14,
        "=x" => 14,
        "=#" => 14,
        "x:" => 14,
        ":-S" => 15,
        ":S" => 15,
        ":-s" => 15,
        ":s" => 15,
        "=S" => 15,
        "=s" => 15,
        "s:" => 15,
        ":'-(" => 16,
        ":'(" => 16,
        "='(" => 16,
    ];

    private $emojisPosition = [
        "EMOJIID#1" => "-45px -45px",
        "EMOJIID#2" => "-90px -45px",
        "EMOJIID#3" => "-45px -90px",
        "EMOJIID#4" => "-180px 0",
        "EMOJIID#5" => "-90px 0",
        "EMOJIID#6" => "-90px -45px",
        "EMOJIID#7" => "-135px -90px",
        "EMOJIID#8" => "-45px 0",
        "EMOJIID#9" => "-45px 0",
        "EMOJIID#10" => "-90px 0",
        "EMOJIID#11" => "0 0",
        "EMOJIID#12" => "-135px -45px",
        "EMOJIID#13" => "-270px 0",
        "EMOJIID#14" => "-270px -90px",
        "EMOJIID#15" => "-225px -45px",
        "EMOJIID#16" => "-45px -90px",
    ];

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('emoji', array($this, 'returnEmojiText'))
        );
    }

    public function getName()
    {
        return 'emoji_filter';
    }

    /**
     * @param $entity
     * @param $path
     */
    public function returnEmojiText($entity, $path)
    {
        $entity = str_replace('<br />', "BREAKLINE#", $entity);

        foreach($this->emojisCode as $key => $value){
            $entity = str_replace($key, "EMOJIID#".$value, $entity);
        }

        $entity = strip_tags($entity);

        $entity = str_replace("BREAKLINE#", '<br />', $entity);

        foreach(array_reverse($this->emojisPosition) as $key => $value){
            $entity = str_replace($key, ' <small class="emoji" style="background-position:'.$value.';"></small> ', $entity);
        }

        echo $entity;
    }
}