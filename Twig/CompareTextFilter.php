<?php

namespace Gkratz\AdminBundle\Twig;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 29/01/2017
 * Time: 18:33
 */
class CompareTextFilter extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('comparetext', array($this, 'returnComparedText'))
        );
    }

    public function getName()
    {
        return 'compare_text_filter';
    }

    /**
     * @param $entity
     * @param $textToCompare
     * @param $add
     */
    public function returnComparedText($entity, $textToCompare, $add)
    {
        $color = ($add == 'red') ? 'mark' : 'green';
        $rows1 = explode('.', strip_tags($entity));
        $diff = array_diff($rows1, explode('.', strip_tags($textToCompare)));
        if (count($diff) == 0){
            echo $entity;
        } else {
            foreach($diff as $dif){
                try{
                    $rows1[array_search($dif, $rows1)] = '<span class="' . $color . '">' . $dif . '</span>';
                } catch(\Exception $e){
                    echo '<span class="red">' . strtoupper($e->getMessage()) . '</span>';
                }
            }
            echo implode('.<br/><br/>', $rows1);
        }
    }
}