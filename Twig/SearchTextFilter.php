<?php

namespace Gkratz\AdminBundle\Twig;

use Symfony\Component\Translation\TranslatorInterface;


/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 29/01/2017
 * Time: 18:33
 */
class SearchTextFilter extends \Twig_Extension
{
    private $translator;

    /**
     * RedTextFilter constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('searchtext', array($this, 'returnSearchText'))
        );
    }

    public function getName()
    {
        return 'search_text_filter';
    }

    /**
     * @param $entity
     * @param $searchedText
     * @param bool $test
     */
    public function returnSearchText($entity, $searchedText, $test = false)
    {
        $resultCount = 0;

        $wordsInputText = explode(' ', $searchedText);

        if(!$test){
            if(strlen($searchedText) > 1){
                $entity = str_ireplace($searchedText, '<span class="mark">'.strtoupper($searchedText).'</span>', $entity, $i);
                if(is_int(stripos($entity, $searchedText))){
                    $resultCount = count($wordsInputText);
                }
            }
        } else {
            foreach ($wordsInputText as $word){
                if(strlen($word) > 1){
                    $entity = str_ireplace($word, '<span class="mark">'.strtoupper($word).'</span>', $entity, $i);
                    if(is_int(stripos($entity, $word))){
                        $resultCount ++;
                    }
                }
            }
        }

        echo    '<p>'.$entity.'</p>';

            $trad = $this->translator->transChoice(
                    'There is one word found on|There are %count% words found on',
                    $resultCount,
                    array('%count%' => $resultCount)
                );

        echo   '<p>
                    <small>
                        '.$trad.' '.count($wordsInputText).'
                    </small>
                </p>';
    }
}