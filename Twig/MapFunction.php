<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 21/04/2017
 * Time: 23:37
 */

namespace Gkratz\AdminBundle\Twig;


use AppBundle\Entity\Map;
use Doctrine\ORM\EntityManager;
use Gkratz\AdminBundle\Constants\Constants;
use Symfony\Component\Translation\TranslatorInterface;

class MapFunction extends \Twig_Extension
{
    private $manager;
    private $translator;

    /**
     * MapFunction constructor.
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager, TranslatorInterface $translator)
    {
        $this->manager = $manager;
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction(
                'map',
                array($this, 'returnGeneratedMap'),
                array(
                    'needs_environment' => true,
                    'is_safe' => array('html')
                )
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'map_function';
    }

    /**
     * @param \Twig_Environment $environment
     * @param $entity
     * @return string
     */
    public function returnGeneratedMap(\Twig_Environment $environment, $entity)
    {
        if(is_int($entity)){
            $field = 'id';
        } elseif(is_string($entity)){
            $field = 'name';
        }

        if(!isset($field)){
            echo '<p class="error">' . $this->translator->trans('No entities selectionned') . '</p>';
        } else {
            /** @var \AppBundle\Entity\Map $map */
            $map = $this->manager->getRepository(Map::class)->findOneBy(array(
                'state' => Constants::ENTITY_STATE_VALID,
                $field => $entity
            ));

            if($map == null){
                echo '<p class="error">' . $this->translator->trans('No entities selectionned') . '</p>';
            } else {
                return $environment->render('@GkratzAdmin/admin/javascript/google-maps.html.twig', array(
                    'map' => $map
                ));
            }
        }
    }
}