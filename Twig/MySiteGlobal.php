<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 07-02-17
 * Time: 17:04
 */

namespace Gkratz\AdminBundle\Twig;


use AppBundle\Entity\Parameters;
use Doctrine\ORM\EntityManager;

class MySiteGlobal
{
    private $manager;


    /**
     * MySiteGlobal constructor.
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }


    /**
     * @return Parameters|null|object
     */
    public function getInfo()
    {
        return $this->manager->getRepository(Parameters::class)->find(1);
    }


    /**
     * @param $displayMode
     * @return string
     */
    public function getNextMode($displayMode)
    {
        switch($displayMode){
            case "0":
                $nextMode = "2";
                break;
            case "1":
                $nextMode = "3";
                break;
            case "2":
                $nextMode = "0";
                break;
            case "3":
                $nextMode = "1";
        }
        return $nextMode;
    }
}