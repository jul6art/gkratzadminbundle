<?php

/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 13-03-17
 * Time: 15:24
 */
namespace Gkratz\AdminBundle\EventListener;

use AppBundle\Entity\MenuItem;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\Post;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PostSubscriber implements EventSubscriber
{
    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'prePersist',
            'preUpdate',
            'postPersist',
        ];
    }

    private $token_storage;

    public function __construct(TokenStorageInterface $token_storage)
    {
        $this->token_storage = $token_storage;
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function prePersist(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        if ($entity instanceof Post && null === $entity->getAuthor()) {
            /** @var \AppBundle\Entity\User $user */
            $user = $this->token_storage->getToken()->getUser();
            $entity->setAuthor($user);
        }
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function preUpdate(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        if ($entity instanceof Post) {
            $entity->setLastModificationDate(new \DateTime());
        }
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function postPersist(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        if ($entity instanceof Post) {
            if($entity->isMenu()){
                /** @var \Doctrine\ORM\EntityManager $em */
                $em = $event->getEntityManager();
                /** @var \AppBundle\Entity\MenuItem $menuItem */
                $menuItem = new MenuItem();
                $menuItem->setName($entity->getTitle());
                $menuItem->setIsRemovable(1);
                $menuItem->setIsSection(0);
                $menuItem->setPath($entity->getSlug());
                $em->persist($menuItem);
                $em->flush();
            }
        }
    }
}