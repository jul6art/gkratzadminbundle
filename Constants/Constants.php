<?php

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 21/11/2016
 * Time: 00:44
 */

namespace Gkratz\AdminBundle\Constants;

class Constants
{
    /**
     * TODO GENERAL CONSTANTS
     * TODO GENERAL CONSTANTS
     */
    const ENTITY_STATE_VALID = 0;
    const ENTITY_STATE_TRASH = 1;
    const ENTITY_STATE_DELETED = 2;
    const LIMIT_ENTITIES_DISPLAYED_MODE_THUMBNAIL = 500;
    const LIMIT_SIZE_SEARCH_RESULTS_STRING = 1000;


    /**
     * TODO TRANSLATIONBUNDLE CONSTANTS
     * TODO TRANSLATIONBUNDLE CONSTANTS
     */
    const statusEverythingIsOkCode = 200;
    const statusErrorWritingFileCode = 600;
    const statusEmptyParamsCode = 601;
    const statusErrorManagingTheDomCode = 602;
    const statusErrorDeletingCacheCode = 603;
    const statusErrorFileNotFoundCode = 604;
    const statusErrorAjaxCallCode = 605;
    const statusInvalidCsrfTokenCode = 606;


    /**
     * TODO ANALYTICS CONSTANTS
     * TODO ANALYTICS CONSTANTS
     */
    const ITEM_PER_PAGE = 25;
    const COUNTRY_CODE_UNKNOWN = 'ZZZ';
    const COUNTRY_NAME_UNKNOWN = 'Unknown country';
    const CITY_NAME_UNKNOWN = 'Unknown city';

    const DATE_FORMAT_CALENDAR = 'dd/MM/yyyy';
    const PHP_DATETIME_FORMAT_EXPORT = 'd/m/Y H:i:s';
    const PHP_DATE_FORMAT_EXPORT = 'd/m/Y';
    const PHP_TIME_FORMAT_EXPORT = 'H:i:s';
    const PHP_DATETIME_FORMAT = 'Y-m-d H:i:s';
    const PHP_DATE_FORMAT = 'Y-m-d';
    const PHP_TIME_FORMAT = 'H:i:s';
    const FORM_DATE_FORMAT = 'yyyy-MM-dd HH:mm:ss';

    const COUNTRY_LIST = [
        "Afghanistan"=>"Afghanistan",
        "Albania"=>"Albania",
        "Algeria"=>"Algeria",
        "Antigua and Barbuda"=>"Antigua and Barbuda",
        "Argentina"=>"Argentina",
        "Armenia"=>"Armenia",
        "Australia"=>"Australia",
        "Austria"=>"Austria",
        "Azerbaijan"=>"Azerbaijan",
        "Bahamas"=>"Bahamas",
        "Bahrain"=>"Bahrain",
        "Bangladesh"=>"Bangladesh",
        "Barbados"=>"Barbados",
        "Belarus"=>"Belarus",
        "Belgium"=>"Belgium",
        "Belize"=>"Belize",
        "Benin"=>"Benin",
        "Bhutan"=>"Bhutan",
        "Bolivia"=>"Bolivia",
        "Bosnia and Herzegovina"=>"Bosnia and Herzegovina",
        "Botswana"=>"Botswana",
        "Brazil"=>"Brazil",
        "Brunei"=>"Brunei",
        "Bulgaria"=>"Bulgaria",
        "Burkina Faso"=>"Burkina Faso",
        "Burundi"=>"Burundi",
        "Cambodia"=>"Cambodia",
        "Cameroon"=>"Cameroon",
        "Canada"=>"Canada",
        "Cape Verde"=>"Cape Verde",
        "Central African Republic"=>"Central African Republic",
        "Chad"=>"Chad",
        "Chile"=>"Chile",
        "China"=>"China",
        "Colombi"=>"Colombi",
        "Comoros"=>"Comoros",
        "Congo (Brazzaville)"=>"Congo (Brazzaville)",
        "Congo"=>"Congo",
        "Costa Rica"=>"Costa Rica",
        "Cote d'Ivoire"=>"Cote d'Ivoire",
        "Croatia"=>"Croatia",
        "Cuba"=>"Cuba",
        "Cyprus"=>"Cyprus",
        "Czech Republic"=>"Czech Republic",
        "Denmark"=>"Denmark",
        "Djibouti"=>"Djibouti",
        "Dominica"=>"Dominica",
        "Dominican Republic"=>"Dominican Republic",
        "East Timor (Timor Timur)"=>"East Timor (Timor Timur)",
        "Ecuador"=>"Ecuador",
        "Egypt"=>"Egypt",
        "El Salvador"=>"El Salvador",
        "Equatorial Guinea"=>"Equatorial Guinea",
        "Eritrea"=>"Eritrea",
        "Estonia"=>"Estonia",
        "Ethiopia"=>"Ethiopia",
        "Fiji"=>"Fiji",
        "Finland"=>"Finland",
        "France"=>"France",
        "Gabon"=>"Gabon",
        "Gambia, The"=>"Gambia, The",
        "Georgia"=>"Georgia",
        "Germany"=>"Germany",
        "Ghana"=>"Ghana",
        "Greece"=>"Greece",
        "Grenada"=>"Grenada",
        "Guatemala"=>"Guatemala",
        "Guinea"=>"Guinea",
        "Guinea-Bissau"=>"Guinea-Bissau",
        "Guyana"=>"Guyana",
        "Haiti"=>"Haiti",
        "Honduras"=>"Honduras",
        "Hungary"=>"Hungary",
        "Iceland"=>"Iceland",
        "India"=>"India",
        "Indonesia"=>"Indonesia",
        "Iran"=>"Iran",
        "Iraq"=>"Iraq",
        "Ireland"=>"Ireland",
        "Israel"=>"Israel",
        "Italy"=>"Italy",
        "Jamaica"=>"Jamaica",
        "Japan"=>"Japan",
        "Jordan"=>"Jordan",
        "Kazakhstan"=>"Kazakhstan",
        "Kenya"=>"Kenya",
        "Kiribati"=>"Kiribati",
        "Korea, North"=>"Korea, North",
        "Korea, South"=>"Korea, South",
        "Kuwait"=>"Kuwait",
        "Kyrgyzstan"=>"Kyrgyzstan",
        "Laos"=>"Laos",
        "Latvia"=>"Latvia",
        "Lebanon"=>"Lebanon",
        "Lesotho"=>"Lesotho",
        "Liberia"=>"Liberia",
        "Libya"=>"Libya",
        "Liechtenstein"=>"Liechtenstein",
        "Lithuania"=>"Lithuania",
        "Luxembourg"=>"Luxembourg",
        "Macedonia"=>"Macedonia",
        "Madagascar"=>"Madagascar",
        "Malawi"=>"Malawi",
        "Malaysia"=>"Malaysia",
        "Maldives"=>"Maldives",
        "Mali"=>"Mali",
        "Malta"=>"Malta",
        "Marshall Islands"=>"Marshall Islands",
        "Mauritania"=>"Mauritania",
        "Mauritius"=>"Mauritius",
        "Mexico"=>"Mexico",
        "Micronesia"=>"Micronesia",
        "Moldova"=>"Moldova",
        "Monaco"=>"Monaco",
        "Mongolia"=>"Mongolia",
        "Morocco"=>"Morocco",
        "Mozambique"=>"Mozambique",
        "Myanmar"=>"Myanmar",
        "Namibia"=>"Namibia",
        "Nauru"=>"Nauru",
        "Nepa"=>"Nepa",
        "Netherlands"=>"Netherlands",
        "New Zealand"=>"New Zealand",
        "Nicaragua"=>"Nicaragua",
        "Niger"=>"Niger",
        "Nigeria"=>"Nigeria",
        "Norway"=>"Norway",
        "Oman"=>"Oman",
        "Pakistan"=>"Pakistan",
        "Palau"=>"Palau",
        "Panama"=>"Panama",
        "Papua New Guinea"=>"Papua New Guinea",
        "Paraguay"=>"Paraguay",
        "Peru"=>"Peru",
        "Philippines"=>"Philippines",
        "Poland"=>"Poland",
        "Portugal"=>"Portugal",
        "Qatar"=>"Qatar",
        "Romania"=>"Romania",
        "Russia"=>"Russia",
        "Rwanda"=>"Rwanda",
        "Saint Kitts and Nevis"=>"Saint Kitts and Nevis",
        "Saint Lucia"=>"Saint Lucia",
        "Saint Vincent"=>"Saint Vincent",
        "Samoa"=>"Samoa",
        "San Marino"=>"San Marino",
        "Sao Tome and Principe"=>"Sao Tome and Principe",
        "Saudi Arabia"=>"Saudi Arabia",
        "Senegal"=>"Senegal",
        "Serbia and Montenegro"=>"Serbia and Montenegro",
        "Seychelles"=>"Seychelles",
        "Sierra Leone"=>"Sierra Leone",
        "Singapore"=>"Singapore",
        "Slovakia"=>"Slovakia",
        "Slovenia"=>"Slovenia",
        "Solomon Islands"=>"Solomon Islands",
        "Somalia"=>"Somalia",
        "South Africa"=>"South Africa",
        "Spain"=>"Spain",
        "Sri Lanka"=>"Sri Lanka",
        "Sudan"=>"Sudan",
        "Suriname"=>"Suriname",
        "Swaziland"=>"Swaziland",
        "Sweden"=>"Sweden",
        "Switzerland"=>"Switzerland",
        "Syria"=>"Syria",
        "Taiwan"=>"Taiwan",
        "Tajikistan"=>"Tajikistan",
        "Tanzania"=>"Tanzania",
        "Thailand"=>"Thailand",
        "Togo"=>"Togo",
        "Tonga"=>"Tonga",
        "Trinidad and Tobago"=>"Trinidad and Tobago",
        "Tunisia"=>"Tunisia",
        "Turkey"=>"Turkey",
        "Turkmenistan"=>"Turkmenistan",
        "Tuvalu"=>"Tuvalu",
        "Uganda"=>"Uganda",
        "Ukraine"=>"Ukraine",
        "United Arab Emirates"=>"United Arab Emirates",
        "United Kingdom"=>"United Kingdom",
        "United States"=>"United States",
        "Uruguay"=>"Uruguay",
        "Uzbekistan"=>"Uzbekistan",
        "Vanuatu"=>"Vanuatu",
        "Vatican City"=>"Vatican City",
        "Venezuela"=>"Venezuela",
        "Vietnam"=>"Vietnam",
        "Yemen"=>"Yemen",
        "Zambia"=>"Zambia",
        "Zimbabwe"=>"Zimbabwe"
    ];
}