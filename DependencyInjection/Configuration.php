<?php

namespace Gkratz\AdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('gkratz_admin');

        $rootNode
            ->children()
                ->arrayNode('search')
                    ->children()
                        ->scalarNode('allow_approaching')
                        ->end()
                        ->scalarNode('fields')
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('notification')
                    ->children()
                        ->scalarNode('option')
                        ->end()
                        ->scalarNode('fields')
                        ->end()
                    ->end()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
