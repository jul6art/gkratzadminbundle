<?php

/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 16-11-16
 * Time: 13:17
 */
namespace Gkratz\AdminBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

class GkratzAdminExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $processedConfig = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yml');

        $configDefinition = $container->getDefinition('gkratz_configurator');

        if (isset($processedConfig['search'])) {
            $configDefinition->addMethodCall('setSearch', array($processedConfig['search']));
        }

        if (isset($processedConfig['notification'])) {
            $configDefinition->addMethodCall('setNotification', array($processedConfig['notification']));
        }
    }
}