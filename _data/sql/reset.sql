SET foreign_key_checks = 0;

DROP TABLE `analytic`, `backup`, `maintenance`, `menu`, `menu_item`, `message`, `post`, `post_audit`, `post_tag`, `preferences`, `revisions`, `search`, `tag`, `trash`, `user`;

SET foreign_key_checks = 1;