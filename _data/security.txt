security:
    encoders:
        AppBundle\Entity\User: sha512

    providers:
        main:
            id: fos_user.user_provider.username_email

    firewalls:
        main:
            pattern: ^/
            form_login:
                login_path: fos_user_security_login
                check_path: fos_user_security_check
                provider: main
                csrf_token_generator: security.csrf.token_manager
            anonymous: true
            logout:
                path: fos_user_security_logout
            anonymous:    true
            remember_me:
                secret:     %secret%

    role_hierarchy:
            ROLE_USER: [ROLE_ANONYMOUS_USER]
            ROLE_ADMIN: [ROLE_USER, ROLE_ALLOWED_TO_SWITCH]
            ROLE_SUPER_ADMIN: [ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]

    access_control:
        - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/admin/, role: ROLE_ADMIN }