<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gkratz\AdminBundle\Model\Menu as BaseMenu;

/**
 *
 *  @ORM\Entity(repositoryClass="AppBundle\Repository\MenuRepository")
 *  @ORM\Table(name="menu")
 */
class Menu extends BaseMenu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
