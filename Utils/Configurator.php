<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 15-03-17
 * Time: 13:10
 */

namespace Gkratz\AdminBundle\Utils;


class Configurator
{
    private $search;
    private $notification;

    /**
     * @param $search
     */
    public function setSearch($search)
    {
        $this->search = $search;
    }

    /**
     * @return mixed
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * @return mixed
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * @param mixed $notification
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;
    }

    /**
     * @param array $searchConfig
     * @return array
     */
    public function convertSearchArray(array $searchConfig)
    {
        $convertedArray = [];

        foreach (explode(',', $searchConfig['fields']) as $field){
            $field = trim($field);
            $cols = explode('.', $field);
            $convertedArray[$cols[0]] []= $cols[1];
        }

        $result = [
			'allow_approaching' => $searchConfig['allow_approaching'],
            'results' => $convertedArray
		];

        return $result;
    }
}