<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 22/10/2017
 * Time: 06:00
 */

namespace Gkratz\AdminBundle\Utils\Analytic;


use AppBundle\Entity\Analytic;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\TranslatorInterface;

class Homepage extends Base
{
    private $manager;
    private $translator;

    public function __construct(EntityManager $manager, TranslatorInterface $translator)
    {
        $this->manager = $manager;
        $this->translator = $translator;
    }

    /**
     * @return EntityManager
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param EntityManager $manager
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return TranslatorInterface
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public function getHomePageUniqueGraph()
    {
        //set db managers
        $em = $this->getManager();
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, COUNT(a.id) AS records')
            ->where('a.date BETWEEN :date1 AND :date2 AND a.newSession = :newSession')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('v_day')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array('count' => 0);
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($this->getTranslator(), $sorts, $dow);

        //sort records
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                $params[$offset]['count'] += $entity["records"];
            }
        }

        //set series
        $counts = array(
            $params[1]['count'], $params[2]['count'], $params[3]['count'],
            $params[4]['count'], $params[5]['count'], $params[6]['count'],
            $params[0]['count']
        );

        //set categories
        $days = array(
            $params[1]['day'], $params[2]['day'], $params[3]['day'],
            $params[4]['day'], $params[5]['day'], $params[6]['day'],
            $params[0]['day']
        );

        return array(
            'counts' => $counts,
            'days' => $days
        );
    }
}