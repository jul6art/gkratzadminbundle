<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 08/03/2017
 * Time: 00:34
 */

namespace Gkratz\AdminBundle\Utils;


class SizeAndUnity
{
    /**
     * @param $size
     * @return array
     */
    public function getSizeWithUnity($size)
    {
        $unities = ['o', 'Ko', 'Mo', 'Go', 'To'];
		
        $i = 0;
		
        while($size > 1000){
            $size /= 1000;
            $i ++;
        }
		
       return [
            'value' => $size,
            'unity' => $unities[$i]
        ];
    }
}