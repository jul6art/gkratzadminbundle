<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 15-03-17
 * Time: 11:26
 */

namespace Gkratz\AdminBundle\Utils;


use AppBundle\Entity\Search;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Gkratz\AdminBundle\Constants\Constants;

class Searcher
{
    private $allowApproaching;

    /**
     * @param $searchParams
     * @param $searchText
     * @param EntityManager $em
     * @return array
     */
    public function getResultId($searchParams, $searchText, EntityManager $em)
    {
        //unique date and unique id for the search
        $date = new \DateTime();
        $searchId = $date->format('YmdHis') . '-' . uniqid(rand(10000,99999), true);

        //creation of search entity
        $search = new Search();
        $search->setSearchId($searchId);
        $search->setClass('test');
        $search->setDate($date);
        $search->setElementId(0);
        $search->setSearchText($searchText);
        $search->setResultText('');
        $search->setPoints(0);

        $em->persist($search);

        //check if we need perfect match or approaching match
        $this->allowApproaching = $searchParams['allow_approaching'];

        foreach ($searchParams['results'] as $className => $fields){
            //basic search in database
            if(!$this->allowApproaching){
                $resultObjects = $this->getApproachingFalseQuery($em, $className, $fields, $searchText)->getQuery()->getResult();
            } else {
                $resultObjects = $this->getApproachingTrueQuery($em, $className, $fields, $searchText)->getResult();
            }

            //split of the searched text
            $splittedText = explode(' ', $searchText);

            //loop for words in query and words in database objects
            foreach($resultObjects as $ob){
                $text = '';

                for($i = 0; $i < count($fields); $i ++){
                    $function = 'get'.ucfirst($fields[$i]);

                    if(!$this->allowApproaching){
                        if(is_int(stripos($ob->$function(), $searchText))){
                            //test the words, escape and limit size of text
                            $text = $this->getWellIndexedResult($ob->$function(), $searchText);
                        }
                    } else {
                        for($ii = 0; $ii < count($splittedText); $ii ++){
                            if(strlen($splittedText[$ii]) > 1){
                                if(is_int(stripos($ob->$function(), $splittedText[$ii]))){
                                    //test the words, escape and limit size of text
                                    $text = $this->getWellIndexedResult($ob->$function(), $splittedText[$ii]);

                                    $ii = count($fields);
                                }
                            }
                        }
                    }
                }

                if($text != null){
                    //get total points for every occurence
                    $points = (!$this->allowApproaching) ? 100 : $this->getPoints($text, $splittedText);
                    //creation of search entity
                    $search = new Search();
                    $search->setSearchId($searchId);
                    $search->setClass($className);
                    $search->setDate($date);
                    $search->setElementId($ob->getId());
                    $search->setSearchText($searchText);
                    $search->setResultText($text);
                    $search->setPoints($points);

                    $em->persist($search);
                }
            }
        }
        $em->flush();
        return [
            'id' => $searchId,
            'allowApproaching' => ($this->allowApproaching) ? 1 : 0
        ];
    }


    /**
     * @param EntityManager $em
     * @param $className
     * @param $fields
     * @param $searchText
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function getApproachingFalseQuery(EntityManager $em, $className, $fields, $searchText)
    {
        $query = $em->getRepository('AppBundle:'.$className)
            ->createQueryBuilder('s')
            ->where('s.state = :state');

        $condition = '';

        for($i = 0; $i < count($fields); $i ++){
            $condition .= ($i == 0) ? 's.'.$fields[$i].' LIKE :search' : ' OR s.'.$fields[$i].' LIKE :search';
        }

        $query->andWhere($condition)
            ->setParameters([
                'state' => Constants::ENTITY_STATE_VALID,
                'search' => "%".$searchText."%"
            ]);

        return $query;
    }


    /**
     * @param EntityManager $em
     * @param $className
     * @param $fields
     * @param $searchText
     * @return \Doctrine\ORM\NativeQuery
     */
    private function getApproachingTrueQuery(EntityManager $em, $className, $fields, $searchText)
    {
        $rsm = new ResultSetMapping();

        $rsm->addEntityResult('AppBundle:'.$className, 'x');
        $rsm->addFieldResult('x', 'id', 'id');
        foreach ($fields as $field){
            $rsm->addFieldResult('x', $field, $field);
        }

        $query = 'SELECT * FROM '.strtolower($className).' x WHERE x.state = '.Constants::ENTITY_STATE_VALID;

        $searchTextWords = explode(' ', $searchText);

        for($i = 0; $i < count($fields); $i ++){
            if($i == 0){
                $query .= " AND (x.".$fields[$i]." RLIKE '";
                for($ii = 0; $ii < count($searchTextWords); $ii ++){
                    $query .= ($ii == 0) ? $searchTextWords[$ii] : "|".$searchTextWords[$ii];
                }
                $query .= "'";
            } else {
                $query .= " OR x.".$fields[$i]." RLIKE '";
                for($ii = 0; $ii < count($searchTextWords); $ii ++){
                    $query .= ($ii == 0) ? $searchTextWords[$ii] : "|".$searchTextWords[$ii];
                }
                $query .= "'";
            }
            if($i == (count($fields) - 1)){
                $query .= ")";
            }
        }

        $result = $em->createNativeQuery($query, $rsm);

        return $result;
    }


    /**
     * @param $entityText
     * @param $search
     * @return null|string
     */
    private function getWellIndexedResult($entityText, $search)
    {
        $text = '';
        if(!$this->allowApproaching){
            $text = $this->escapeAndLimitTextSize($text, $entityText, $search);

            if(!is_int(stripos($text, $search))){
                $text = '';
            }
        } else {
            $words = explode(' ', $search);

            for($i = 0; $i < count($words); $i ++){
                $text = $this->escapeAndLimitTextSize($text, $entityText, $words[$i]);

                if(is_int(stripos($text, $words[$i]))){
                    $i = count($words);
                } else {
                    $text = '';
                }
            }
        }

        return ($text == '') ? null : $text;
    }

    /**
     * @param $text
     * @param $entityText
     * @param $searchedText
     * @return string
     */
    public function escapeAndLimitTextSize($text, $entityText, $searchedText)
    {
        $entityText = strip_tags($entityText);
        $position = stripos($entityText, $searchedText);

        $max = Constants::LIMIT_SIZE_SEARCH_RESULTS_STRING;

        while ($position > $max){
            $max += Constants::LIMIT_SIZE_SEARCH_RESULTS_STRING;
        }

        $offset = $max - Constants::LIMIT_SIZE_SEARCH_RESULTS_STRING;

        if($offset > 0){
            $text .= '...';
        }

        $text .= substr($entityText, $offset);

        if (strlen($text) > Constants::LIMIT_SIZE_SEARCH_RESULTS_STRING) {
            $text = rtrim(substr($text, 0, Constants::LIMIT_SIZE_SEARCH_RESULTS_STRING)).'...';
        }

        return $text;
    }

    /**
     * @param $text
     * @param array $words
     * @return int
     */
    public function getPoints($text, array $words)
    {
        $count = 0;

        foreach($words as $word){
            if(is_int(stripos($text, $word))){
                $count ++;
            }
        }

        return ((int) (($count * 100) / count($words)));
    }
}