<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 10-03-17
 * Time: 14:23
 */

namespace Gkratz\AdminBundle\Utils\Files;

use ZipArchive;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class Zipper
{
    /**
     * @param array $sources
     * @param $zipTo
     * @param array|null $patterns
     * @param array|null $singleFiles
     * @return bool
     */
    public function zip(array $sources, $zipTo, array $patterns = null, array $singleFiles = null)
    {
        if(extension_loaded('zip')){
            $zip = new ZipArchive();
            $zip->open($zipTo, ZipArchive::CREATE | ZipArchive::OVERWRITE);

            foreach ($sources as $source) {
                $rootPath = realpath($source);

                $pathPieces = explode('/', $source);
                $i = 0;
                $newPath = '';
                while($i < (count($pathPieces) - 1)){
                    $newPath .= $pathPieces[$i].'/';
                    $i ++;
                }
                $newPath = realpath($newPath);

                $files = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($rootPath),
                    RecursiveIteratorIterator::LEAVES_ONLY
                );

                foreach ($files as $name => $file)
                {
                    if (!$file->isDir())
                    {
                        $filePath = $file->getRealPath();
                        $relativePath = substr($filePath, strlen($newPath));

                        foreach($patterns as $pattern){
                            $locked = false;
                            if(preg_match($pattern, $relativePath)){
                                $locked = true;
                            }
                            if(!$locked){
                                $zip->addFile($filePath, $relativePath);
                            }
                        }
                    }
                }
            }
            foreach($singleFiles as $singleFile){
                try{
                    $zip->addFile($sources[0].'/../'.$singleFile, $singleFile);
                } catch (\Exception $e){

                }
            }

            $zip->close();
            return true;
        } else {
            return false;
        }
    }
}