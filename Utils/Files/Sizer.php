<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 10-03-17
 * Time: 14:31
 */

namespace Gkratz\AdminBundle\Utils\Files;


use FilesystemIterator;
use Gkratz\AdminBundle\Utils\SizeAndUnity;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class Sizer
{
    /** @var \Gkratz\AdminBundle\Utils\SizeAndUnity SizeAndUnity */
    private $sizeAndUnity;

    public function __construct(SizeAndUnity $sizeAndUnity)
    {
        $this->sizeAndUnity = $sizeAndUnity;
    }

    /**
     * @param $path
     * @return array
     */
    public function sizeFolder($path)
    {
        $size = 0;
        $path = realpath($path);
        if($path!==false){
            foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object){
                try{
                    $size += $object->getSize();
                } catch (\Exception $e){}
            }
        }
        $folder = $this->sizeAndUnity->getSizeWithUnity($size);
        return $folder;
    }

    /**
     * @param $path
     * @param $filename
     * @return int
     */
    public function sizeFile($path, $filename)
    {
        $size = 0;
        $path = realpath($path);
        if($path!==false){
            foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object){
                if($object->getFilename() == $filename){
                    $size += $object->getSize();
                }
            }
        }
        return $size;
    }
}